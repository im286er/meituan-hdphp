<?php if(!defined('HDPHP_PATH'))exit;
return array (
  'order_id' => 
  array (
    'field' => 'order_id',
    'type' => 'int(10) unsigned zerofill',
    'null' => 'NO',
    'key' => true,
    'default' => NULL,
    'extra' => 'auto_increment',
  ),
  'sendtime' => 
  array (
    'field' => 'sendtime',
    'type' => 'int(10)',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'goods_num' => 
  array (
    'field' => 'goods_num',
    'type' => 'tinyint(4) unsigned',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'subtotal' => 
  array (
    'field' => 'subtotal',
    'type' => 'decimal(10,2)',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'uid' => 
  array (
    'field' => 'uid',
    'type' => 'int(11)',
    'null' => 'NO',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'gid' => 
  array (
    'field' => 'gid',
    'type' => 'int(10) unsigned',
    'null' => 'NO',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'state' => 
  array (
    'field' => 'state',
    'type' => 'set(\'0\',\'1\')',
    'null' => 'YES',
    'key' => false,
    'default' => '0',
    'extra' => '',
  ),
  'is_comments' => 
  array (
    'field' => 'is_comments',
    'type' => 'tinyint(1)',
    'null' => 'NO',
    'key' => false,
    'default' => '0',
    'extra' => '',
  ),
);
?>