<?php if(!defined('HDPHP_PATH'))exit;
return array (
  'sid' => 
  array (
    'field' => 'sid',
    'type' => 'int(11)',
    'null' => 'NO',
    'key' => true,
    'default' => NULL,
    'extra' => 'auto_increment',
  ),
  'sname' => 
  array (
    'field' => 'sname',
    'type' => 'varchar(45)',
    'null' => 'NO',
    'key' => false,
    'default' => '',
    'extra' => '',
  ),
  'introduction' => 
  array (
    'field' => 'introduction',
    'type' => 'text',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
);
?>