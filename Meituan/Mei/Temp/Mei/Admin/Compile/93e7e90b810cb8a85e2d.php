<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>属性列表</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/base.css" />
	<script type="text/javascript">
			$(function(){
				$('form[name=edit_form]').hide();
				$('.edit').click(function(){

						if($(this).html() == '保存'){
							$(this).parents('tr').find('form').submit();
						}
						
						$('.form_value').hide();
						$('.span_value').show();
						$('.edit').html('修改');
						
						$(this).html('保存');
						$(this).parents('tr').find('span').hide();
						$(this).parents('tr').find('form').show();
				})
			})
	</script>
</head>
<body>
	<div class="pos">属性列表</div>
	<table class="table table-bordered table-hover">
		<tr height="30">
			<th width="5%" style="text-align:center">atid</th>
			<th width="60%">属性名称</th>
			<th width="5%" style="text-align:center">种类id</th>
			<th width="5%" style="text-align:center">父级id</th>
			<th style="text-align:center">操作</th>
		</tr>
		<?php if(is_array($allAttr)):?><?php  foreach($allAttr as $k=>$v){ ?>									
			<tr height="30">
				<td style="text-align:center"><?php echo $v['atid'];?></td>
				<td>
					<span class="span_value"><?php echo $v['attrname'];?></span>
					<form class="form_value" action="<?php echo U('attr_edit');?>" name="edit_form" method="post">
						<input type="text" name="attrname" value="<?php echo $v['attrname'];?>" />
						<input type="hidden" name="kindid" value="<?php echo $v['kindid'];?>" />
					</form>
				</td>
				<td style="text-align:center"><?php echo $v['kindid'];?></td>
				<td style="text-align:center"><?php echo $v['pid'];?></td>
				<td style="text-align:center"><a href="<?php echo U('attr_value_list',array('kindid'=>$v['kindid']));?>" class="btn btn-info btn-mini">查看</a> <a href="javascript:" class="btn btn-primary btn-mini edit">修改</a> <a href="javascript:" class="btn btn-danger btn-mini" onclick="if(confirm('确定删除吗？'))location.href='<?php echo U('attr_del',array('kindid'=>$v['kindid']));?>'">删除</a></td>
			</tr>
		<?php }?><?php endif;?>
	</table>
	<div class="pagelist"><?php echo $pagelist;?></div>
</body>
</html>