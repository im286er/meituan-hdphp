<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>商品列表</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/base.css" />

</head>
<body>
	<div class="pos">商品列表</div>
	<table class="table table-bordered table-hover">
		<tr>
			<th width="10%" style="text-align:center">订单编号</th>
			<th width="15%" style="text-align:center">下单时间</th>
			<th width="20%" style="text-align:center">商品名称</th>
			<th width="10%" style="text-align:center">商品数量</th>
			<th width="15%" style="text-align:center">总价格</th>
			<th width="15%" style="text-align:center">订单状态</th>
			<th style="text-align:center">操作</th>
		</tr>
		<?php if(is_array($order)):?><?php  foreach($order as $k=>$v){ ?>									
			<tr height="40">
				<td style="text-align:center"><?php echo $v['order_id'];?></td>
				<td style="text-align:center"><?php echo hd_date($v['sendtime'],'Y-m-d');?></td>
				<td style="text-align:center"><?php echo $v['title'];?></td>
				<td style="text-align:center"><?php echo $v['goods_num'];?></td>
				<td style="text-align:center"><?php echo $v['subtotal'];?></td>
				<td style="text-align:center"><?php if($v['state'] == 1){?>已付款<?php  }else{ ?><span style="color:red">待付款</span><?php }?></td>
				<td style="text-align:center"><a href="javascript:" class="btn btn-primary btn-small edit">查看</a></td>
			</tr>
		<?php }?><?php endif;?>
	</table>
	<div class="pagelist"><?php echo $pagelist;?></div>
</body>
</html>