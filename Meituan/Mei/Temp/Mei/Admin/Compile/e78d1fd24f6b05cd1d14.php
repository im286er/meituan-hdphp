<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>添加类型</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<!-- bootstrap 核心js -->
	<script src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<style type="text/css">
		input[type="text"]{
            background-color: #FFFFFF;
            border: 2px solid #DDDDDD;
            border-radius: 0;
            box-shadow: 2px 2px 2px #F0F0F0 inset !important;
            color: #555555;
            display: inline-block;
            font-size: 12px;
            line-height: 30px;
            margin: 0;
            padding: 0 6px;
            vertical-align: middle;
            height: 30px;
        }
	</style>
</head>
<body>
	<div class="pos">添加类型</div>
	<form action="<?php echo U('add_1');?>" name="addForm" method="post">
		<table class="table table-bordered table-hover">

			<tr height="30">
				<td class="" width="150">所属分类：</td>
				<td>
					<select name="category">
						<?php if(is_array($allCate)):?><?php  foreach($allCate as $v){ ?>
						<option value="<?php echo $v['cid'];?>"><?php echo $v['cname'];?></option>
						<?php }?><?php endif;?>
					</select>
				</td>
			</tr>
			<tr height="30">
				<td class="" width="150">所属商家：</td>
				<td>
					<select name="seller">
						<?php if(is_array($allSeller)):?><?php  foreach($allSeller as $v){ ?>
						<option value="<?php echo $v['sid'];?>"><?php echo $v['sname'];?></option>
						<?php }?><?php endif;?>
					</select>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="下一步" class="btn btn-primary" /></td>
			</tr>
		</table>
	</form>
</body>
</html>