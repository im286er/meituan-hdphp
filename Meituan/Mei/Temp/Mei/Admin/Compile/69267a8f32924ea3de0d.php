<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>网站管理后台登陆</title>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/base.css" />
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/login.css" />
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<!-- 后台遮罩效果 -->
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/zhezhao.js"></script>
	<!-- 后台登陆验证 -->
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/login_validate.js"></script>
	<script type='text/javascript' src='http://localhost/Project/Meituan/hdphp/hdphp/Extend/Org/Jquery/jquery-1.8.2.min.js'></script>
<link href='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
HOST = '<?php echo $GLOBALS['user']['HOST'];?>';
ROOT = '<?php echo $GLOBALS['user']['ROOT'];?>';
WEB = '<?php echo $GLOBALS['user']['WEB'];?>';
URL = '<?php echo $GLOBALS['user']['URL'];?>';
HDPHP = '<?php echo $GLOBALS['user']['HDPHP'];?>';
HDPHPDATA = '<?php echo $GLOBALS['user']['HDPHPDATA'];?>';
HDPHPTPL = '<?php echo $GLOBALS['user']['HDPHPTPL'];?>';
HDPHPEXTEND = '<?php echo $GLOBALS['user']['HDPHPEXTEND'];?>';
APP = '<?php echo $GLOBALS['user']['APP'];?>';
CONTROL = '<?php echo $GLOBALS['user']['CONTROL'];?>';
METH = '<?php echo $GLOBALS['user']['METH'];?>';
GROUP = '<?php echo $GLOBALS['user']['GROUP'];?>';
TPL = '<?php echo $GLOBALS['user']['TPL'];?>';
CONTROLTPL = '<?php echo $GLOBALS['user']['CONTROLTPL'];?>';
STATIC = '<?php echo $GLOBALS['user']['STATIC'];?>';
PUBLIC = '<?php echo $GLOBALS['user']['PUBLIC'];?>';
</script>
</head>
<body style="overflow:hidden">
	<div class="login_con">
		<div class="login_title"><p>网站后台登陆</p></div>
		<div class="login_main">
			<div class="form">
				<form name="loginForm" action="<?php echo U('Login/Login');?>" method="post">
					<table>
						<tr>
							<td>用户名：</td>
							<td><input type="text" name="username" /></td>
						</tr>
						<tr>
							<td>密　码：</td>
							<td><input type="password" name="password" /></td>
						</tr>
						<tr>
							<td>验证码：</td>
							<td><input type="text" name="code" class="code" /><img src="<?php echo U('Login/code');?>" alt="点击刷新验证码" width="60" height="25" class="codeimg" title="点击刷新" onclick="this.src=this.src+'&'" /><span id="hd_code"></span></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input type="submit" value="" class="submit" /></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
	<div class="leftDiv"></div>
	<div class="rightDiv"></div>
</body>
</html>