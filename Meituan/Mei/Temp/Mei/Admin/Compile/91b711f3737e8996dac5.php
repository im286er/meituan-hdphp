<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>添加商家位置</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<!-- 百度api -->
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=F3nauaZu1gN3wOvvwUeH9egs"></script>
	<!-- bootstrap 核心js -->
	<script src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<style type="text/css">
		input[type="text"]{
            background-color: #FFFFFF;
            border: 2px solid #DDDDDD;
            border-radius: 0;
            box-shadow: 2px 2px 2px #F0F0F0 inset !important;
            color: #555555;
            display: inline-block;
            font-size: 12px;
            line-height: 30px;
            margin: 0;
            padding: 0 6px;
            vertical-align: middle;
            height: 30px;
        }
	</style>
</head>
<body>
	<div class="pos">添加商家位置</div>
	<form action="<?php echo U('Seller/add_location');?>" name="addForm" method="post">
		<table class="table table-bordered table-hover">
			<tr height="40">
				<td class="" width="150">分店名称：</td>
				<td><input type="text" name="shopname" /></td>
			</tr>
			<tr height="40">
				<td class="" width="150">分店地址：</td>
				<td><input type="text" name="address" /></td>
			</tr>
			<tr height="40">
				<td class="" width="150">位置坐标：</td>
				<td><input type="text" name="coord" /> <input type="button" value="获取坐标" class="coord-btn btn btn-info" /></td>
			</tr>	
			<tr height="40">
				<td class="" width="150">地铁：</td>
				<td><input type="text" name="subway" /></td>
			</tr>	
			<tr height="40">
				<td class="" width="150">电话：</td>
				<td><input type="text" name="tel" /></td>
			</tr>			
					

			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="添加" class="btn btn-primary" /></td>
			</tr>
			<input type="hidden" name="sid" value="<?php echo $_GET['sid'];?>" />
		</table>
	</form>
		<script type="text/javascript">
			 $(function(){
			 	$('.coord-btn').click(function(){
			 		// 获得商家地址
			 		var address = $('input[name=address]').val();

			 		// 创建地址解析器实例
					var myGeo = new BMap.Geocoder();
					// 将地址解析结果显示在地图上,并调整地图视野
					myGeo.getPoint(address, function(point){
					  	 $('input[name=coord]').val(JSON.stringify(point))
					}, "北京市");
			 	})
			 	

			 	
			 })
			
		</script>
</body>
</html>