<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>修改属性</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<!-- bootstrap 核心js -->
	<script src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />

</head>
<body>
	<div class="pos">修改属性</div>
	<form action="<?php echo U('Attr/add');?>" name="addForm" method="post">
		<table class="table table-bordered table-hover">
			<tr>
				<td class="">属性名称</td>
				<td><input type="text" name="attrname" value="<?php echo $attrinfo['attrname'];?>" /></td>
			</tr>

			<tr>
				<td class="">父级</td>
				<td>
					<select name="pid">
						<option value="0">顶级属性</option>
						<?php if(is_array($allAttr)):?><?php  foreach($allAttr as $k=>$v){ ?>
							<option value="<?php echo $attrinfo['atid'];?>"><?php echo $attrinfo['attrvalue'];?></option>
						<?php }?><?php endif;?>
					</select>
				</td>
			</tr>
		
			<tr>
				<td>种类id</td>
				<td><input type="text" value="<?php echo $attrinfo['kindid'];?>" name="kindid" /></td>
			</tr>
			<tr>
				<td>属性值</td>
				<td><textarea cols="30" rows="3" name="attrvalue"></textarea> <span id="tishi">多个值用 | 分开</span></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="添加" class="btn btn-primary" /></td>
			</tr>
		</table>
	</form>
	
</body>
</html>