<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>分类列表</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/base.css" />
	<style type="text/css">
		span.icon{display:inline-block; width:11px; height:11px;  cursor: pointer;}
		span.cid{display: inline-block; width: 15px; text-indent: 2px;}
		.jia{ background: url("http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/images/jia.gif"); }
		.jian{ background: url("http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/images/jian.gif"); }
		.indent .icon{margin-left: 20px;}
		.sonbg{background: #F9F9F9;}
	</style>
	<script type="text/javascript">
		$(function(){
			// 让所有二级分类隐藏
			$('tr[pid!=0]').hide();
			$('.icon').toggle(function(){
				$(this).removeClass('jia').addClass('jian');
				var this_cid = $(this).parents('tr').attr('cid');
				$("tr[pid="+this_cid+"]").show();
			},function(){
				$(this).removeClass('jian').addClass('jia');
				var this_cid = $(this).parents('tr').attr('cid');
				$("tr[pid="+this_cid+"]").hide();
			})
		})
	</script>
</head>
<body>
	<div class="pos">分类列表</div>
	<table class="table table-bordered table-hover">
		<tr pid="0">
			<th width="5%" style="text-align:center">cid</th>
			<th width="25%">分类名称</th>
			<th width="25%" style="text-align:center">所属分类</th>
			<th width="5%" style="text-align:center">是否显示</th>
			<th width="5%" style="text-align:center">排序</th>
			<th width="20%" style="text-align:center">操作</th>
		</tr>
		<?php if(is_array($allCate)):?><?php  foreach($allCate as $k=>$v){ ?>									
			<tr height="30" pid="<?php echo $v['pid'];?>" cid="<?php echo $v['cid'];?>" <?php if($v['pid'] == 0){?> class="sonbg"<?php }?>>
				<td <?php if($v['pid'] <> 0){?>class="indent"<?php }?> style="text-align:center"><span class="icon jia" mark="p<?php echo $v['pid'];?>"></span><span class="cid"><?php echo $v['cid'];?></span></td>
				<td><?php echo $v['_name'];?></td>
				<td style="text-align:center"><?php echo $v['tname'];?></td>
				<td style="text-align:center"><?php if($v['is_show'] == 1){?>是<?php  }else{ ?>否<?php }?></td>
				<td style="text-align:center"><?php echo $v['sort'];?></td>
				<td style="text-align:center"><a href="<?php echo U('Category/add_sonCate',array('cid'=>$v['cid']));?>" class="btn btn-info btn-mini edit">添加子分类</a> <a href="<?php echo U('Category/edit',array('cid'=>$v['cid']));?>" class="btn btn-primary btn-mini edit">修改</a> <a href="javascript:" class="btn btn-danger btn-mini" onclick="if(confirm('确定删除吗？'))location.href='<?php echo U('Category/dele',array('cid'=>$v['cid']));?>'">删除</a></td>
			</tr>
		<?php }?><?php endif;?>
	</table>
	<div class="pagelist"><?php echo $pagelist;?></div>
</body>
</html>