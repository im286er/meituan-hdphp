<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>用户列表</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/base.css" />

</head>
<body>
	<div class="pos">用户列表</div>
	<table class="table table-bordered table-hover">
		<tr>
			<th width="5%" style="text-align:center">uid</th>
			<th width="10%" style="text-align:center">用户名</th>
			<th width="10%" style="text-align:center">昵称</th>
			<th width="15%" style="text-align:center">邮箱</th>
			<th width="15%" style="text-align:center">注册时间</th>
			<th width="15%" style="text-align:center">最后登录时间</th>
			<th width="10%" style="text-align:center">状态</th>
			<th width="20%" style="text-align:center">操作</th>
		</tr>
		<?php if(is_array($data)):?><?php  foreach($data as $k=>$v){ ?>									
			<tr height="40">
				<td style="text-align:center"><?php echo $v['uid'];?></td>
				<td style="text-align:center"><?php echo $v['username'];?></td>
				<td style="text-align:center"><?php echo $v['nick'];?></td>
				<td style="text-align:center"><?php echo $v['email'];?></td>
				<td style="text-align:center"><?php echo hd_date($v['registtime'],'Y-m-d');?></td>
				<td style="text-align:center"><?php echo hd_date($v['logintime'],'Y-m-d');?></td>
				<td style="text-align:center"><?php if($v['is_lock'] == 1){?>锁定<?php  }else{ ?>正常<?php }?></td>
				<td style="text-align:center"><a href="<?php echo U('User/lock',array('uid'=>$v['uid']));?>" class="btn btn-danger btn-small edit"><?php if($v['is_lock']){?>解锁<?php  }else{ ?>锁定<?php }?></a> <a href="javascript:" class="btn btn-primary btn-small" onclick="if(confirm('确定删除吗？'))location.href='<?php echo U('User/del_user',array('uid'=>$v['uid']));?>'">删除</a></td>
			</tr>
		<?php }?><?php endif;?>
	</table>
	<div class="pagelist"><?php echo $pagelist;?></div>
</body>
</html>