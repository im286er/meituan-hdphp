<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>商家位置列表</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/base.css" />
</head>
<body>
	<div class="pos"><span style="float:right;height:30x; display:block;padding:0;"><a class="btn btn-primary" href="<?php echo U('index');?>">返回</a> <a href="<?php echo U('add_location',array('sid'=>$_GET['sid']));?>" class="btn btn-primary">添加位置</a></span>商家位置列表</div>
	<table class="table table-bordered table-hover">
		<tr height="30">
			<th width="5%" style="text-align:center">id</th>
			<th width="35%" style="text-align:center">地址</th>
			<th width="20%" style="text-align:center">电话</th>
			<th width="25%" style="text-align:center">地铁</th>
			<th style="text-align:center">操作</th>
		</tr>
		<?php if(is_array($data)):?><?php  foreach($data as $v){ ?>									
			<tr height="30">
				<td style="text-align:center"><?php echo $v['id'];?></td>
				<td style="text-align:center"><?php echo $v['address'];?></td>
				<td style="text-align:center"><?php echo $v['tel'];?></td>
				<td style="text-align:center"><?php echo $v['subway'];?></td>

				<td style="text-align:center"><a href="<?php echo U('edit_location',array('sid'=>$_GET['sid'],'id'=>$v['id']));?>" class="btn btn-primary btn-mini edit">修改</a> <a href="javascript:" class="btn btn-danger btn-mini" onclick="if(confirm('确定删除吗？'))location.href='<?php echo U('del_location',array('id'=>$v['id']));?>'">删除</a></td>
			</tr>
		<?php }?><?php endif;?>
	</table>
	<div class="pagelist"><?php echo $pagelist;?></div>
</body>
</html>