<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>属性值列表</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<!-- bootstrap 核心js -->
	<script src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/base.css" />
	<script type="text/javascript">
			$(function(){
				$('form[name=edit_form]').hide();

				$('.edit').click(function(){

						if($(this).html() == '保存'){
							$(this).parents('tr').find('form').submit();
						}
						$('.form_value').hide();
						$('.span_value').show();
						$('.edit').html('修改');
						
						$(this).html('保存');
						$(this).parents('tr').find('span').hide();
						$(this).parents('tr').find('form').show();

					
					
				})
			/*	$('.edit').toggle(function(){
					$('.form_value').hide();
					$('.span_value').show();
					$('.edit').html('修改');
					$(this).html('保存');
					$(this).parents('tr').find('span').hide();
					$(this).parents('tr').find('form').show();
				},function(){
					if($(this).html() == '保存'){
						$(this).parents('tr').find('form').submit();
					}
					
				})*/
			})
	</script>
	<style type="text/css">
		input[type="text"]{
            background-color: #FFFFFF;
            border: 2px solid #DDDDDD;
            border-radius: 0;
            box-shadow: 2px 2px 2px #F0F0F0 inset !important;
            color: #555555;
            display: inline-block;
            font-size: 12px;
            line-height: 30px;
            margin: 0;
            padding: 0 6px;
            vertical-align: middle;
            height: 30px;
        }
	</style>
</head>
<body>
	<div class="pos">属性值列表</div>
	<table class="table table-bordered table-hover">
		<tr>
			<th width="5%" style="text-align:center">atid</th>
			<th width="60%">属性值</th>
			<th width="5%">属性名称</th>
			<th width="5%">父级id</th>
			<th style="text-align:center">操作</th>
		</tr>
		<?php if(is_array($attrvalue)):?><?php  foreach($attrvalue as $k=>$v){ ?>									
			<tr height="30">
				<td style="text-align:center"><?php echo $v['atid'];?></td>
				<td>
					<span class="span_value"><?php echo $v['attrvalue'];?></span>
					<form class="form_value" action="<?php echo U('attr_value_edit');?>" name="edit_form" method="post">
						<input type="text" name="attrvalue" value="<?php echo $v['attrvalue'];?>" />
						<input type="hidden" name="atid" value="<?php echo $v['atid'];?>" />
					</form>
				</td>
				<td style="text-align:center"><?php echo $v['attrname'];?></td>
				<td style="text-align:center"><?php echo $v['pid'];?></td>
				<td style="text-align:center"><a href="javascript:" class="btn btn-primary btn-mini edit">修改</a> <a href="javascript:" class="btn btn-danger btn-mini" onclick="if(confirm('确定删除吗？'))location.href='<?php echo U('attr_value_del',array('atid'=>$v['atid']));?>'">删除</a></td>
			</tr>
		<?php }?><?php endif;?>
	</table>
</body>
</html>