<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>添加商家</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<!-- bootstrap 核心js -->
	<script src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<style type="text/css">
		input[type="text"]{
            background-color: #FFFFFF;
            border: 2px solid #DDDDDD;
            border-radius: 0;
            box-shadow: 2px 2px 2px #F0F0F0 inset !important;
            color: #555555;
            display: inline-block;
            font-size: 12px;
            line-height: 30px;
            margin: 0;
            padding: 0 6px;
            vertical-align: middle;
            height: 30px;
        }
	</style>
</head>
<body>
	<div class="pos">添加商家</div>
	<form action="<?php echo U('Seller/add');?>" name="addForm" method="post">
		<table class="table table-bordered table-hover">
			<tr height="40">
				<td class="" width="150">商家名称：</td>
				<td><input type="text" name="sname" /></td>
			</tr>
			<tr height="40">
				<td class="" width="150">商家介绍</td>
				<td><script charset="utf-8" src="http://localhost/Project/Meituan/hdphp/hdphp/Extend/Org/Keditor/kindeditor-all-min.js"></script>
            <script charset="utf-8" src="http://localhost/Project/Meituan/hdphp/hdphp/Extend/Org/Keditor/lang/zh_CN.js"></script>
        <textarea id="hd_introduction" name="introduction"></textarea>
        <script>
        var options_introduction = {
        filterMode : false
                ,id : "editor_id"
        ,width : "800px"
        ,height:"300px"
                ,formatUploadUrl:false
        ,allowFileManager:false
        ,allowImageUpload:true
        ,afterBlur: function(){this.sync();}
        ,uploadJson : "http://localhost/Project/Meituan/index.php?a=Admin&c=Seller&m=keditor_upload&g=Mei&water=1&uploadsize=2000000&maximagewidth=false&maximageheight=false&PHPSESSID=ah0tjjigmhju3nqt42v56dv1k1"
        };var hd_introduction;
        KindEditor.ready(function(K) {
                    hd_introduction = KindEditor.create("#hd_introduction",options_introduction);
        });
        </script>
        </td>
			</tr>			

			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="添加" class="btn btn-primary" /></td>
			</tr>
		</table>
	</form>
</body>
</html>