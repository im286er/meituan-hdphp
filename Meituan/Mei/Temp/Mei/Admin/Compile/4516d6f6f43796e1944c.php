<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>添加商品</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type='text/javascript' src='http://localhost/Project/Meituan/hdphp/hdphp/Extend/Org/Jquery/jquery-1.8.2.min.js'></script>
<link href='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
HOST = '<?php echo $GLOBALS['user']['HOST'];?>';
ROOT = '<?php echo $GLOBALS['user']['ROOT'];?>';
WEB = '<?php echo $GLOBALS['user']['WEB'];?>';
URL = '<?php echo $GLOBALS['user']['URL'];?>';
HDPHP = '<?php echo $GLOBALS['user']['HDPHP'];?>';
HDPHPDATA = '<?php echo $GLOBALS['user']['HDPHPDATA'];?>';
HDPHPTPL = '<?php echo $GLOBALS['user']['HDPHPTPL'];?>';
HDPHPEXTEND = '<?php echo $GLOBALS['user']['HDPHPEXTEND'];?>';
APP = '<?php echo $GLOBALS['user']['APP'];?>';
CONTROL = '<?php echo $GLOBALS['user']['CONTROL'];?>';
METH = '<?php echo $GLOBALS['user']['METH'];?>';
GROUP = '<?php echo $GLOBALS['user']['GROUP'];?>';
TPL = '<?php echo $GLOBALS['user']['TPL'];?>';
CONTROLTPL = '<?php echo $GLOBALS['user']['CONTROLTPL'];?>';
STATIC = '<?php echo $GLOBALS['user']['STATIC'];?>';
PUBLIC = '<?php echo $GLOBALS['user']['PUBLIC'];?>';
HISTORY = '<?php echo $GLOBALS['user']['HISTORY'];?>';
HTTPREFERER = '<?php echo $GLOBALS['user']['HTTPREFERER'];?>';
</script>
	<!-- bootstrap 核心js -->
	<script src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<style type="text/css">
		input[type="text"]{
            background-color: #FFFFFF;
            border: 2px solid #DDDDDD;
            border-radius: 0;
            box-shadow: 2px 2px 2px #F0F0F0 inset !important;
            color: #555555;
            display: inline-block;
            font-size: 12px;
            line-height: 30px;
            margin: 0;
            padding: 0 6px;
            vertical-align: middle;
            height: 30px;
        }
	</style>
</head>
<body>
	<div class="pos">添加商品</div>
	<form action="<?php echo U('add2_save');?>" name="addForm" method="post">
		<table class="table table-bordered table-hover">

			<tr height="30">
				<td class="" width="150">主标题：</td>
				<td><input type="text" name="title" /></td>
			</tr>
			<tr height="30">
				<td class="" width="150">副标题：</td>
				<td><input type="text" name="subtitle" /></td>
			</tr>
			<tr height="30">
				<td class="" width="150">原价：</td>
				<td><input type="text" name="price" /><span class="tishi">元</span></td>
			</tr>
			<tr height="30">
				<td class="" width="150">团购价：</td>
				<td><input type="text" name="group_price" /><span class="tishi">元</span></td>
			</tr>
			<?php if(is_array($attrArr)):?><?php  foreach($attrArr as $k=>$v){ ?>
			<tr height="30">
				<td class="" width="150"><?php echo $v['attrname'];?>：</td>
				<td>
					<?php if(is_array($v['value'])):?><?php  foreach($v['value'] as $key=>$value){ ?>
					<label class="radio inline"><input type="radio" name="atid[<?php echo $k;?>]" value="<?php echo $key;?>"> <?php echo $value;?></label> 
					<?php }?><?php endif;?>
				</td>
			</tr>
			<?php }?><?php endif;?>
			<tr height="30">
				<td class="" width="150">截止日期：</td>
				<td>
				  <input type="text" readonly="readonly" id="updatetime" name="close_date"
				  value="<?php echo date('Y-m-d');?>" class="w150"/>
 				 <script>
				    $('#updatetime').calendar({format: 'yyyy-MM-dd'});
				 </script>
				</td>
			</tr>
			<tr height="30">
				<td class="" width="150">不可用日期：</td>
				<td><input type="text" name="not_use_date" /></td>
			</tr>
			<tr height="30">
				<td class="" width="150">使用时间：</td>
				<td><input type="text" name="use_time" /></td>
			</tr>
			<tr height="30">
				<td class="" width="150">服务承诺：</td>
				<td>
				<label class="checkbox inline"><input type="checkbox" name="promise[]" value="1" /> 随时退</label>
				<label class="checkbox inline"><input type="checkbox" name="promise[]" value="2" /> 过期退</label>
				<label class="checkbox inline"><input type="checkbox" name="promise[]" value="3" /> 急速退</label>
				<label class="checkbox inline"><input type="checkbox" name="promise[]" value="4" /> 真实评价</label>
				</td>
			</tr>
			<tr height="30">
				<td class="" width="150">预约提醒：</td>
				<td><input type="text" name="appointment" /></td>
			</tr>
			<tr height="30">
				<td class="" width="150">温馨提示：</td>
				<td><textarea name="reminder"></textarea></td>
			</tr>
			<tr height="30">
				<td class="" width="150">重要通知：</td>
				<td><textarea name="notice"></textarea></td>
			</tr>
			<tr height="30">
				<td class="" width="150">商家服务：</td>
				<td><input type="text" name="service" /></td>
			</tr>
			<tr height="30">
				<td class="" width="150">商品图片：</td>
				<td><link rel="stylesheet" type="text/css" href="http://localhost/Project/Meituan/hdphp/hdphp/Extend/Org/Uploadify/uploadify.css" />
            <script type="text/javascript" src="http://localhost/Project/Meituan/hdphp/hdphp/Extend/Org/Uploadify/jquery.uploadify.min.js"></script>
            <script type="text/javascript">
            var HDPHP_CONTROL         = "http://localhost/Project/Meituan/index.php?a=Admin&c=Goods&m=keditor_upload&g=Mei";
            var UPLOADIFY_URL    = "http://localhost/Project/Meituan/hdphp/hdphp/Extend/Org/Uploadify/";
            var HDPHP_UPLOAD_THUMB    ="";
HDPHP_UPLOAD_TOTAL = 0</script>
            <script type="text/javascript" src="http://localhost/Project/Meituan/hdphp/hdphp/Extend/Org/Uploadify/hd_uploadify.js"></script>
<script type="text/javascript">
    $(function() {
        hd_uploadify_options.removeTimeout  =0;
        hd_uploadify_options.fileSizeLimit  ="5MB";
        hd_uploadify_options.fileTypeExts   ="*.jpg;*.png;*.gif";
        hd_uploadify_options.queueID        ="hd_uploadify_pic_queue";
        hd_uploadify_options.showalt        =false;
        hd_uploadify_options.uploadLimit    =5;
        hd_uploadify_options.input_type    ="input";
        hd_uploadify_options.elem_id    ="";
        hd_uploadify_options.upload_img_max_width    ="1000";
        hd_uploadify_options.upload_img_max_    ="1000";
        hd_uploadify_options.success_msg    ="正在上传...";//上传成功提示文字
        hd_uploadify_options.formData ={water : "0",upload_img_max_width:"1000",upload_img_max_height:"1000",fileSizeLimit:5242880, someOtherKey:1,PHPSESSID:"k1en353gctb556ne6qli3fpe87",upload_dir:"",hdphp_upload_thumb:""};
        hd_uploadify_options.thumb_width =200;
        hd_uploadify_options.thumb_height          =150;
        hd_uploadify_options.uploadsSuccessNums = 0;
        $("#hd_uploadify_pic").uploadify(hd_uploadify_options);
        });
</script>
<input type="file" name="up" id="hd_uploadify_pic"/>
<div class="hd_uploadify_pic_msg num_upload_msg" style="display:block">
<input type="checkbox" id="add_upload_water" uploadify_id="hd_uploadify_pic" /><strong style="color:#03565E">是否添加水印</strong><span></span>单文件最大<strong>5MB，允许上传类型*.jpg;*.png;*.gif</strong>
</div>

<div id="hd_uploadify_pic_queue"></div>
<div class="hd_uploadify_pic_files uploadify_upload_files" input_file_id ="hd_uploadify_pic">
    <ul></ul>
    <div style="clear:both;"></div>
</div></td>
			</tr>

			<tr height="30">
				<td class="" width="150">本单详情：</td>
				<td><script charset="utf-8" src="http://localhost/Project/Meituan/hdphp/hdphp/Extend/Org/Keditor/kindeditor-all-min.js"></script>
            <script charset="utf-8" src="http://localhost/Project/Meituan/hdphp/hdphp/Extend/Org/Keditor/lang/zh_CN.js"></script>
        <textarea id="hd_details" name="details"></textarea>
        <script>
        var options_details = {
        filterMode : false
                ,id : "editor_id"
        ,width : "800px"
        ,height:"300px"
                ,formatUploadUrl:false
        ,allowFileManager:false
        ,allowImageUpload:true
        ,afterBlur: function(){this.sync();}
        ,uploadJson : "http://localhost/Project/Meituan/index.php?a=Admin&c=Goods&m=keditor_upload&g=Mei&water=1&uploadsize=2000000&maximagewidth=false&maximageheight=false&PHPSESSID=k1en353gctb556ne6qli3fpe87"
        };var hd_details;
        KindEditor.ready(function(K) {
                    hd_details = KindEditor.create("#hd_details",options_details);
        });
        </script>
        </td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="添加" class="btn btn-primary" /></td>
			</tr>
		</table>
		<input type="hidden" name="cid" value="<?php echo $step1['cid'];?>" />
		<input type="hidden" name="sid" value="<?php echo $step1['sid'];?>" />
	</form>
</body>
</html>