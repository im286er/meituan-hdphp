<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>后台内容管理系统</title>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/base.css" />
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/index.css" />
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/left_menu.js"></script>
	<base target="iframe" />
<body>
<!-- 头部 -->
	<div id="top">
		<div class="top-logo"><img src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/images/logo.png" /></div>
		<div class="top-link">
			<ul>
				<li>您好： <?php echo $_SESSION['aname'];?>，欢迎回来！</li>
				<li><a href="<?php echo U('Index/Index/index');?>" target="_blank">网站主页</a></li>
				<li><a href="<?php echo U('Login/quit');?>" target="_self">退出登陆</a></li>
			</ul>
		</div>
	</div>
<!-- 主体 -->
	<div id="main">
<!-- 左侧栏目 -->
		<div id="left">
			<dl class="left_menu">
				<dt>属性管理</dt>
				<dd><a href="<?php echo U('Attr/index');?>">属性列表</a></dd>
				<dd><a href="<?php echo U('Attr/add');?>">添加属性</a></dd>	
			</dl>
			<dl class="left_menu">
				<dt>类型管理</dt>
				<dd><a href="<?php echo U('Type/index');?>">类型列表</a></dd>
				<dd><a href="<?php echo U('Type/add');?>">添加类型</a></dd>	
			</dl>
			<dl class="left_menu">
				<dt>分类管理</dt>
				<dd><a href="<?php echo U('Category/index');?>">分类列表</a></dd>
				<dd><a href="<?php echo U('Category/add');?>">添加分类</a></dd>
			</dl>
			<dl class="left_menu">
				<dt>商家管理</dt>
				<dd><a href="<?php echo U('Seller/index');?>">商家列表</a></dd>
				<dd><a href="<?php echo U('Seller/add');?>">添加商家</a></dd>
			</dl>
			<dl class="left_menu">
				<dt>商品管理</dt>
				<dd><a href="<?php echo U('Goods/index');?>">商品列表</a></dd>
				<dd><a href="<?php echo U('Goods/add_1');?>">添加商品</a></dd>
			</dl>
			<dl class="left_menu">
				<dt>订单管理</dt>
				<dd><a href="<?php echo U('Order/index');?>">订单列表</a></dd>
			</dl>
			<dl class="left_menu">
				<dt>用户管理</dt>
				<dd><a href="<?php echo U('User/index');?>">用户列表</a></dd>
			</dl>
			<!-- <dl class="left_menu">
				<dt>系统管理</dt>
				<dd><a href="<?php echo U('Admin/index');?>">管理员列表</a></dd>
			</dl> -->
		</div>
<!-- 内容区 -->
		<div id="right">
			<iframe src="<?php echo U('Index/home');?>" width="100%" height="100%" frameborder="0" height="100%" name="iframe"></iframe>
		</div>
	</div>
</body>
</html>