<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>商品列表</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/base.css" />

</head>
<body>
	<div class="pos">商品列表</div>
	<table class="table table-bordered table-hover">
		<tr>
			<th width="5%" style="text-align:center">gid</th>
			<th width="30%" style="text-align:center">商品名称</th>
			<th width="20%" style="text-align:center">所属商家</th>
			<th width="15%" style="text-align:center">所属分类</th>
			<th style="text-align:center">操作</th>
		</tr>
		<?php if(is_array($allGoods)):?><?php  foreach($allGoods as $k=>$v){ ?>									
			<tr height="40">
				<td style="text-align:center"><?php echo $v['gid'];?></td>
				<td style="text-align:center"><?php echo $v['title'];?></td>
				<td style="text-align:center"><?php echo $v['sname'];?></td>
				<td style="text-align:center"><?php echo $v['cname'];?></td>
				<td style="text-align:center"><a href="<?php echo U('Index/Article/index',array('gid'=>$v['gid']));?>" target="_blank" class="btn btn-info btn-small">预览</a> <a href="<?php echo U('edit_1',array('gid'=>$v['gid']));?>" class="btn btn-primary btn-small edit">修改</a> <a href="javascript:" class="btn btn-danger btn-small" onclick="if(confirm('确定删除吗？'))location.href='<?php echo U('del_data',array('gid'=>$v['gid']));?>'">删除</a></td>
			</tr>
		<?php }?><?php endif;?>
	</table>
	<div class="pagelist"><?php echo $pagelist;?></div>
</body>
</html>