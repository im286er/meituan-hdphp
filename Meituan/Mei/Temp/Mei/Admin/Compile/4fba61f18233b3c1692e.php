<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>修改分类</title>
	<!-- bootstrap 核心css -->
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/js/jquery-1.7.2.min.js"></script>
	<!-- bootstrap 核心js -->
	<script src="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="http://localhost/Project/Meituan/./Mei/Mei/Admin/Tpl/Public/Common/css/main.css" />
	<style type="text/css">
		input[type="text"]{
            background-color: #FFFFFF;
            border: 2px solid #DDDDDD;
            border-radius: 0;
            box-shadow: 2px 2px 2px #F0F0F0 inset !important;
            color: #555555;
            display: inline-block;
            font-size: 12px;
            line-height: 30px;
            margin: 0;
            padding: 0 6px;
            vertical-align: middle;
            height: 30px;
        }
	</style>
</head>
<body>
	<div class="pos">修改分类</div>
	<form action="<?php echo U('Category/edit');?>" name="addForm" method="post">
		<table class="table table-bordered table-hover">
			<tr height="40">
				<td class="" width="150">分类名称：</td>
				<td><input type="text" name="cname" value="<?php echo $data['cname'];?>" /></td>
			</tr>
			<tr height="40">
				<td class="" width="150">分类标题：</td>
				<td><input type="text" name="title" value="<?php echo $data['title'];?>" /></td>
			</tr>			
			<tr height="40">
				<td class="" width="150">分类关键字：</td>
				<td><input type="text" name="keywords" value="<?php echo $data['keywords'];?>" /></td>
			</tr>
			<tr>
				<td>所属类型：</td>
				<td>
					<select name="tid">
						<?php if(is_array($allType)):?><?php  foreach($allType as $v){ ?>
							<option value="<?php echo $v['tid'];?>" <?php if($v['tid'] == $data['tid']){?>selected="selected"<?php }?>><?php echo $v['tname'];?></option>
						<?php }?><?php endif;?>
					</select>		
				</td>
			</tr>
			<tr height="40">
				<td class="" width="150">是否显示：</td>
				<td>
				<label class="radio inline"><input type="radio" name="is_show" <?php if($data['is_show'] == 1){?>checked="checked"<?php }?> value="1" /> 显示</label>
				<label class="radio inline"><input type="radio" name="is_show" <?php if($data['is_show'] == 0){?>checked="checked"<?php }?> value="0" /> 隐藏</label>
				</td>
			</tr>
			<tr height="40">
				<td class="" width="150">排序：</td>
				<td><input type="text" name="sort" value="<?php echo $data['sort'];?>" /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="修改" class="btn btn-primary" /></td>
			</tr>
		</table>
		<input type="hidden" name="cid" value="<?php echo $data['cid'];?>" />
	</form>
</body>
</html>