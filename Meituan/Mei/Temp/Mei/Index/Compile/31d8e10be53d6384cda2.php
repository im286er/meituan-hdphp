<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>美团用户登陆页</title>
	<link rel="stylesheet" type="text/css" href="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/css/base.css" />
	<link rel="stylesheet" type="text/css" href="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/css/login.css" />
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/login_validate.js"></script>
	<!-- 为了保证IE载入以下文件 -->
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/es5-sham.min.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/es5-shim.min.js"></script>
	<!-- 载入less.js -->
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/less-1.5.0.min.js"></script>
	<script type='text/javascript' src='http://localhost/Project/Meituan/hdphp/hdphp/Extend/Org/Jquery/jquery-1.8.2.min.js'></script>
<link href='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/Project/Meituan/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
HOST = '<?php echo $GLOBALS['user']['HOST'];?>';
ROOT = '<?php echo $GLOBALS['user']['ROOT'];?>';
WEB = '<?php echo $GLOBALS['user']['WEB'];?>';
URL = '<?php echo $GLOBALS['user']['URL'];?>';
HDPHP = '<?php echo $GLOBALS['user']['HDPHP'];?>';
HDPHPDATA = '<?php echo $GLOBALS['user']['HDPHPDATA'];?>';
HDPHPTPL = '<?php echo $GLOBALS['user']['HDPHPTPL'];?>';
HDPHPEXTEND = '<?php echo $GLOBALS['user']['HDPHPEXTEND'];?>';
APP = '<?php echo $GLOBALS['user']['APP'];?>';
CONTROL = '<?php echo $GLOBALS['user']['CONTROL'];?>';
METH = '<?php echo $GLOBALS['user']['METH'];?>';
GROUP = '<?php echo $GLOBALS['user']['GROUP'];?>';
TPL = '<?php echo $GLOBALS['user']['TPL'];?>';
CONTROLTPL = '<?php echo $GLOBALS['user']['CONTROLTPL'];?>';
STATIC = '<?php echo $GLOBALS['user']['STATIC'];?>';
PUBLIC = '<?php echo $GLOBALS['user']['PUBLIC'];?>';
HISTORY = '<?php echo $GLOBALS['user']['HISTORY'];?>';
HTTPREFERER = '<?php echo $GLOBALS['user']['HTTPREFERER'];?>';
</script>
</head>
<body>
	<div id="main">
		<div id="logo"><a href="http://localhost/Project/Meituan"></a></div>
		<div id="left">
			<div class="adv"><a href=""><img src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/images/loginimg.jpg" alt="" /></a></div>
		</div>
		<div id="right">
			<p>账号登陆</p>
			<form action="" method="post">
				<div class="int"><span class="icon-user"></span><input type="text" name="username"placeholder="用户名/邮箱" /></div>
				<span id="hd_username"></span>
				<div class="int"><span class="icon-lock"></span><input type="password" name="password"placeholder="密码" /></div>
				<span id="hd_password"></span>
				<div class="check">
					<a href="">忘记密码</a>
					<input type="checkbox" name="remember" /> 记住账号
					<input type="checkbox" name="autoLogin" /> 下次自动登陆
				</div>
				<div class="btn"><input type="submit" value="登陆" /></div>
				<div class="go-regist">还没有账号？<a href="<?php echo U('regist/index');?>">免费注册</a></div>
				<div class="else_login">
					<p>用合作网站账号登陆</p>
					<ul>
						<li><a href="" class="qq_login"></a>QQ登陆</li>
					</ul>
				</div>
			</form>
		</div>
	</div>
</body>
</html>