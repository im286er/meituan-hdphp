<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?>	<!-- 头部开始 -->
	<div id="top">
		<div id="top-con2">
			<div class="top-left">
				<div class="top-left1">
					<?php if(isset($_SESSION['username'])){?>
						<span class="userinfo">hi,<a href="<?php echo U('Member/order');?>" class="username"><?php echo $_SESSION['username'];?></a> <a href="<?php echo U('Login/quit');?>" class="quit">退出</a></span>
						<?php  }else{ ?>
						<span>美团一次 美一次</span>
						<span><a class="red" href="<?php echo U('Login/index');?>">登陆</a></span>
						<span><a href="<?php echo U('Regist/index');?>">注册</a></span>
						<!-- <span>|</span> -->
					<?php }?>
				</div>
			</div>
			<div class="top-right" >
				<ul>
					<li class="top-right-li"><p class="top-right-p"><a href="<?php echo U('Member/order');?>">我的订单</a></p></li>
					<li class="top-right-li have-son">
						<p class="top-right-p"><a class="top-right-menu" href="<?php echo U('Member/order');?>">我的美团</a><i class="sj"></i><i class="shu"></i></p>
						
						<div class="top-right-son mymt-pos">
							<ul class="mymt">
								<li><a href="<?php echo U('Member/order');?>">我的订单</a></li>
								<li><a href="<?php echo U('Member/collect');?>">我的收藏</a></li>
								<li><a href="<?php echo U('Member/basicInfo');?>">基本信息</a></li>
								<li><a href="<?php echo U('Member/userinfo');?>">个人资料</a></li>
								<li><a href="<?php echo U('Member/address');?>">收货地址</a></li>
							</ul>
						</div>
						
					</li>
					<li class="top-right-li have-son">
						<p class="top-right-p"><a class="top-right-menu" href="">最近浏览</a><i class="sj"></i><i class="shu"></i></p>
						
						<div class="top-right-son viewed-pos">
							<?php if(is_null($_SESSION['viewed'])){?>
							<p class="wu">暂无浏览记录</p>
							<?php  }else{ ?>
							<ul class="viewed">
								<?php if(is_array($_SESSION['viewed'])):?><?php  foreach($_SESSION['viewed'] as $k=>$v){ ?>
								<li>
									<div class="tu"><a href="<?php echo U('Article/index',array('gid'=>$k));?>"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="80" height="49" /></a></div>
									<div class="info">
										<p><a href=""><?php echo $v['title'];?></a></p>
										<p class="p2"><span>￥<?php echo $v['group_price'];?></span><del><?php echo $v['price'];?></del></p>
									</div>
								</li>
							<?php }?><?php endif;?>
							</ul>
							<p class="unset-viewed"><a  href="javascript:" class="del_viewed" style="color:#2bb8aa">清空最近浏览记录</a></p>
							<?php }?>
						</div>
						</li>
						<!-- 商品购物车开始 -->
					<li class="top-right-li have-son">
						<p class="top-right-p gwche"><i class="che"></i><a class="top-right-menu" href="">购物车<span class="gwche-num"><?php echo count($cart);?></span>件</a><i class="sj"></i><i class="shu"></i></p>
					
						<div class="top-right-son gwche-pos" id="cart">
							<?php if(is_null($cart)){?>
							<p class="wu">暂没有加入购物车的商品</p>
							<?php  }else{ ?>
							<ul class="viewed">
								<?php if(is_array($cart)):?><?php  foreach($cart as $v){ ?>
								<li class="top_cartlist">
									<div class="tu"><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="80" height="49" /></a></div>
									<div class="info">
										<p><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>"><?php echo $v['title'];?></a></p>
										<p class="p2"><span>￥<?php echo $v['group_price'];?></span><a class="del top_del_cart" href="javascript:" gid="<?php echo $v['gid'];?>">删除</a></p>
									</div>
								</li>
							<?php }?><?php endif;?>
							</ul>

							<p class="go-gwche"><a href="<?php echo U('Cart/cart1');?>">查看我的购物车</a></p>
							<?php }?>
						</div>
		
						</li>
						<!-- 商品购物车结束 -->
					<li class="top-right-li have-son">
					<p class="top-right-p"><a class="top-right-menu" href="">联系客服</a><i class="sj"></i><i class="shu"></i></p>
					<div class="top-right-son contact-pos">
							<ul class="mymt">
								<li><a href="">申请退款</a></li>
								<li><a href="">申请退换货</a></li>
								<li><a href="">常见问题</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<!-- 头部结束 -->
<!-- logo 和 搜索开始 -->
	<div id="logo-sch-bao2">
		<div class="logo-piece logo-piece-w240">
			<a href="http://localhost/Project/Meituan" id="logo"></a>
			<span class="city">北京</span>
			<a class="choose-city" href="">切换城市</a>
		</div>
		<div class="sch-piece sch-piece-w">
			<div class="sch">
				<div class="form">
				<form name="sch_form" action="<?php echo U('Search/index');?>" method="post">
					<div class="sch-left sch-left-w">
						<span class="sj"></span>
						<ul class="tuan-shang">
							<li>团购</li>
							<li>商家</li>
						</ul>
						<input class="search search-w" type="text" name="search" placeholder="请输入商品名称" />
					</div>
					<input class="btn" name="sch-btn" type="submit" value="搜 索" />
					</form>
				</div>
			</div>
			<div class="clear"></div>
			<!-- <div class="hot-words">
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
			</div> -->
		</div>
		<div class="bao"></div>
		<div class="clear"></div>
	</div>
<!-- logo 和 搜素结束 -->

<!-- 菜单部分开始 -->
<div id="menu">
	<div class="menu-l">
		<p class="cate-top">全部分类<i></i></p>
		<div class="cate-son-con">
			
			<?php if(is_array($allCate)):?><?php  foreach($allCate as $k=>$v){ ?>
			<div class="cate-list">
				<div class="cate-left-con">
					<h3 class="top-cate"><a href="<?php echo U('List/index',array('cid'=>$k));?>"><?php echo $v['parent'];?></a></h3>
					<p class="son-cate">
						<?php $son = array_slice($v['son'], 0,3,true); ?>
						<?php if(is_array($son)):?><?php  foreach($son as $key=>$value){ ?>
						<a href="<?php echo U('List/index',array('cid'=>$key));?>"><?php echo $value;?></a>
						<?php }?><?php endif;?>
					</p>
					<i class="right-arrow"></i>
				</div>
				<div class="cate-xian"></div>
				<div class="cate-right-con">
					<h3 class="right-top-cate"><a href=""><?php echo $v['parent'];?></a></h3>
					<p class="right-cate-son">
						<?php if(is_array($v['son'])):?><?php  foreach($v['son'] as $key=>$v){ ?>
						<a href="<?php echo U('List/index',array('cid'=>$key));?>"><?php echo $v;?></a>
						<?php }?><?php endif;?>
					</p>
				</div>
			</div>
			<?php }?><?php endif;?>

		</div>
	</div>
	<div class="menu-r">
		<ul>
			<li><a href="">首页</a></li>
			<li><a href="">身边团购</a></li>
			<li><a href="">身边外卖</a></li>
			<li><a href="">今日新单</a></li>
			<li><a href="">购物</a></li>
			<li><a href="">商家点评</a></li>
			<li><a href="">大牌街</a></li>
		</ul>
	</div>
	<div class="clear"></div>
</div>
<!-- 菜单部分结束 -->