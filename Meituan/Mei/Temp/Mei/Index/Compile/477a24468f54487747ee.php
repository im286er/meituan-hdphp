<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>内容页</title>

	<link rel="stylesheet" type="text/css" href="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/css/base.css" />
	<link rel="stylesheet" type="text/css" href="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/css/article.css" />
	<script type="text/javascript">
		var APP = '<?php echo "http://localhost/Project/Meituan/index.php/Index" ;?>';
	</script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/head.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/menu.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/article.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/common.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/right.js"></script>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=F3nauaZu1gN3wOvvwUeH9egs"></script>
	<!-- 为了保证IE载入以下文件 -->
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/es5-sham.min.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/es5-shim.min.js"></script>
	<!-- 载入less.js -->
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/less-1.5.0.min.js"></script>
	
</head>
<body>
<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?>	<!-- 头部开始 -->
	<div id="top">
		<div id="top-con2">
			<div class="top-left">
				<div class="top-left1">
					<?php if(isset($_SESSION['username'])){?>
						<span class="userinfo">hi,<a href="<?php echo U('Member/order');?>" class="username"><?php echo $_SESSION['username'];?></a> <a href="<?php echo U('Login/quit');?>" class="quit">退出</a></span>
						<?php  }else{ ?>
						<span>美团一次 美一次</span>
						<span><a class="red" href="<?php echo U('Login/index');?>">登陆</a></span>
						<span><a href="<?php echo U('Regist/index');?>">注册</a></span>
						<!-- <span>|</span> -->
					<?php }?>
				</div>
			</div>
			<div class="top-right" >
				<ul>
					<li class="top-right-li"><p class="top-right-p"><a href="<?php echo U('Member/order');?>">我的订单</a></p></li>
					<li class="top-right-li have-son">
						<p class="top-right-p"><a class="top-right-menu" href="<?php echo U('Member/order');?>">我的美团</a><i class="sj"></i><i class="shu"></i></p>
						
						<div class="top-right-son mymt-pos">
							<ul class="mymt">
								<li><a href="<?php echo U('Member/order');?>">我的订单</a></li>
								<li><a href="<?php echo U('Member/collect');?>">我的收藏</a></li>
								<li><a href="<?php echo U('Member/basicInfo');?>">基本信息</a></li>
								<li><a href="<?php echo U('Member/userinfo');?>">个人资料</a></li>
								<li><a href="<?php echo U('Member/address');?>">收货地址</a></li>
							</ul>
						</div>
						
					</li>
					<li class="top-right-li have-son">
						<p class="top-right-p"><a class="top-right-menu" href="">最近浏览</a><i class="sj"></i><i class="shu"></i></p>
						
						<div class="top-right-son viewed-pos">
							<?php if(is_null($_SESSION['viewed'])){?>
							<p class="wu">暂无浏览记录</p>
							<?php  }else{ ?>
							<ul class="viewed">
								<?php if(is_array($_SESSION['viewed'])):?><?php  foreach($_SESSION['viewed'] as $k=>$v){ ?>
								<li>
									<div class="tu"><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="80" height="49" /></a></div>
									<div class="info">
										<p><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><?php echo $v['title'];?></a></p>
										<p class="p2"><span>￥<?php echo $v['group_price'];?></span><del><?php echo $v['price'];?></del></p>
									</div>
								</li>
							<?php }?><?php endif;?>
							</ul>
							<p class="unset-viewed"><a  href="javascript:" class="del_viewed" style="color:#2bb8aa">清空最近浏览记录</a></p>
							<?php }?>
						</div>
						</li>
						<!-- 商品购物车开始 -->
					<li class="top-right-li have-son">
						<p class="top-right-p gwche"><i class="che"></i><a class="top-right-menu" href="">购物车<span class="gwche-num"><?php echo count($cart);?></span>件</a><i class="sj"></i><i class="shu"></i></p>
					
						<div class="top-right-son gwche-pos" id="cart">
							<?php if(is_null($cart)){?>
							<p class="wu">暂没有加入购物车的商品</p>
							<?php  }else{ ?>
							<ul class="viewed">
								<?php if(is_array($cart)):?><?php  foreach($cart as $v){ ?>
								<li class="top_cartlist">
									<div class="tu"><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="80" height="49" /></a></div>
									<div class="info">
										<p><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>"><?php echo $v['title'];?></a></p>
										<p class="p2"><span>￥<?php echo $v['group_price'];?></span><a class="del top_del_cart" href="javascript:" gid="<?php echo $v['gid'];?>">删除</a></p>
									</div>
								</li>
							<?php }?><?php endif;?>
							</ul>

							<p class="go-gwche"><a href="<?php echo U('Cart/cart1');?>">查看我的购物车</a></p>
							<?php }?>
						</div>
		
						</li>
						<!-- 商品购物车结束 -->
				<!-- 	<li class="top-right-li have-son">
				<p class="top-right-p"><a class="top-right-menu" href="">联系客服</a><i class="sj"></i><i class="shu"></i></p>
				<div class="top-right-son contact-pos">
						<ul class="mymt">
							<li><a href="">申请退款</a></li>
							<li><a href="">申请退换货</a></li>
							<li><a href="">常见问题</a></li>
						</ul>
					</div>
				</li> -->
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<!-- 头部结束 -->
<!-- logo 和 搜索开始 -->
	<div id="logo-sch-bao2">
		<div class="logo-piece logo-piece-w240">
			<a href="http://localhost/Project/Meituan" id="logo"></a>
			<span class="city">北京</span>
			<a class="choose-city" href="">切换城市</a>
		</div>
		<div class="sch-piece sch-piece-w">
			<div class="sch">
				<div class="form">
				<form name="sch_form" action="<?php echo U('Search/index');?>" method="post">
					<div class="sch-left sch-left-w">
						<span class="sj"></span>
						<ul class="tuan-shang">
							<li>团购</li>
							<!-- <li>商家</li> -->
						</ul>
						<input class="search search-w" type="text" name="search" placeholder="请输入商品名称" />
					</div>
					<input class="btn" name="sch-btn" type="submit" value="搜 索" />
					</form>
				</div>
			</div>
			<div class="clear"></div>
			<!-- <div class="hot-words">
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
			</div> -->
		</div>
		<div class="bao"></div>
		<div class="clear"></div>
	</div>
<!-- logo 和 搜素结束 -->

<!-- 菜单部分开始 -->
<div id="menu">
	<div class="menu-l">
		<p class="cate-top">全部分类<i></i></p>
		<div class="cate-son-con">
			
			<?php if(is_array($allCate)):?><?php  foreach($allCate as $k=>$v){ ?>
			<div class="cate-list">
				<div class="cate-left-con">
					<h3 class="top-cate"><a href="<?php echo U('List/index',array('cid'=>$k));?>"><?php echo $v['parent'];?></a></h3>
					<p class="son-cate">
						<?php 
							if($v['parent'] == '酒店' || $v['parent'] == '电影'){
								$son = array_slice($v['son'], 0,2,true); 
							}else{
								$son = array_slice($v['son'], 0,3,true); 
							}
						?>
						<?php if(is_array($son)):?><?php  foreach($son as $key=>$value){ ?>
						<a href="<?php echo U('List/index',array('cid'=>$key));?>"><?php echo $value;?></a>
						<?php }?><?php endif;?>
					</p>
					<i class="right-arrow"></i>
				</div>
				<div class="cate-xian"></div>
				<div class="cate-right-con">
					<h3 class="right-top-cate"><a href=""><?php echo $v['parent'];?></a></h3>
					<p class="right-cate-son">
						<?php if(is_array($v['son'])):?><?php  foreach($v['son'] as $key=>$v){ ?>
						<a href="<?php echo U('List/index',array('cid'=>$key));?>"><?php echo $v;?></a>
						<?php }?><?php endif;?>
					</p>
				</div>
			</div>
			<?php }?><?php endif;?>

		</div>
	</div>
	<div class="menu-r">
		<ul>
			<li><a href="http://localhost/Project/Meituan">首页</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>1));?>">美食</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>2));?>">酒店</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>3));?>">电影</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>4));?>">休闲娱乐</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>5));?>">旅游</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>6));?>">生活服务</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>8));?>">丽人</a></li>
		</ul>
	</div>
	<div class="clear"></div>
</div>
<!-- 菜单部分结束 -->
<div id="part-top">
	<!-- 内容当前位置开始 -->
	<div class="art-pos">
		<a href="http://localhost/Project/Meituan">北京团购</a> <?php if(is_array($positoinArr)):?><?php  foreach($positoinArr as $k=>$v){ ?> 》<a href="<?php echo U('List/index',array('cid'=>$k));?>"><?php echo $v;?></a><?php }?><?php endif;?> 》<?php echo $data['title'];?>
	</div>
	<!-- 内容当前位置结束 -->
	<div class="pro-title">
		<h3> <?php echo $data['title'];?></h3>
		<p><?php echo $data['subtitle'];?></p>
	</div>
	<div class="pro-info-box">
		<div class="pic">
			<ul class="big-pic">
				<?php if(is_array($data['pic'])):?><?php  foreach($data['pic'] as $v){ ?>
				<li><img src="http://localhost/Project/Meituan/<?php echo $v;?>" alt="" width="460" height="279" /></li>
				<?php }?><?php endif;?>
			</ul>
			<ul class="sma-pic">
				<?php if(is_array($data['pic'])):?><?php  foreach($data['pic'] as $v){ ?>
				<li><img src="http://localhost/Project/Meituan/<?php echo $v;?>" alt="" width="78" height="45" /></li>
				<?php }?><?php endif;?>
			</ul>
		</div>
		<div class="info-r">
			<div class="price">
				<span class="money">¥<em><?php echo $data['group_price'];?></em></span>
				<span class="zhe"><?php echo $data['zhe'];?>折<br/><del>￥<?php echo $data['price'];?></del></span>
			</div>
			<div class="yeji">
				<ul>
					<li class="shou">已售<span class="red"><?php echo $data['sell_num'];?></span></li>
					<li class="xing"><span class="xing1"><span class="xing2"></span></span><span class="red fen">4.6</span>分</li>
					<li class="ping"><span class="red"><?php echo $data['sell_num'];?></span>评价</li>
				</ul>
			</div>
			<div class="info3">
				<p><span class="info-l-name">商家</span><a href="#seller"><?php echo $data['sname'];?></a><i>|</i><a href="#seller">查看地址/电话 </a></p>
				<p><span class="info-l-name">有效期</span>截止到 <?php echo $data['close_date'];?> <span class="red">不可用日期</span> <?php echo $data['not_use_date'];?></p>
			</div>
			<form action="<?php echo U('Buy/buy1');?>" method="post">
				<div class="num-box">
					<span class="info-l-name">数量</span>
					<p class="num"><a class="subtract" href="javascript:">－</a><input class="num-text" type="text" name="num" value="1" /><a class="plus" href="javascript:">＋</a></p>
					<p class="tishi">最少 1 件起售</p>
				</div>
				<div class="btn">
					<input type="submit" class="buy" value="" />
					<a href="javascript:" class="to-che"></a>
					<div class="collect">
						<?php if(!$_SESSION['uid']){?>

							<a href="javascript:" class="sc"><i></i>收藏(<strong><?php echo count($collect);?></strong>)</a>

							<?php  }else{ ?>

							<?php if($collected == 1||$collected == 2){?>
								<a href="javascript:" class="sc"><i></i>收藏(<strong><?php echo count($collect);?></strong>)</a>
								<?php  }else{ ?>
								<i></i>收藏(<strong><?php echo count($collect);?></strong>)
							<?php }?>
						

							
						<?php }?>
						
					</div>
					
				</div>
				<input type="hidden" name="gid" value="<?php echo $data['gid'];?>" />
			</form>
			<div class="promise">
				<div class="promise-l-name">服务承诺</div>
				<div class="promise-list">
				<?php if(is_array($data['promise'])):?><?php  foreach($data['promise'] as $v){ ?>
					<span><a href="javascript:"><i class="promise<?php echo $v;?>"></i><?php if($v == 1){?>随时退<?php  }elseif($v == 2){ ?>过期退<?php  }elseif($v == 3){ ?>极速退<?php  }else{ ?>真实评价<?php }?></a></span>
				<?php }?><?php endif;?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 产品更多详情开始 -->
<div id="pro-more-info">
	<div class="left">
		<?php if($data['notice']){?>
		<div class="notice">
			<div class="des"><?php echo $data['notice'];?></div>
		</div>
		<?php }?>
		<div class="pro-menu">
			<ul>
				<li><a class="title_seller current" href="#seller">商家位置</a></li>
				<li><a class="title_zuzhi" href="#xuzhi">购买须知</a></li>
				<li><a class="title_content" href="#content">本单详情</a></li>
				<li><a class="title_sellerinfo" href="#sellerinfo">商家介绍</a></li>
				<li><a class="title_comments" href="#comments">消费评价</a></li>
			</ul>
		</div>
		<div class="tidai">&nbsp;</div>
		<div class="info-title" id="seller" style="">商家位置</div>
		<div class="info-content">
			<div class="seller_map">
			<div  id="smap" style="height:252px; width:315px;border:1px solid #eee;"></div>
			</div>
			<div class="seller_location">
			<?php $hd["list"]["n"]["total"]=0;if(isset($location) && !empty($location)):$_id_n=0;$_index_n=0;$lastn=min(1000,count($location));
$hd["list"]["n"]["first"]=true;
$hd["list"]["n"]["last"]=false;
$_total_n=ceil($lastn/1);$hd["list"]["n"]["total"]=$_total_n;
$_data_n = array_slice($location,0,$lastn);
if(count($_data_n)==0):echo "";
else:
foreach($_data_n as $key=>$n):
if(($_id_n)%1==0):$_id_n++;else:$_id_n++;continue;endif;
$hd["list"]["n"]["index"]=++$_index_n;
if($_index_n>=$_total_n):$hd["list"]["n"]["last"]=true;endif;?>

				<div class="list">
					<h3><?php echo $n['shopname'];?></h3>
					<?php if($hd['list']['n']['first']){?>
						<div class="info" style="display:block">
							<p>地址：<?php echo $n['address'];?></p>
							<?php if($n['subway']){?><p>地铁：<?php echo $n['subway'];?></p><?php }?>
							<p>电话：<?php echo $n['tel'];?></p>
							<p style="display:none" class="p_coord"><?php echo $n['coord'];?></p>
						</div>
						<?php  }else{ ?>
						<div class="info">
							<p>地址：<?php echo $n['address'];?></p>
							<?php if($n['subway']){?><p>地铁：<?php echo $n['subway'];?></p><?php }?>
							<p>电话：<?php echo $n['tel'];?></p>
							<p style="display:none" class="p_coord"><?php echo $n['coord'];?></p>
						</div>
					<?php }?>
					
				</div>
			<?php $hd["list"]["n"]["first"]=false;
endforeach;
endif;
else:
echo "";
endif;?>
			</div>
			<div class="clear"></div>
		</div>

		<div class="info-title" id="xuzhi" style="position:relative">购买须知</div>
		<div class="info-content">
			<ul class="xuzhi">
				<li>
					<div class="zhi-name">有效期</div>
					<div class="zhi-info">至 <?php echo $data['close_date'];?></div>
				</li>
				<li>
					<div class="zhi-name">不可用日期</div>
					<div class="zhi-info"><?php echo $data['not_use_date'];?></div>
				</li>
				<li>
					<div class="zhi-name">预约提醒</div>
					<div class="zhi-info"><?php echo $data['appointment'];?></div>
				</li>
				<li>
					<div class="zhi-name">温馨提示</div>
					<div class="zhi-info"><pre><?php echo $data['reminder'];?></pre></div>
				</li>
				<?php if($data['service']){?>
				<li>
					<div class="zhi-name">商家服务</div>
					<div class="zhi-info"><?php echo $data['service'];?></div>
				</li>
				<?php }?>
			</ul>
		</div>
		<div class="info-title" id="content">本单详情</div>
		<div class="info-content">
			<table width="100%" class="taocan" border="0" cellspacing="0" cellpadding="0">
				<tr height="40" bgcolor="#f0f0f0">
					<th align="left" width="400">套餐内容</th>
					<th align="center" width="70">单价</th>
					<th align="center" width="110">数量/规格</th>
					<th width="110" align="right" style="padding-right:10px;">小计</th>
				</tr>
				<tr height="40">
					<td align="left" width="400"><?php echo $data['title'];?></td>
					<td align="center" width="70"><?php echo $data['price'];?></td>
					<td align="center" width="110">1位</td>
					<td width="110" align="right" style="padding-right:10px;"><?php echo $data['price'];?></td>
				</tr>
			</table>
			<div class="jiazhi"><span>价值: ￥<?php echo $data['price'];?></span><span>美团价： ￥<em><?php echo $data['group_price'];?></em></span></div>
			<div class="clear"></div>
		</div>
		<div class="details">
			<?php echo $data['details'];?>
		</div>
		<div class="info-title" id="sellerinfo">商家介绍</div>
		<div class="introduction"><?php echo $data['introduction'];?></div>
		<div class="info-title" id="comments">消费评价</div>
		<div class="info-content">
			<div class="all_comments">全部评价</div>
			<?php if(is_null($Comments)){?>
			<p style="padding:30px 0 0 20px;">该商品还没有被评论</p>
			<?php  }else{ ?>
			<?php if(is_array($Comments)):?><?php  foreach($Comments as $v){ ?>
			<div class="comments_list">
				<p class="name"><?php if($v['nick']){?><?php echo $v['nick'];?><?php  }else{ ?><?php echo $v['username'];?><?php }?> <span><?php echo hd_date($v['sendtime'],'Y-m-d');?></span></p>
				<div class="c_content"><?php echo $v['content'];?></div>
			</div>
			<?php }?><?php endif;?>
			<?php }?>
		</div>

	</div>
	<div class="right">
		<div class="version">
			<a href="http://i.meituan.com/mobile/down/meituan?" class="tel"></a>
			<a href="http://maoyan.com/" class="mao"></a>
		</div>
		<div id="view"></div>
		<?php if($_SESSION['viewed']){?>
		<div class="recently-viewed">
			<div class="title"><strong>最近浏览</strong></div>
			<ul style="padding:10px 0">
				<?php if(is_array($_SESSION['viewed'])):?><?php  foreach($_SESSION['viewed'] as $k=>$v){ ?>
					<li>
						<div class="tu"><a href="<?php echo U('Article/index',array('gid'=>$k));?>"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="80" height="49" /></a></div>
						<div class="info">
							<p><a href=""><?php echo $v['title'];?></a></p>
							<p class="p2"><span>￥<?php echo $v['group_price'];?></span><del><?php echo $v['price'];?></del></p>
						</div>
					</li>
				<?php }?><?php endif;?>
			</ul>
		</div>
		<?php }?>

	</div>
</div>
<!-- 产品更多详情结束 -->
<!-- 底部开始 -->
<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><div id="footer">
	<div id="footer-con">
		<div class="foot-info">
			<h3>获取更新</h3>
			<ul>
				<li><a href="">邮件订阅</a></li>
				<li><a href="">iPhone/Android</a></li>
				<li><a href="">美团QQ空间</a></li>
				<li><a href="">美团新浪微博</a></li>
				<li><a href="">sitemap</a></li>
				<li><a href="">RSS订阅</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>商务合作</h3>
			<ul>
				<li><a href="">提供团购信息</a></li>
				<li><a href="">美团商家营销平台</a></li>
				<li><a href="">市场合作</a></li>
				<li><a href="">美团联盟</a></li>
				<li><a href="">廉正邮箱</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>公司信息</h3>
			<ul>
				<li><a href="">邮件订阅</a></li>
				<li><a href="">美团承诺</a></li>
				<li><a href="">加入我们</a></li>
				<li><a href="">法律声明</a></li>
				<li><a href="">用户协议</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>用户帮助</h3>
			<ul>
				<li><a href="">申请退款</a></li>
				<li><a href="">查看美团券密码</a></li>
				<li><a href="">常见问题</a></li>
				<li><a href="">开放API</a></li>
				<li><a href="">美团云</a></li>
				<li><a href="">反诈骗公告</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>获取更新</h3>
			<ul>
				<li><a href="">邮件订阅</a></li>
				<li><a href="">iPhone/Android</a></li>
				<li><a href="">美团QQ空间</a></li>
				<li><a href="">美团新浪微博</a></li>
				<li><a href="">sitemap</a></li>
				<li><a href="">RSS订阅</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<span class="tou"></span>	
			<p class="p1"><strong>退款、退换货、查看美团券</strong><br /><a href="">参考教程，轻松搞定 »</a></p>
			<p class="p2">
客服电话：400-660-5335 <br />
服务时间：每天 9:00 - 22:00</p>
		</div>
		<div class="clear"></div>
		<p class="power">©2014美团网团购 meituan.com 京ICP证070791号 京公网安备11010502025545号 电子公告服务规则</p>
	<div class="zheng">
		<a class="a1" href=""></a>
		<a class="a2" href=""></a>
		<a class="a3" href=""></a>
		<a class="a4" href=""></a>
		<a class="a5" href=""></a>
		<a class="a6" href=""></a>

	</div>
	</div>
	
</div>
<!-- 底部结束 -->

<script type="text/javascript">
	
	// 百度地图API功能
	var map = new BMap.Map("smap");            // 创建Map实例
	
	map.enableScrollWheelZoom();                            //启用滚轮放大缩小

	map.addControl(new BMap.NavigationControl());  //添加默认缩放平移控件
	map.addControl(new BMap.NavigationControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT, type: BMAP_NAVIGATION_CONTROL_PAN}));  //左下角，仅包含平移按钮

	$(function(){
		var p_coord = $('.p_coord');

		p_coord.each(function(i){
			var coord = $.parseJSON(p_coord.eq(i).html());
		var point = new BMap.Point(coord.lng, coord.lat);    // 创建点坐标
		map.centerAndZoom(point,15);                     // 初始化地图,设置中心点坐标和地图级别。
		 	 
		 	 var marker = new BMap.Marker(point);  // 创建标注
			map.addOverlay(marker);              // 将标注添加到地图中
		})
	})
	

</script>
</body>
</html>