<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>基本信息</title>
	<link rel="stylesheet" type="text/css" href="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/css/base.css" />
	<link rel="stylesheet" type="text/css" href="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/css/m_index.css" />
	<link rel="stylesheet" type="text/css" href="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/css/m_set.css" />

	<script type="text/javascript">
		var APP = '<?php echo "http://localhost/Project/Meituan/index.php/Index" ;?>';
		
	</script>

	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/jquery-1.11.0.min.js"></script>
	<script language="javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/area.ui.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/head.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/menu.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/common.js"></script>
	<script type="text/javascript">
		$(function(){  
		    $(document).area("s_province","s_city","s_county");//调用插件  
		}); 
	</script>
	<!-- 为了保证IE载入以下文件 -->
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/es5-sham.min.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/es5-shim.min.js"></script>
	<!-- 载入less.js -->
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/less-1.5.0.min.js"></script>

</head>
<body>
<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?>	<!-- 头部开始 -->
	<div id="top">
		<div id="top-con2">
			<div class="top-left">
				<div class="top-left1">
					<?php if(isset($_SESSION['username'])){?>
						<span class="userinfo">hi,<a href="<?php echo U('Member/order');?>" class="username"><?php echo $_SESSION['username'];?></a> <a href="<?php echo U('Login/quit');?>" class="quit">退出</a></span>
						<?php  }else{ ?>
						<span>美团一次 美一次</span>
						<span><a class="red" href="<?php echo U('Login/index');?>">登陆</a></span>
						<span><a href="<?php echo U('Regist/index');?>">注册</a></span>
						<!-- <span>|</span> -->
					<?php }?>
				</div>
			</div>
			<div class="top-right" >
				<ul>
					<li class="top-right-li"><p class="top-right-p"><a href="<?php echo U('Member/order');?>">我的订单</a></p></li>
					<li class="top-right-li have-son">
						<p class="top-right-p"><a class="top-right-menu" href="<?php echo U('Member/order');?>">我的美团</a><i class="sj"></i><i class="shu"></i></p>
						
						<div class="top-right-son mymt-pos">
							<ul class="mymt">
								<li><a href="<?php echo U('Member/order');?>">我的订单</a></li>
								<li><a href="<?php echo U('Member/collect');?>">我的收藏</a></li>
								<li><a href="<?php echo U('Member/basicInfo');?>">基本信息</a></li>
								<li><a href="<?php echo U('Member/userinfo');?>">个人资料</a></li>
								<li><a href="<?php echo U('Member/address');?>">收货地址</a></li>
							</ul>
						</div>
						
					</li>
					<li class="top-right-li have-son">
						<p class="top-right-p"><a class="top-right-menu" href="">最近浏览</a><i class="sj"></i><i class="shu"></i></p>
						
						<div class="top-right-son viewed-pos">
							<?php if(is_null($_SESSION['viewed'])){?>
							<p class="wu">暂无浏览记录</p>
							<?php  }else{ ?>
							<ul class="viewed">
								<?php if(is_array($_SESSION['viewed'])):?><?php  foreach($_SESSION['viewed'] as $k=>$v){ ?>
								<li>
									<div class="tu"><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="80" height="49" /></a></div>
									<div class="info">
										<p><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><?php echo $v['title'];?></a></p>
										<p class="p2"><span>￥<?php echo $v['group_price'];?></span><del><?php echo $v['price'];?></del></p>
									</div>
								</li>
							<?php }?><?php endif;?>
							</ul>
							<p class="unset-viewed"><a  href="javascript:" class="del_viewed" style="color:#2bb8aa">清空最近浏览记录</a></p>
							<?php }?>
						</div>
						</li>
						<!-- 商品购物车开始 -->
					<li class="top-right-li have-son">
						<p class="top-right-p gwche"><i class="che"></i><a class="top-right-menu" href="">购物车<span class="gwche-num"><?php echo count($cart);?></span>件</a><i class="sj"></i><i class="shu"></i></p>
					
						<div class="top-right-son gwche-pos" id="cart">
							<?php if(is_null($cart)){?>
							<p class="wu">暂没有加入购物车的商品</p>
							<?php  }else{ ?>
							<ul class="viewed">
								<?php if(is_array($cart)):?><?php  foreach($cart as $v){ ?>
								<li class="top_cartlist">
									<div class="tu"><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="80" height="49" /></a></div>
									<div class="info">
										<p><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>"><?php echo $v['title'];?></a></p>
										<p class="p2"><span>￥<?php echo $v['group_price'];?></span><a class="del top_del_cart" href="javascript:" gid="<?php echo $v['gid'];?>">删除</a></p>
									</div>
								</li>
							<?php }?><?php endif;?>
							</ul>

							<p class="go-gwche"><a href="<?php echo U('Cart/cart1');?>">查看我的购物车</a></p>
							<?php }?>
						</div>
		
						</li>
						<!-- 商品购物车结束 -->
				<!-- 	<li class="top-right-li have-son">
				<p class="top-right-p"><a class="top-right-menu" href="">联系客服</a><i class="sj"></i><i class="shu"></i></p>
				<div class="top-right-son contact-pos">
						<ul class="mymt">
							<li><a href="">申请退款</a></li>
							<li><a href="">申请退换货</a></li>
							<li><a href="">常见问题</a></li>
						</ul>
					</div>
				</li> -->
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<!-- 头部结束 -->
<!-- logo 和 搜索开始 -->
	<div id="logo-sch-bao2">
		<div class="logo-piece logo-piece-w240">
			<a href="http://localhost/Project/Meituan" id="logo"></a>
			<span class="city">北京</span>
			<a class="choose-city" href="">切换城市</a>
		</div>
		<div class="sch-piece sch-piece-w">
			<div class="sch">
				<div class="form">
				<form name="sch_form" action="<?php echo U('Search/index');?>" method="post">
					<div class="sch-left sch-left-w">
						<span class="sj"></span>
						<ul class="tuan-shang">
							<li>团购</li>
							<!-- <li>商家</li> -->
						</ul>
						<input class="search search-w" type="text" name="search" placeholder="请输入商品名称" />
					</div>
					<input class="btn" name="sch-btn" type="submit" value="搜 索" />
					</form>
				</div>
			</div>
			<div class="clear"></div>
			<!-- <div class="hot-words">
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
			</div> -->
		</div>
		<div class="bao"></div>
		<div class="clear"></div>
	</div>
<!-- logo 和 搜素结束 -->

<!-- 菜单部分开始 -->
<div id="menu">
	<div class="menu-l">
		<p class="cate-top">全部分类<i></i></p>
		<div class="cate-son-con">
			
			<?php if(is_array($allCate)):?><?php  foreach($allCate as $k=>$v){ ?>
			<div class="cate-list">
				<div class="cate-left-con">
					<h3 class="top-cate"><a href="<?php echo U('List/index',array('cid'=>$k));?>"><?php echo $v['parent'];?></a></h3>
					<p class="son-cate">
						<?php 
							if($v['parent'] == '酒店' || $v['parent'] == '电影'){
								$son = array_slice($v['son'], 0,2,true); 
							}else{
								$son = array_slice($v['son'], 0,3,true); 
							}
						?>
						<?php if(is_array($son)):?><?php  foreach($son as $key=>$value){ ?>
						<a href="<?php echo U('List/index',array('cid'=>$key));?>"><?php echo $value;?></a>
						<?php }?><?php endif;?>
					</p>
					<i class="right-arrow"></i>
				</div>
				<div class="cate-xian"></div>
				<div class="cate-right-con">
					<h3 class="right-top-cate"><a href=""><?php echo $v['parent'];?></a></h3>
					<p class="right-cate-son">
						<?php if(is_array($v['son'])):?><?php  foreach($v['son'] as $key=>$v){ ?>
						<a href="<?php echo U('List/index',array('cid'=>$key));?>"><?php echo $v;?></a>
						<?php }?><?php endif;?>
					</p>
				</div>
			</div>
			<?php }?><?php endif;?>

		</div>
	</div>
	<div class="menu-r">
		<ul>
			<li><a href="http://localhost/Project/Meituan">首页</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>1));?>">美食</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>2));?>">酒店</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>3));?>">电影</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>4));?>">休闲娱乐</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>5));?>">旅游</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>6));?>">生活服务</a></li>
			<li><a href="<?php echo U('List/index',array('cid'=>8));?>">丽人</a></li>
		</ul>
	</div>
	<div class="clear"></div>
</div>
<!-- 菜单部分结束 -->

<!-- 主体部分 开始-->
<div id="main">
	<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><div id="left">
		<div class="m-home">
			<div class="tu">
			<?php if($userinfo['face']){?>
				<img src="http://localhost/Project/Meituan/<?php echo $userinfo['face'];?>" width="30" height="30" />
				<?php  }else{ ?>
				<img src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/images/user-default.png" />
			<?php }?>
			
			</div>
			<div class="name">
				<p class="p1"><?php echo $userinfo['username'];?></p>
				<p class="p2"><!-- <a href=""><span>v0</span>会员</a> --></p>
			</div>
			<div class="clear"></div>
			<div class="xiugai">
				<a href="" class="safe"></a>
				<a href="" class="tel"></a>
				<a href="<?php echo U('Member/userinfo');?>" class="shezhi"></a>
			</div>
		</div>
		<div class="ye-jifen">
			<ul>
				<li><a href=""><span><?php echo $userinfo['points'];?></span><br />余额</a></li>
				<li><a href=""><span>0</span><br />积分</a></li>
				<li class="last"><a href=""><span>0</span><br />代金券</a></li>
			</ul>
		</div>
		<div class="my-item">
			<ul>
				<li><a href="<?php echo U('Member/order');?>">我的订单</a></li>
				<li><a href="<?php echo U('Member/collect');?>">我的收藏</a></li>
				<li><a href="<?php echo U('Member/basicInfo');?>">基本信息</a></li>
				<li><a href="<?php echo U('Member/userinfo');?>">个人资料</a></li>
				<li><a href="<?php echo U('Member/address');?>">收货地址</a></li>
			</ul>
		</div>
	</div>
	<div id="right">
		<div class="my-set"><a href="<?php echo U('Member/basicInfo');?>">基本信息</a><a href="<?php echo U('Member/userinfo');?>">个人资料</a><a href="<?php echo U('Member/address');?>" class="current">收获地址</a></div>
		<table class="address-table" border="0" width="100%" cellspacing="0" style="border:1px solid #eee">
			<tr bgcolor="#f4f4f4" style="height:40px; font-size:14px;">
				<th align="left" style=" padding-left:20px;">添加地址</th>
			</tr>
			<tr>
				<td>
					<form class="add_form" action="<?php echo U('Member/add_address');?>" method="post">
						<div class="add_list">
							<label for=""><span>*</span>所在地区：</label>
							<select id="s_province" name="s_province"></select>    
						    <select id="s_city" name="s_city" ></select>    
						    <select id="s_county" name="s_county"></select> 
						</div>
						<div class="add_list">
							<label for=""><span>*</span>街道地址： </label>
							<input type="text" class="street" name="street" style="width:280px;" />
						</div>
						<div class="add_list">
							<label for=""><span>*</span>邮政编码：</label>
							<input type="text" class="postalcode" name="postcode" /> <a href="http://opendata.baidu.com/post/s" style="color:#2bb8aa">邮编查询</a>
						</div>
						<div class="add_list">
							<label for=""><span>*</span>收货人姓名：</label>
							<input type="text" name="name" />
						</div>
						<div class="add_list">
							<label for=""><span>*</span>电话号码：</label>
							<input type="text" name="mobilephone" />
						</div>
						<div class="btn">
							<input type="submit" value="保存" />
						</div>
					</form>
				</td>
			</tr>
		</table>

	</div>
</div>
<!-- 主体部分 结束 -->

<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><div id="footer">
	<div id="footer-con">
		<div class="foot-info">
			<h3>获取更新</h3>
			<ul>
				<li><a href="">邮件订阅</a></li>
				<li><a href="">iPhone/Android</a></li>
				<li><a href="">美团QQ空间</a></li>
				<li><a href="">美团新浪微博</a></li>
				<li><a href="">sitemap</a></li>
				<li><a href="">RSS订阅</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>商务合作</h3>
			<ul>
				<li><a href="">提供团购信息</a></li>
				<li><a href="">美团商家营销平台</a></li>
				<li><a href="">市场合作</a></li>
				<li><a href="">美团联盟</a></li>
				<li><a href="">廉正邮箱</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>公司信息</h3>
			<ul>
				<li><a href="">邮件订阅</a></li>
				<li><a href="">美团承诺</a></li>
				<li><a href="">加入我们</a></li>
				<li><a href="">法律声明</a></li>
				<li><a href="">用户协议</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>用户帮助</h3>
			<ul>
				<li><a href="">申请退款</a></li>
				<li><a href="">查看美团券密码</a></li>
				<li><a href="">常见问题</a></li>
				<li><a href="">开放API</a></li>
				<li><a href="">美团云</a></li>
				<li><a href="">反诈骗公告</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>获取更新</h3>
			<ul>
				<li><a href="">邮件订阅</a></li>
				<li><a href="">iPhone/Android</a></li>
				<li><a href="">美团QQ空间</a></li>
				<li><a href="">美团新浪微博</a></li>
				<li><a href="">sitemap</a></li>
				<li><a href="">RSS订阅</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<span class="tou"></span>	
			<p class="p1"><strong>退款、退换货、查看美团券</strong><br /><a href="">参考教程，轻松搞定 »</a></p>
			<p class="p2">
客服电话：400-660-5335 <br />
服务时间：每天 9:00 - 22:00</p>
		</div>
		<div class="clear"></div>
		<p class="power">©2014美团网团购 meituan.com 京ICP证070791号 京公网安备11010502025545号 电子公告服务规则</p>
	<div class="zheng">
		<a class="a1" href=""></a>
		<a class="a2" href=""></a>
		<a class="a3" href=""></a>
		<a class="a4" href=""></a>
		<a class="a5" href=""></a>
		<a class="a6" href=""></a>

	</div>
	</div>
	
</div>
</body>
</html>