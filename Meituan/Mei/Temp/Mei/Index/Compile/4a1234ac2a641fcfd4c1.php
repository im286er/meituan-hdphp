<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>美团网首页</title>

	<link rel="stylesheet" type="text/css" href="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/css/index.css" />
	<link rel="stylesheet" type="text/css" href="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/css/base.css" />
	<script type="text/javascript">
		var APP = '<?php echo "http://localhost/Project/Meituan/index.php/Index" ;?>';
	</script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/head.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/menu.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/index.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/common.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/js/right.js"></script>
	
	<!-- 为了保证IE载入以下文件 -->
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/es5-sham.min.js"></script>
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/es5-shim.min.js"></script>
	<!-- 载入less.js -->
	<script type="text/javascript" src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/Css/es5-shim/less-1.5.0.min.js"></script>
	<style type="text/css">
		.sch-piece .sch-left .index_sch_result{width: 444px;}
	</style>
</head>
<body>
<!-- 头部开始 -->
	<div id="top">
		<div id="top-con">
			<div class="top-left">
				<div class="top-left1">
				<?php if(isset($_SESSION['username'])){?>
					<span class="userinfo">hi,<a href="<?php echo U('Member/order');?>" class="username"><?php echo $_SESSION['username'];?></a> <a href="<?php echo U('Login/quit');?>" class="quit">退出</a></span>
					<?php  }else{ ?>
					<span>美团一次 美一次</span>
					<span><a class="red" href="<?php echo U('Login/index');?>">登陆</a></span>
					<span><a href="<?php echo U('Regist/index');?>">注册</a></span>
					<!-- <span>|</span> -->
				<?php }?>
				</div>
			</div>
			<div class="top-right" >
				<ul>
					<li class="top-right-li"><p class="top-right-p"><a href="<?php echo U('Member/order');?>">我的订单</a></p></li>
					<li class="top-right-li have-son">
						<p class="top-right-p"><a class="top-right-menu" href="<?php echo U('Member/order');?>">我的美团</a><i class="sj"></i><i class="shu"></i></p>
						
						<div class="top-right-son mymt-pos">
							<ul class="mymt">
								<li><a href="<?php echo U('Member/order');?>">我的订单</a></li>
								<li><a href="<?php echo U('Member/collect');?>">我的收藏</a></li>
								<li><a href="<?php echo U('Member/basicInfo');?>">基本信息</a></li>
								<li><a href="<?php echo U('Member/userinfo');?>">个人资料</a></li>
								<li><a href="<?php echo U('Member/address');?>">收货地址</a></li>
							</ul>
						</div>
						
					</li>
					<li class="top-right-li have-son">
						<p class="top-right-p"><a class="top-right-menu" href="">最近浏览</a><i class="sj"></i><i class="shu"></i></p>
						
						<div class="top-right-son viewed-pos">
						<?php if(is_null($_SESSION['viewed'])){?>
							<p class="wu">暂无浏览记录</p>
							<?php  }else{ ?>
							<ul class="viewed">
								<?php if(is_array($_SESSION['viewed'])):?><?php  foreach($_SESSION['viewed'] as $k=>$v){ ?>
								<li>
									<div class="tu"><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="80" height="49" /></a></div>
									<div class="info">
										<p><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><?php echo $v['title'];?></a></p>
										<p class="p2"><span>￥<?php echo $v['group_price'];?></span><del><?php echo $v['price'];?></del></p>
									</div>
								</li>
							<?php }?><?php endif;?>
							</ul>
							<p class="unset-viewed"><a  href="javascript:" class="del_viewed" style="color:#2bb8aa">清空最近浏览记录</a></p>
							<?php }?>
						</div>
					</li>
				<!-- 商品购物车开始 -->
					<li class="top-right-li have-son">
						<p class="top-right-p gwche"><i class="che"></i><a class="top-right-menu" href="">购物车<span class="gwche-num"><?php echo count($cart);?></span>件</a><i class="sj"></i><i class="shu"></i></p>
						
						<div class="top-right-son gwche-pos" id="cart">
							<?php if(is_null($cart)){?>
							<p class="wu">暂没有加入购物车的商品</p>
							<?php  }else{ ?>
							<ul class="viewed">
								<?php if(is_array($cart)):?><?php  foreach($cart as $v){ ?>
								<li class="top_cartlist">
									<div class="tu"><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="80" height="49" /></a></div>
									<div class="info">
										<p><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>"><?php echo $v['title'];?></a></p>
										<p class="p2"><span>￥<?php echo $v['group_price'];?></span><a class="del top_del_cart" href="javascript:" gid="<?php echo $v['gid'];?>">删除</a></p>
									</div>
								</li>
							<?php }?><?php endif;?>
							</ul>

							<p class="go-gwche"><a href="<?php echo U('Cart/cart1');?>">查看我的购物车</a></p>
							<?php }?>
						</div>
		
						</li>
						<!-- 商品购物车结束 -->
					<!-- <li class="top-right-li have-son">
					<p class="top-right-p"><a class="top-right-menu" href="">联系客服</a><i class="sj"></i><i class="shu"></i></p>
					<div class="top-right-son contact-pos">
							<ul class="mymt">
								<li><a href="">申请退款</a></li>
								<li><a href="">申请退换货</a></li>
								<li><a href="">常见问题</a></li>
							</ul>
						</div>
					</li> -->
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<!-- 头部结束 -->
<!-- 头部广告图片 -->
	<div class="adv1"><img src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/images/adv1.jpg" width="1200" height="60" alt="" /><a class="close" href="javascript:"></a></div>
<!-- logo 和 搜索开始 -->
	<div id="logo-sch-bao">
		<div class="logo-piece">
			<a href="" id="logo"></a>
			<span class="city">北京</span>
			<a class="choose-city" href="">切换城市</a>
		</div>
		<div class="sch-piece">
			<div class="sch index_">
				<div class="form">
				<form name="sch_form" action="<?php echo U('Search/index');?>" method="post">
					<div class="sch-left">
						<span class="sj"></span>
						<ul class="tuan-shang">
							<li>团购</li>
							<li>商家</li>
						</ul>
						<input class="search" type="text" name="search" placeholder="请输入商品名称" />
					</div>
					<input class="btn" type="submit" value="搜 索" />
					</form>
				</div>
			</div>
			<div class="clear"></div>
			<!-- <div class="hot-words">
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
				<a href="">自助餐</a>
			</div> -->
		</div>
		<div class="bao"></div>
	</div>
<!-- logo 和 搜素结束 -->
<!-- 第一部分开始 -->
<div id="part1">
	<!-- 全部分类开始 -->

	<div id="category">
		<p class="cate-top">全部分类</p>
		<?php if(is_array($allCate)):?><?php  foreach($allCate as $k=>$v){ ?>
		<div class="cate-list">
			<div class="cate-left-con">
				<h3 class="top-cate"><a href="<?php echo U('List/index',array('cid'=>$k));?>" target="_blank"><?php echo $v['parent'];?></a></h3>
				<p class="son-cate">
					<?php 
						if($v['parent'] == '酒店' || $v['parent'] == '电影'){
							$son = array_slice($v['son'], 0,2,true); 
						}else{
							$son = array_slice($v['son'], 0,3,true); 
						}
					?>
					<?php if(is_array($son)):?><?php  foreach($son as $key=>$value){ ?>
					<a href="<?php echo U('List/index',array('cid'=>$key));?>" target="_blank"><?php echo $value;?></a>
					<?php }?><?php endif;?>
					
				</p>
				<i class="right-arrow"></i>
			</div>
			<div class="cate-xian"></div>
			<div class="cate-right-con">
				<h3 class="right-top-cate"><a href=""><?php echo $v['parent'];?></a></h3>
				<p class="right-cate-son">
					<?php if(is_array($v['son'])):?><?php  foreach($v['son'] as $key=>$v){ ?>
					<a href="<?php echo U('List/index',array('cid'=>$key));?>"><?php echo $v;?></a>
					<?php }?><?php endif;?>
				</p>
			</div>
		</div>
		<?php }?><?php endif;?>
		
	</div>
	<!-- 全部分类结束 -->
	<!-- 第一部分 中间部分开始 -->
	<div class="part1-mid">
		<div id="mid-menu">
			<ul>
				<li><a href="http://localhost/Project/Meituan">首页</a></li>
				<li><a href="<?php echo U('List/index',array('cid'=>1));?>">美食</a></li>
				<li><a href="<?php echo U('List/index',array('cid'=>2));?>">酒店</a></li>
				<li><a href="<?php echo U('List/index',array('cid'=>3));?>">电影</a></li>
				<li><a href="<?php echo U('List/index',array('cid'=>4));?>">休闲娱乐</a></li>
				<li><a href="<?php echo U('List/index',array('cid'=>5));?>">旅游</a></li>
				<li><a href="<?php echo U('List/index',array('cid'=>6));?>">生活服务</a></li>
				<li><a href="<?php echo U('List/index',array('cid'=>8));?>">丽人</a></li>
			</ul>
		</div>
		<div class="border-right">
			<div class="index-filter">
				<div class="filter-list">
					<h3 class="big-choose"><i class="h"></i>热门团购</h3>
					<div class="s-choose">
						<ul>
							<li><a href="<?php echo U('List/index',array('cid'=>9));?>" target="_blank">自助餐</a></li>
							<li><a href="<?php echo U('List/index',array('cid'=>10));?>" target="_blank">火锅</a></li>
							<li><a href="<?php echo U('List/index',array('cid'=>11));?>" target="_blank">烤肉</a></li>
							<li><a href="<?php echo U('List/index',array('cid'=>13));?>" target="_blank">麻辣烫</a></li>
							<li><a href="<?php echo U('List/index',array('cid'=>40));?>" target="_blank">经济型酒店</a></li>
							<li><a href="<?php echo U('List/index',array('cid'=>41));?>" target="_blank">豪华酒店</a></li>
							<li><a href="<?php echo U('List/index',array('cid'=>85));?>" target="_blank">KTV</a></li>
							<li><a href="<?php echo U('List/index',array('cid'=>86));?>" target="_blank">足疗按摩</a></li>
						</ul>
					</div>
				</div>
				<div class="filter-list qu">
					<h3 class="big-choose"><i class="quyu"></i>全部区域</h3>
					<div class="s-choose qu_value">
						<ul>
							<?php if(is_array($quyuArr)):?><?php  foreach($quyuArr as $k=>$v){ ?>
							<li><a href="<?php echo U('Index/area',array('atid'=>$v['atid']));?>"><?php echo $v['attrvalue'];?></a></li>
							<?php }?><?php endif;?>
						</ul>
					</div>
					<div class="qu_hidden">
						<h3 class="big-choose"><i class="quyu"></i>全部区域</h3>
						<div class="s-choose">
							<ul>
								<?php if(is_array($quyuArr)):?><?php  foreach($quyuArr as $k=>$v){ ?>
								<li><a href="<?php echo U('Index/area',array('atid'=>$v['atid']));?>"><?php echo $v['attrvalue'];?></a></li>
								<?php }?><?php endif;?>
							</ul>
						</div>
					</div>
					<script type="text/javascript">
							$('.qu').hover(function(){
								$(this).find('.qu_hidden').show();
							},function(){
								$(this).find('.qu_hidden').hide();
							});
					</script>
				</div>
				<div class="filter-list">
					<h3 class="big-choose"><i class="shangq"></i>热门区域</h3>
					<div class="s-choose">
						<ul>
							<?php if(is_array($hotqu)):?><?php  foreach($hotqu as $k=>$v){ ?>
							<li><a href="<?php echo U('Index/area',array('atid'=>$v['atid']));?>"><?php echo $v['attrvalue'];?></a></li>
							<?php }?><?php endif;?>
						</ul>
					</div>
				</div>
			</div>
			<div class="this-weed-choose">
				<div class="title">
					<h3><i></i>本周精选</h3>
					<ul class="btn-list">
						<li class="hover"></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
					</ul>
				</div>
				<div class="all-box">
					<div class="btn">
						<a href="javascript:" class="l-btn"></a>
						<a href="javascript:" class="r-btn"></a>
					</div>
					<ul class="list">
					<?php if(is_array($weekgoods)):?><?php  foreach($weekgoods as $k=>$v){ ?>
						<li <?php if($k == 0){?>style="opacity:1;filter(opacity=100)"<?php }?>>
							<div class="left">
								<a href="<?php echo U('Article/index',array('gid'=>$v[0]['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v[0]['pic'];?>" width="366" height="222" alt="" /></a>
								<span class="zhe"><?php echo $v[0]['zhe'];?></span>
								<span class="bot-ll"></span>
								<div class="bot">
									<div class="bot-l">
										<div class="p1"><?php echo $v[0]['title'];?></div>
									</div>
									<div class="bot-r">¥<span><?php echo $v[0]['group_price'];?></span></div>
								</div>
							</div>
							<div class="left right">
								<a href="<?php echo U('Article/index',array('gid'=>$v[0]['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v[1]['pic'];?>" width="366" height="222" alt="" /></a>
								<span class="zhe"><?php echo $v[1]['zhe'];?></span>
								<span class="bot-rr"></span>
								<div class="bot">
									<div class="bot-l">
										<div class="p1"><?php echo $v[1]['title'];?></div>
									</div>
									<div class="bot-r">¥<span><?php echo $v[1]['group_price'];?></span></div>
								</div>
							</div>
						</li>
					<?php }?><?php endif;?>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<!-- 第一部分 中间部分结束 -->
	<!-- 第一部分右侧开始 -->
	<div class="part1-right">
		<img src="http://localhost/Project/Meituan/./Mei/Mei/Index/Tpl/Public/images/right1.jpg" alt="" />
	</div>
	<!-- 第一部分右侧结束 -->
	<div class="clear"></div>
	</div>

<!-- 第一部分结束 -->
<!-- 主要内容部分开始 -->
<div id="index-main">
	<!-- 左侧部分开始 -->
	<div id="left">
	<!-- 美食开始 -->
		<div class="title">
			<div class="t-left food">美食</div>
			<div class="t-right">
				<?php $sonCate = array_slice($allCate['1']['son'], 0,6,true); ?>
				<?php if(is_array($sonCate)):?><?php  foreach($sonCate as $k=>$v){ ?>
				<a href="<?php echo U('List/index',array('cid'=>$k));?>" target="_blank"><?php echo $v;?></a><i></i>
				<?php }?><?php endif;?>
				<a href="<?php echo U('List/index',array('cid'=>1));?>" target="_blank" class="all">全部 &gt;</a>
			</div>
		</div>
		<ul class="floor-box">
		<?php if(is_array($meishi)):?><?php  foreach($meishi as $k=>$v){ ?>
			<li class="list-box">
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="314" height="192" alt="<?php echo $v['title'];?>" />
				<div class="sq"><span></span>北下关、对外经贸、人民大学、牡丹园/北太平庄、海淀区、五道口、小营、魏公村、崇文门</div>
				<!-- <div class="mark">
					<span class="m-yuyue"></span>
				</div> --></a>
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank">
					<p class="title2"><?php echo $v['title'];?></p>
					<p class="des"><?php echo hd_substr($v['subtitle'],40,'...');?></p>
				</a>
				<div class="price">
					<span class="cur-p">¥<strong><?php echo $v['group_price'];?></strong></span>
					<span class="stor-p">门店价<del>¥<?php echo $v['price'];?></del></span>
				</div>
				<div class="bot">
					<div class="p1">已售<span><?php echo $v['sell_num'];?></span></div>
					<div class="p2"><a href=""><span class="xing1"><span class="xing2"></span></span><?php echo $v['comment_num'];?>人评价</a></div>
				</div>
			</li>
			<?php }?><?php endif;?>
		</ul>
		<div class="more"><a href="<?php echo U('List/index',array('cid'=>1));?>" target="_blank">更多美食团购，请点击查看<i></i></a></div>
	<!-- 美食结束 -->
	<!-- 休闲娱乐开始 -->
		<div class="title">
			<div class="t-left yule">休闲娱乐</div>
			<div class="t-right">
				<?php $sonCate = array_slice($allCate['4']['son'], 0,4,true); ?>
				<?php if(is_array($sonCate)):?><?php  foreach($sonCate as $k=>$v){ ?>
				<a href="<?php echo U('List/index',array('cid'=>$k));?>" target="_blank"><?php echo $v;?></a><i></i>
				<?php }?><?php endif;?>
				<a href="<?php echo U('List/index',array('cid'=>4));?>" target="_blank" class="all">全部 &gt;</a>
			</div>
		</div>
		<ul class="floor-box">
		<?php if(is_array($recreation)):?><?php  foreach($recreation as $k=>$v){ ?>
			<li class="list-box">
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="314" height="192" alt="<?php echo $v['title'];?>" />
				<div class="sq"><span></span>北下关、对外经贸、人民大学、牡丹园/北太平庄、海淀区、五道口、小营、魏公村、崇文门</div>
				<!-- <div class="mark">
					<span class="m-yuyue"></span>
				</div> --></a>
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank">
					<p class="title2"><?php echo $v['title'];?></p>
					<p class="des"><?php echo hd_substr($v['subtitle'],40,'...');?></p>
				</a>
				<div class="price">
					<span class="cur-p">¥<strong><?php echo $v['group_price'];?></strong></span>
					<span class="stor-p">门店价<del>¥<?php echo $v['price'];?></del></span>
				</div>
				<div class="bot">
					<div class="p1">已售<span><?php echo $v['sell_num'];?></span></div>
					<div class="p2"><a href=""><span class="xing1"><span class="xing2"></span></span><?php echo $v['comment_num'];?>人评价</a></div>
				</div>
			</li>
			<?php }?><?php endif;?>
		</ul>		
		<div class="more"><a href="<?php echo U('List/index',array('cid'=>4));?>" target="_blank">更多休闲娱乐团购，请点击查看<i></i></a></div>
	<!-- 休闲娱乐结束 -->
	<!-- 电影开始 -->
		<div class="title">
			<div class="t-left movie">电影</div>
			<div class="t-right">
				<?php $sonCate = array_slice($allCate['3']['son'], 0,3,true); ?>
				<?php if(is_array($sonCate)):?><?php  foreach($sonCate as $k=>$v){ ?>
				<a href="<?php echo U('List/index',array('cid'=>$k));?>" target="_blank"><?php echo $v;?></a><i></i>
				<?php }?><?php endif;?>
				<a href="<?php echo U('List/index',array('cid'=>3));?>" target="_blank" class="all">全部 &gt;</a>
			</div>
		</div>
		<ul class="floor-box">
		<?php if(is_array($movie)):?><?php  foreach($movie as $k=>$v){ ?>
			<li class="list-box">
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="314" height="192" alt="<?php echo $v['title'];?>" />
				<div class="sq"><span></span>北下关、对外经贸、人民大学、牡丹园/北太平庄、海淀区、五道口、小营、魏公村、崇文门</div>
				<!-- <div class="mark">
					<span class="m-yuyue"></span>
				</div> --></a>
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank">
					<p class="title2"><?php echo $v['title'];?></p>
					<p class="des"><?php echo hd_substr($v['subtitle'],40,'...');?></p>
				</a>
				<div class="price">
					<span class="cur-p">¥<strong><?php echo $v['group_price'];?></strong></span>
					<span class="stor-p">门店价<del>¥<?php echo $v['price'];?></del></span>
				</div>
				<div class="bot">
					<div class="p1">已售<span><?php echo $v['sell_num'];?></span></div>
					<div class="p2"><a href=""><span class="xing1"><span class="xing2"></span></span><?php echo $v['comment_num'];?>人评价</a></div>
				</div>
			</li>
			<?php }?><?php endif;?>
		</ul>	
		<div class="more"><a href="<?php echo U('List/index',array('cid'=>3));?>" target="_blank">更多电影团购，请点击查看<i></i></a></div>
	<!-- 电影结束 -->
	<!-- 酒店开始 -->
		<div class="title">
			<div class="t-left hotel">酒店</div>
			<div class="t-right">
				<?php $sonCate = array_slice($allCate['2']['son'], 0,4,true); ?>
				<?php if(is_array($sonCate)):?><?php  foreach($sonCate as $k=>$v){ ?>
				<a href="<?php echo U('List/index',array('cid'=>$k));?>" target="_blank"><?php echo $v;?></a><i></i>
				<?php }?><?php endif;?>
				<a href="<?php echo U('List/index',array('cid'=>2));?>" target="_blank" class="all">全部 &gt;</a>
			</div>
		</div>
		<ul class="floor-box">
		<?php if(is_array($hotel)):?><?php  foreach($hotel as $k=>$v){ ?>
			<li class="list-box">
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="314" height="192" alt="<?php echo $v['title'];?>" />
				<div class="sq"><span></span>北下关、对外经贸、人民大学、牡丹园/北太平庄、海淀区、五道口、小营、魏公村、崇文门</div>
				<!-- <div class="mark">
					<span class="m-yuyue"></span>
				</div> --></a>
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank">
					<p class="title2"><?php echo $v['title'];?></p>
					<p class="des"><?php echo hd_substr($v['subtitle'],40,'...');?></p>
				</a>
				<div class="price">
					<span class="cur-p">¥<strong><?php echo $v['group_price'];?></strong></span>
					<span class="stor-p">门店价<del>¥<?php echo $v['price'];?></del></span>
				</div>
				<div class="bot">
					<div class="p1">已售<span><?php echo $v['sell_num'];?></span></div>
					<div class="p2"><a href=""><span class="xing1"><span class="xing2"></span></span><?php echo $v['comment_num'];?>人评价</a></div>
				</div>
			</li>
			<?php }?><?php endif;?>
		</ul>	
		<div class="more"><a href="<?php echo U('List/index',array('cid'=>2));?>" target="_blank">更多酒店团购，请点击查看<i></i></a></div>
	<!-- 酒店结束 -->
	<!-- 生活服务开始 -->
		<div class="title">
			<div class="t-left life">生活服务</div>
			<div class="t-right">
				<?php $sonCate = array_slice($allCate['6']['son'], 0,4,true); ?>
				<?php if(is_array($sonCate)):?><?php  foreach($sonCate as $k=>$v){ ?>
				<a href="<?php echo U('List/index',array('cid'=>$k));?>" target="_blank"><?php echo $v;?></a><i></i>
				<?php }?><?php endif;?>
				<a href="<?php echo U('List/index',array('cid'=>6));?>" target="_blank" class="all">全部 &gt;</a>
			</div>
		</div>
		<ul class="floor-box">
		<?php if(is_array($lifeServer)):?><?php  foreach($lifeServer as $k=>$v){ ?>
			<li class="list-box">
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="314" height="192" alt="<?php echo $v['title'];?>" />
				<div class="sq"><span></span>北下关、对外经贸、人民大学、牡丹园/北太平庄、海淀区、五道口、小营、魏公村、崇文门</div>
				<!-- <div class="mark">
					<span class="m-yuyue"></span>
				</div> --></a>
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank">
					<p class="title2"><?php echo $v['title'];?></p>
					<p class="des"><?php echo hd_substr($v['subtitle'],40,'...');?></p>
				</a>
				<div class="price">
					<span class="cur-p">¥<strong><?php echo $v['group_price'];?></strong></span>
					<span class="stor-p">门店价<del>¥<?php echo $v['price'];?></del></span>
				</div>
				<div class="bot">
					<div class="p1">已售<span><?php echo $v['sell_num'];?></span></div>
					<div class="p2"><a href=""><span class="xing1"><span class="xing2"></span></span><?php echo $v['comment_num'];?>人评价</a></div>
				</div>
			</li>
			<?php }?><?php endif;?>
		</ul>	
		<div class="more"><a href="<?php echo U('List/index',array('cid'=>6));?>" target="_blank">更多生活服务团购，请点击查看<i></i></a></div>
	<!-- 生活服务结束 -->
	<!-- 购物开始 -->
		<div class="title">
			<div class="t-left shop">购物</div>
			<div class="t-right">
				<?php $sonCate = array_slice($allCate['7']['son'], 0,4,true); ?>
				<?php if(is_array($sonCate)):?><?php  foreach($sonCate as $k=>$v){ ?>
				<a href="<?php echo U('List/index',array('cid'=>$k));?>" target="_blank"><?php echo $v;?></a><i></i>
				<?php }?><?php endif;?>
				<a href="<?php echo U('List/index',array('cid'=>7));?>" target="_blank" class="all">全部 &gt;</a>
			</div>
		</div>
		<ul class="floor-box">
		<?php if(is_array($shopping)):?><?php  foreach($shopping as $k=>$v){ ?>
			<li class="list-box">
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="314" height="192" alt="<?php echo $v['title'];?>" />
				<div class="sq"><span></span>北下关、对外经贸、人民大学、牡丹园/北太平庄、海淀区、五道口、小营、魏公村、崇文门</div>
				<!-- <div class="mark">
					<span class="m-yuyue"></span>
				</div> --></a>
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank">
					<p class="title2"><?php echo $v['title'];?></p>
					<p class="des"><?php echo hd_substr($v['subtitle'],40,'...');?></p>
				</a>
				<div class="price">
					<span class="cur-p">¥<strong><?php echo $v['group_price'];?></strong></span>
					<span class="stor-p">门店价<del>¥<?php echo $v['price'];?></del></span>
				</div>
				<div class="bot">
					<div class="p1">已售<span><?php echo $v['sell_num'];?></span></div>
					<div class="p2"><a href=""><span class="xing1"><span class="xing2"></span></span><?php echo $v['comment_num'];?>人评价</a></div>
				</div>
			</li>
			<?php }?><?php endif;?>
		</ul>
		<div class="more"><a href="<?php echo U('List/index',array('cid'=>7));?>" target="_blank">更多购物团购，请点击查看<i></i></a></div>
	<!-- 购物结束 -->
	<!-- 丽人开始 -->
		<div class="title">
			<div class="t-left beauty">丽人</div>
			<div class="t-right">
				<?php $sonCate = array_slice($allCate['8']['son'], 0,4,true); ?>
				<?php if(is_array($sonCate)):?><?php  foreach($sonCate as $k=>$v){ ?>
				<a href="<?php echo U('List/index',array('cid'=>$k));?>" target="_blank"><?php echo $v;?></a><i></i>
				<?php }?><?php endif;?>
				<a href="<?php echo U('List/index',array('cid'=>8));?>" target="_blank" class="all">全部 &gt;</a>
			</div>
		</div>
		<ul class="floor-box">
		<?php if(is_array($beauty)):?><?php  foreach($beauty as $k=>$v){ ?>
			<li class="list-box">
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="314" height="192" alt="<?php echo $v['title'];?>" />
				<div class="sq"><span></span>北下关、对外经贸、人民大学、牡丹园/北太平庄、海淀区、五道口、小营、魏公村、崇文门</div>
				<!-- <div class="mark">
					<span class="m-yuyue"></span>
				</div> --></a>
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank">
					<p class="title2"><?php echo $v['title'];?></p>
					<p class="des"><?php echo hd_substr($v['subtitle'],40,'...');?></p>
				</a>
				<div class="price">
					<span class="cur-p">¥<strong><?php echo $v['group_price'];?></strong></span>
					<span class="stor-p">门店价<del>¥<?php echo $v['price'];?></del></span>
				</div>
				<div class="bot">
					<div class="p1">已售<span><?php echo $v['sell_num'];?></span></div>
					<div class="p2"><a href=""><span class="xing1"><span class="xing2"></span></span><?php echo $v['comment_num'];?>人评价</a></div>
				</div>
			</li>
			<?php }?><?php endif;?>
		</ul>
		<div class="more"><a href="<?php echo U('List/index',array('cid'=>8));?>" target="_blank">更多丽人团购，请点击查看<i></i></a></div>
	<!-- 丽人结束 -->
	<!-- 旅游开始 -->
		<div class="title">
			<div class="t-left travel">旅游</div>
			<div class="t-right">
				<?php $sonCate = array_slice($allCate['5']['son'], 0,4,true); ?>
				<?php if(is_array($sonCate)):?><?php  foreach($sonCate as $k=>$v){ ?>
				<a href="<?php echo U('List/index',array('cid'=>$k));?>" target="_blank"><?php echo $v;?></a><i></i>
				<?php }?><?php endif;?>
				<a href="<?php echo U('List/index',array('cid'=>5));?>" target="_blank" class="all">全部 &gt;</a>
			</div>
		</div>
		<ul class="floor-box">
		<?php if(is_array($travel)):?><?php  foreach($travel as $k=>$v){ ?>
			<li class="list-box">
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="314" height="192" alt="<?php echo $v['title'];?>" />
				<div class="sq"><span></span>北下关、对外经贸、人民大学、牡丹园/北太平庄、海淀区、五道口、小营、魏公村、崇文门</div>
				<!-- <div class="mark">
					<span class="m-yuyue"></span>
				</div> --></a>
				<a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank">
					<p class="title2"><?php echo $v['title'];?></p>
					<p class="des"><?php echo hd_substr($v['subtitle'],40,'...');?></p>
				</a>
				<div class="price">
					<span class="cur-p">¥<strong><?php echo $v['group_price'];?></strong></span>
					<span class="stor-p">门店价<del>¥<?php echo $v['price'];?></del></span>
				</div>
				<div class="bot">
					<div class="p1">已售<span><?php echo $v['sell_num'];?></span></div>
					<div class="p2"><a href=""><span class="xing1"><span class="xing2"></span></span><?php echo $v['comment_num'];?>人评价</a></div>
				</div>
			</li>
			<?php }?><?php endif;?>
		</ul>
		<div class="more"><a href="<?php echo U('List/index',array('cid'=>5));?>" target="_blank">更多旅游团购，请点击查看<i></i></a></div>
	<!-- 旅游结束 -->
	</div>
	
	<!-- 左侧部分结束-->
	<!-- 右侧部分开始-->
	<div id="right">
		<div class="version">
			<a href="http://i.meituan.com/mobile/down/meituan?" class="tel"></a>
			<a href="http://maoyan.com/" class="mao"></a>
		</div>
		<div id="view"></div>
		<?php if($_SESSION['viewed']){?>
		<div class="recently-viewed">
			<div class="title"><strong>最近浏览</strong></div>
			<ul style="padding:10px 0">
				<?php if(is_array($_SESSION['viewed'])):?><?php  foreach($_SESSION['viewed'] as $k=>$v){ ?>
					<li>
						<div class="tu"><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><img src="http://localhost/Project/Meituan/<?php echo $v['pic'];?>" width="80" height="49" /></a></div>
						<div class="info">
							<p><a href="<?php echo U('Article/index',array('gid'=>$v['gid']));?>" target="_blank"><?php echo $v['title'];?></a></p>
							<p class="p2"><span>￥<?php echo $v['group_price'];?></span><del><?php echo $v['price'];?></del></p>
						</div>
					</li>
				<?php }?><?php endif;?>
			</ul>
		</div>
		<?php }?>
	</div>
	<!-- 右侧部分结束-->
</div>
<!-- 主要内容部分结束 -->
<!-- 底部开始 -->
<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><div id="footer">
	<div id="footer-con">
		<div class="foot-info">
			<h3>获取更新</h3>
			<ul>
				<li><a href="">邮件订阅</a></li>
				<li><a href="">iPhone/Android</a></li>
				<li><a href="">美团QQ空间</a></li>
				<li><a href="">美团新浪微博</a></li>
				<li><a href="">sitemap</a></li>
				<li><a href="">RSS订阅</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>商务合作</h3>
			<ul>
				<li><a href="">提供团购信息</a></li>
				<li><a href="">美团商家营销平台</a></li>
				<li><a href="">市场合作</a></li>
				<li><a href="">美团联盟</a></li>
				<li><a href="">廉正邮箱</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>公司信息</h3>
			<ul>
				<li><a href="">邮件订阅</a></li>
				<li><a href="">美团承诺</a></li>
				<li><a href="">加入我们</a></li>
				<li><a href="">法律声明</a></li>
				<li><a href="">用户协议</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>用户帮助</h3>
			<ul>
				<li><a href="">申请退款</a></li>
				<li><a href="">查看美团券密码</a></li>
				<li><a href="">常见问题</a></li>
				<li><a href="">开放API</a></li>
				<li><a href="">美团云</a></li>
				<li><a href="">反诈骗公告</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<h3>获取更新</h3>
			<ul>
				<li><a href="">邮件订阅</a></li>
				<li><a href="">iPhone/Android</a></li>
				<li><a href="">美团QQ空间</a></li>
				<li><a href="">美团新浪微博</a></li>
				<li><a href="">sitemap</a></li>
				<li><a href="">RSS订阅</a></li>
			</ul>	
		</div>
		<div class="foot-info">
			<span class="tou"></span>	
			<p class="p1"><strong>退款、退换货、查看美团券</strong><br /><a href="">参考教程，轻松搞定 »</a></p>
			<p class="p2">
客服电话：400-660-5335 <br />
服务时间：每天 9:00 - 22:00</p>
		</div>
		<div class="clear"></div>
		<p class="power">©2014美团网团购 meituan.com 京ICP证070791号 京公网安备11010502025545号 电子公告服务规则</p>
	<div class="zheng">
		<a class="a1" href=""></a>
		<a class="a2" href=""></a>
		<a class="a3" href=""></a>
		<a class="a4" href=""></a>
		<a class="a5" href=""></a>
		<a class="a6" href=""></a>

	</div>
	</div>
	
</div>
<!-- 底部结束 -->
<!-- 左侧悬浮 -->
<div class="left-side">
	<ul>
		<li class="l-meishi current0" mark="food"><i></i><span>美食</span></li>
		<li class="l-yule" mark="yule"><i></i><span>休闲娱乐</span></li>
		<li class="l-movie" mark="movie"><i></i><span>电影</span></li>
		<li class="l-hotel" mark="hotel"><i></i><span>酒店</span></li>
		<li class="l-fuwu" mark="life"><i></i><span>生活服务</span></li>
		<li class="l-shop" mark="shop"><i></i><span>购物</span></li>
		<li class="l-liren" mark="beauty"><i></i><span>丽人</span></li>
		<li class="l-trave" mark="travel"><i></i><span>旅游</span></li>
		
	</ul>
	<div class="gotop"><span>返回顶部</span></div>
</div>
</body>
</html>