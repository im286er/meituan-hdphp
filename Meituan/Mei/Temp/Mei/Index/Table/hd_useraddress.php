<?php if(!defined('HDPHP_PATH'))exit;
return array (
  'address_id' => 
  array (
    'field' => 'address_id',
    'type' => 'int(10) unsigned',
    'null' => 'NO',
    'key' => true,
    'default' => NULL,
    'extra' => 'auto_increment',
  ),
  'name' => 
  array (
    'field' => 'name',
    'type' => 'char(10)',
    'null' => 'NO',
    'key' => false,
    'default' => '',
    'extra' => '',
  ),
  'address' => 
  array (
    'field' => 'address',
    'type' => 'varchar(100)',
    'null' => 'NO',
    'key' => false,
    'default' => '',
    'extra' => '',
  ),
  'mobilephone' => 
  array (
    'field' => 'mobilephone',
    'type' => 'char(11)',
    'null' => 'NO',
    'key' => false,
    'default' => '',
    'extra' => '',
  ),
  'postcode' => 
  array (
    'field' => 'postcode',
    'type' => 'char(6)',
    'null' => 'NO',
    'key' => false,
    'default' => '',
    'extra' => '',
  ),
  'is_default' => 
  array (
    'field' => 'is_default',
    'type' => 'tinyint(1)',
    'null' => 'NO',
    'key' => false,
    'default' => '0',
    'extra' => '',
  ),
  'user_uid' => 
  array (
    'field' => 'user_uid',
    'type' => 'int(11)',
    'null' => 'NO',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'street' => 
  array (
    'field' => 'street',
    'type' => 'varchar(100)',
    'null' => 'NO',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
);
?>