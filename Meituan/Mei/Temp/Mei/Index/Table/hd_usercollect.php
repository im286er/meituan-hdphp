<?php if(!defined('HDPHP_PATH'))exit;
return array (
  'gid' => 
  array (
    'field' => 'gid',
    'type' => 'int(11)',
    'null' => 'NO',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'user_uid' => 
  array (
    'field' => 'user_uid',
    'type' => 'int(11)',
    'null' => 'NO',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
);
?>