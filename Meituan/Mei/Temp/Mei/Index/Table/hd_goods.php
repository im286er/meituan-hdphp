<?php if(!defined('HDPHP_PATH'))exit;
return array (
  'gid' => 
  array (
    'field' => 'gid',
    'type' => 'int(10) unsigned',
    'null' => 'NO',
    'key' => true,
    'default' => NULL,
    'extra' => 'auto_increment',
  ),
  'title' => 
  array (
    'field' => 'title',
    'type' => 'varchar(45)',
    'null' => 'NO',
    'key' => false,
    'default' => '',
    'extra' => '',
  ),
  'subtitle' => 
  array (
    'field' => 'subtitle',
    'type' => 'varchar(100)',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'price' => 
  array (
    'field' => 'price',
    'type' => 'decimal(10,2) unsigned',
    'null' => 'NO',
    'key' => false,
    'default' => '0.00',
    'extra' => '',
  ),
  'group_price' => 
  array (
    'field' => 'group_price',
    'type' => 'decimal(10,2)',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'pic' => 
  array (
    'field' => 'pic',
    'type' => 'varchar(300)',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'cid' => 
  array (
    'field' => 'cid',
    'type' => 'int(10) unsigned',
    'null' => 'NO',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'sid' => 
  array (
    'field' => 'sid',
    'type' => 'int(11)',
    'null' => 'NO',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'sell_num' => 
  array (
    'field' => 'sell_num',
    'type' => 'int(11)',
    'null' => 'YES',
    'key' => false,
    'default' => '0',
    'extra' => '',
  ),
  'comment_num' => 
  array (
    'field' => 'comment_num',
    'type' => 'int(11)',
    'null' => 'YES',
    'key' => false,
    'default' => '0',
    'extra' => '',
  ),
  'sendtime' => 
  array (
    'field' => 'sendtime',
    'type' => 'int(10)',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
);
?>