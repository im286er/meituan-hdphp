<?php if(!defined('HDPHP_PATH'))exit;
return array (
  'user_uid' => 
  array (
    'field' => 'user_uid',
    'type' => 'int(11)',
    'null' => 'NO',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'sex' => 
  array (
    'field' => 'sex',
    'type' => 'tinyint(1)',
    'null' => 'NO',
    'key' => false,
    'default' => '0',
    'extra' => '',
  ),
  'birthday' => 
  array (
    'field' => 'birthday',
    'type' => 'date',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'tel' => 
  array (
    'field' => 'tel',
    'type' => 'char(11)',
    'null' => 'NO',
    'key' => false,
    'default' => '',
    'extra' => '',
  ),
  'points' => 
  array (
    'field' => 'points',
    'type' => 'int(10)',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'moeny' => 
  array (
    'field' => 'moeny',
    'type' => 'decimal(10,2)',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'face' => 
  array (
    'field' => 'face',
    'type' => 'varchar(100)',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'shenfen' => 
  array (
    'field' => 'shenfen',
    'type' => 'tinyint(1)',
    'null' => 'YES',
    'key' => false,
    'default' => '1',
    'extra' => '',
  ),
  'qingkuang' => 
  array (
    'field' => 'qingkuang',
    'type' => 'tinyint(1)',
    'null' => 'YES',
    'key' => false,
    'default' => '1',
    'extra' => '',
  ),
  'hobby' => 
  array (
    'field' => 'hobby',
    'type' => 'varchar(30)',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
);
?>