<?php 
class LoginControl extends Control{
	public $model; //定义模型变量

	public function __init(){
		$this->model = K('User'); //实例化模型
	}

	/**
	 * [index 显示登录页]
	 * @return [type] [description]
	 */
	public function index(){
		// 获得登陆前的页面url
		if(session('uid')) go(U('Index/index'));
		if(IS_POST){
			$userinfo = $this->model->login();
			if($userinfo){
				session('username',$userinfo['username']);
				session('uid',$userinfo['uid']);
				// 记录用户登录时间
				$this->model->edit(array('uid'=>session('uid')),array('logintime'=>time()));
				// 将session里的购物车商品存入数据库
				$this->cart();
				if(session('backurl')) $this->success('登陆成功',session('backurl'));
				$this->success('登陆成功','Index/index');
			}

			$this->error('用户名或密码错误');
		}
		$backurl = $_SERVER["HTTP_REFERER"];
		session('backurl',$backurl);
		$this->display();
	}
/**
 * [quit 用户退出登陆]
 * @return [type] [description]
 */
	public function quit(){
		session('username',null);
		session('uid',null);
		$this->success('退出成功！');
	}
/**
 * [cart 将本地加入在 session 里的购物车商品写入数据库]
 * @return [type] [description]
 */
	public function cart(){
			$uid = session('uid');
			// 检测本地 session 里是否有购物车物品
			$session_cart = session('cart');
			// 获得数据库中购物车商品
			$cart = K('Cart')->get_use_cart(session('uid'));
			// 如果本地存在购物车物品
			if($session_cart){
				// 如果数据库中该用户的购物车中已有商品
				if($cart){
					
					foreach ($session_cart as $k => $v) {
						$is_have = true; // 定义变量，用来标识购物车中是否已经存在该商品 true 状态标示不存在，即可以添加进数据库
						foreach ($cart as $key => $value) {
							if($v['gid'] == $value['gid']){
								$is_have = false;
							}
						}
						if($is_have){
							$data = array(
								'uid'		=> $uid,
								'gid'		=> $v['gid'],
								'goods_num' => $v['num'],
								'sendtime'  => time()
							);
							K('Cart')->add_data($data);
						}
					}
				}else{// 如果数据库中该用户的购物车中没有商品
					foreach ($session_cart as $k => $v) {
						$data = array(
							'uid'		=> $uid,
							'gid'		=> $v['gid'],
							'goods_num' => $v['num'],
							'sendtime'  => time()
							);
						K('Cart')->add_data($data);
					}
				}
				session('cart',null); // 清空本地购物车
			}
			
	}


}


 ?>