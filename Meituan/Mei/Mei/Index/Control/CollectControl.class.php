<?php 
class CollectControl extends CommonControl{
	private $model;

	public function __init(){
		$this->model = K('Collect');
	}

	public function collect(){
		if(!session('uid')){
			echo 0; //返回0，代表用户没登陆
			die();
		}
		$gid = Q('post.gid','','intval');
		$uid = session('uid');
		$this->model->add_data(array('gid'=>$gid,'user_uid'=>$uid));
		echo 1; //添加成功，返回 1
		die();
	}

	public function del_collect(){
		$gid = Q('gid','','intval');
		$uid = session('uid');
		$this->model->del_data(array('gid'=>$gid,'user_uid'=>$uid));
		$this->success('删除成功！');
	}
}

