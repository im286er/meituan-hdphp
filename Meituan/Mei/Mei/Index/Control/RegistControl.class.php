<?php 
class RegistControl extends CommonControl{
	public $model;

	public function __init(){
		$this->model = K('User');
	}
	/**
	 * [index 载入注册页]
	 * @return [type] [description]
	 */
	public function index(){
		// 执行公共控制器 CommonControl 类里的公共方法，获得所有分类数据
      	$this->all_category();
      	// 执行执行公共控制器 CommonControl 类里的公共方法，获得用户的购物车商品
        $cart = $this->cart();

		$this->display();
	}
	/**
	 * [ajax_check_code 异步判断验证码是否正确]
	 * @return [type] [description]
	 */
	public function ajax_check_code(){
		if(IS_AJAX){
			// 接收ajax传输过来的code值
				$code = Q('post.code','','strtolower');
				if($code == strtolower(session('code'))){
					echo 1;
					die();
				}else{
					echo 0;
					die();
				}
			
		}
	}
	/**
	 * [ajax_check_email 异步验证邮箱是否存在]
	 * @return [type] [description]
	 */
	public function ajax_check_email(){
		$email = Q('post.email');
		if($this->model->is_have('email',$email)){
			echo 0;
			die();
		}
		echo 1;
		die();
	}
	/**
	 * [ajax_check_username 异步验证用户名是否正确]
	 * @return [type] [description]
	 */
	public function ajax_check_username(){
		$username = Q('post.username');
		if($this->model->is_have('username',$username)){
			echo 0;
			die();
		}
		echo 1;
		die();
	}

	/**
	 * [user_regist 处理注册信息]
	 * @return [type] [description]
	 */
	public function add_user(){
		if(IS_POST){
			// 验证码是否为空
			$code = Q('post.code','','trim,strtolower');
			if(!$code){
				$this->error('验证码不能为空');
			}
			// 验证码是否正确
			if($code != strtolower(session('code'))){
				$this->error('验证码不正确');
			}
			// 密码加密、
			$_POST['password'] = md5(Q('post.password'));
			// 注册时间
			$_POST['registtime'] = time();
			// 自动验证
			if(!$this->model->create()){
				$this->error($this->model->error);
			}
			$user_uid = $this->model->add();
			K('Userinfo')->add_data(array('user_uid'=>$user_uid,'points'=>1000));
			die();
			$this->success('注册成功',U('Index/index'));
		}
	}


	/**
	 * [code 注册页验证码]
	 * @return [type] [description]
	 */
	public function code(){
		$obj = new Code();
		$obj -> show();
	}
}


 ?>