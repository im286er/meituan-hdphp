<?php 
class BuyControl extends CommonControl{

/**
 * [buy1 立即购买页面]
 * @return [type] [description]
 */
	public function buy1(){
		// 执行公共控制器 CommonControl 类里的公共方法，获得所有分类数据
	     $this->all_category();
	    // 执行执行公共控制器 CommonControl 类里的公共方法，获得用户的购物车商品
	     $cart = $this->cart();

		$gid = Q('post.gid','','intval');
		$num = Q('post.num','','intval');
		// 获取商品信息
		$goods = K('Goods')->get_one($gid);
		// 获取第一张图片作为缩略图
		$pic = $goods['pic'];
		$pos = strpos($pic,';');
		$pic = substr($pic, 0,$pos);
		$goods['pic'] = $pic;
		// 将商品数量压入数组
		$goods['num'] = $num;
		
		$this->assign('goods',$goods);
		$this->display();
	}
/**
 * [buy2 通过立即购买提交的订单页面，将订单写进购物车]
 * @return [type] [description]
 */
	public function buy2(){
		if(!session('uid')) {
			go(U('Login/index'));
			die();
		}

		// 执行公共控制器 CommonControl 类里的公共方法，获得所有分类数据
	     $this->all_category();
	    // 执行执行公共控制器 CommonControl 类里的公共方法，获得用户的购物车商品
	     $cart = $this->cart();
	     // 商品详情页点击立即抢购进来的
	     if(IS_POST){
	     	 $data = Q('post.');
		     // 计算总金额
		     $subtotal = $data['goods_num'] * $data['group_price'];
		     $data['subtotal'] = $subtotal;
		     $data['uid'] = session('uid');
		     $data['sendtime'] = time();
		      // 将订单写进数据库,并获得订单id
		      $order_id = K('Order')->add_data($data);
		      $data['order_id'] = $order_id;
		      $this->assign('data',$data);
	     }
	     // 个人中心点击在订单详情页，点击未付款的商品进来的
	     if(IS_GET){
	     	$order_id = Q('get.order_id','','intval');
	     	$data = K('Order')->get_one(array('order_id'=>$order_id));
	     	$data['num'] = $data['goods_num'];
	     	$gid = $data['gid'];
	     	$goods = K('Goods')->get_one($gid);
	     	$data['title'] = $goods['title'];
	     	$data['uid'] = session('uid');
	     	$this->assign('data',$data);
	     }
	     

	     // 获得用户金币
   		$userinfo = K('UserInfo')->get_data(session('uid'));
   		$this->assign('points',$userinfo['points']);
	  

	     
		 $this->display();
	}

}

