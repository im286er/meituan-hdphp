<?php 
/*购物车控制器类*/
class CartControl extends CommonControl{
	private $model;

	public function __init(){
		$this->model = K('Cart');
	}

/**
 * [cart1 第一步。查看购物车]
 * @return [type] [description]
 */
	public function cart1(){
		// 执行公共控制器 CommonControl 类里的公共方法，获得所有分类数据
      	$this->all_category();
      	// 执行执行公共控制器 CommonControl 类里的公共方法，获得用户的购物车商品
        $cart = $this->cart();
       // 计算购物车总物品数与 20 的百分比
       $this->assign('per',(count($cart)/20)*100);

		$this->display();
	}

	public function cart2(){
			// 执行公共控制器 CommonControl 类里的公共方法，获得所有分类数据
	      	$this->all_category();
	      	// 执行执行公共控制器 CommonControl 类里的公共方法，获得用户的购物车商品
	        $cart = $this->cart();

	       	if(IS_POST){
	       		if(!session('uid')){
	       			go(U('Login/index'));
	       			die();
	       		}
	       		// 接受表单数据
	       		$data = Q('post.'); 
	       		// 整理数据
	       		$tempArr = array();
	       	   	foreach ($data['gid'] as $v) {
	       			$tempArr[] = array(
	       				'uid'			=> session('uid'),
	       				'gid'			=> $v['gid'],
	       				'title'			=> $data["title_".$v['gid']],
	       				'group_price'	=> $data["group_price_".$v['gid']],
	       				'goods_num'		=> $data["num_".$v['gid']],
	       				'subtotal'		=> $data["group_price_".$v['gid']] * $data["num_".$v['gid']],
	       				'sendtime'		=> time()
	       				);
	       		}
	       		// 对整理好的数据插入数据库，并计算商品总价格
	       		$total = 0; //定义总价格变量
	       		$order_id = ''; //定义变量，记录订单编号
	       		foreach ($tempArr as $v) {
	       			$order_id .= K('Order')->add_data($v) . ',';
	       			$total = $total + $v['subtotal'];
	       		}
	       		$this->assign('order_id',$order_id);
	       		$this->assign('total',round($total,2));
	       		$this->assign('order',$tempArr);
	       		// 删除购物车中的商品
	       		$this->model->del_data_byuid(session('uid'));
	       		// 获得用户金币
	       		$userinfo = K('UserInfo')->get_data(session('uid'));
	       		$this->assign('points',$userinfo['points']);
       		}

        $this->display();
	}
/**
 * [pay 付款]
 * @return [type] [description]
 */
	public function pay(){
		// 获得应付金币，用户剩余金币、订单 id
		$pay = Q('post.pay','','intval');
		$points = Q('post.points','','intval');
		$order_id = rtrim(Q('post.order_id'),',');
		$order_id = explode(',', $order_id);
		// 为防止重复付款，故先判断是否付款
		$orderInfo = K('Order')->get_one(array('order_id'=>$order_id));
		if($orderInfo['state'] == 1) 
		$this->error('该订单已付款','Index/index');
		// 余额不足，暂时跳转至首页
		if($pay > $points){
			$this->error('金币不足！',U('Index/index'));
		}
		// 付款，数据库扣除用户金币
		$points = $points - $pay;
		K('UserInfo')->edit(session('uid'),array('points'=>$points));
		// 修改订单状态为已付款
		foreach ($order_id as $v) {
			K('Order')->edit($v,array('state'=>1));
		}
		// 修改商品已售出数量
		$gid = Q('post.gid','','intval');
		$goods = K('Goods')->get_one($gid);
		// 售出总数量
		$sell_num = (int)$goods['sell_num'] + (int)Q('post.num','','int');

		K('Goods')->edit(array('gid'=>$gid),array('sell_num'=>$sell_num));

		$this->success('付款成功！','Index/index');
	}
	/**
	 * [go_cart 加入购物车]
	 * @return [type] [description]
	 */
	public function go_cart(){

		$gid = Q('post.gid','','intval');
		// 加入购物车的数量
		$num = Q('post.num');
		$data = K('Goods')->get_one($gid);
		// 获得图片，把字符串格式转为数组，方便调用
		$pic = $data['pic'];
		$picArr = explode(';', rtrim($pic,';'));
		$data['pic'] = $picArr;
		 // 组合要加入购物车的商品信息
		$newCart = array(
				'gid' 			=> $gid,
				'pic'			=> $data['pic'][0],
				'title'			=> $data['title'],
				'group_price'	=> $data['price'],
				'num' 			=> $num
			);
		// 如果用户没有登陆，将购物车存入 session
		if(!session('uid')){
			if(is_null(session('cart'))){
				$cart = array();	
				$cart[] = $newCart;
				session('cart',$cart);
				$str = $this->session_cartStr($gid,session('cart')); // 组合字符串
				echo $str;
				die();
			}else{
				$cart = session('cart');

				// 判断该商品是否已经加入 session
				foreach ($cart as $k => $v) {
					if($v['gid'] == $gid){
						$cart[$k]['num'] = $v['num'] + $num; // 如果已经加入购物车，就在此基础上增加数量
						session('cart',$cart);
						$str = $this->session_cartStr($gid,session('cart')); // 组合字符串
						echo 1;
						die();
					}
				}
				 //计算已经加入session购物车的商品个数 ， 不可超过 20 件商品
				$cart_num = count($cart);
				if($cart_num < 20){
					array_unshift($cart, $newCart);
					session('cart',$cart);
					$str = $this->session_cartStr($gid,session('cart')); // 组合字符串
					echo $str;
					die();
				}else{
					echo 0;
					die();
				}
			}
			
		}else{ //如果用户登陆了
			$uid = session('uid'); //获得用户的id
			// 从购物车表中获取该用户的已加入购物车的商品
			$userCart = $this->model->get_use_cart($uid);
			// 组合要加入购物车表的信息
			$data = array(
					'uid'		=> $uid,
					'gid' 		=> $gid,
					'goods_num' => $num,
					'sendtime'  => time()
					);
			if(is_null($userCart)){ // 如果购物车表中没有该用户的数据，那么直接将该商品加入购物车
				
				$this->model->add_data($data); // 将该商品加入购物车
				$userCart = $this->get_cart(); // 查找该用户加入购物车的所有商品
				$str = $this->session_cartStr($gid,$userCart);
				echo $str;
				die();
			}else{ 
				/*如果购物车表中已经存在该用户加入购物车的数据
					对数据循环操作，如果该商品已加入购物车，就在此基础上，修改原数量为此时的数量*/
				foreach ($userCart as $k => $v) {
					if($v['gid'] == $gid){
						$this->model->edit(array('gid'=>$gid),array('goods_num'=>$num));
						echo 1;
						die();
					}
				}

				// 如果走到这里，说明购物车里尚未添加该商品
				$this->model->add_data($data); // 将该商品加入购物车
				$userCart = $this->get_cart(); // 查找该用户加入购物车的所有商品
				$str = $this->session_cartStr($gid,$userCart);
				echo $str;
				die();
			}
		}
	}
/**
 * [session_cartStr 循环 session 购物车或者 数据库购物车里的数据，得到html字符串]
 * @param  [type] $gid     [商品 gid]
 * @param  [type] $cartArr [本地购物车 session 数组 ，或者数据库中购物车数组]
 * @return [type]          [description]
 */
	public function session_cartStr($gid,$cartArr){

		$str = '<ul class="viewed">';
		foreach ($cartArr as $v) {
			$str .= "<li>
					<div class='tu'><a href='".__APP__."/Article/index/gid/{$gid}'><img src='".__ROOT__."/{$v['pic']}' width='80' height='49' /></a></div>
					<div class='info'>
						<p><a href=href='".__APP__."/Article/index/gid/{$gid}'>{$v['title']}</a></p>
						<p class='p2'><span>￥{$v['group_price']}</span><a class='del top_del_cart' href='javascript:' gid={$gid}>删除</a></p>
					</div>
				</li>";
		}
		$str .= "</ul><p class='go-gwche'>
<a href=".U('Cart/cart1').">查看我的购物车</a>
</p>";
		return $str;
	}
/**
 * [get_cart 从数据库获得购物车中的商品，并组合数组，用ajax改变头部购物车内容]
 * @return [type] [description]
 */
	public function get_cart(){
		$uid = session('uid');
        $cart = K('Cart')->get_use_cart($uid);

        // 循环购物车数据，重组数组
        foreach ($cart as $k => $v) {
            $data = K('Goods')->get_one($v['gid']);
            // 获得图片，把字符串格式转为数组，方便调用
            $pic = $data['pic'];
            $picArr = explode(';', rtrim($pic,';'));
            $data['pic'] = $picArr;
            $cart[$k] = array(
                'gid'           => $v['gid'],
                'pic'           => $data['pic'][0],
                'title'         => $data['title'],
                'group_price'   => $data['price'],
                );

        }
        return $cart;
	}
/**
 * [top_del_cart 异步删除头部购物车商品]
 * @return [type] [description]
 */
	public function ajax_top_del_cart(){
		$gid = Q('post.gid','','intval');

		if(!session('uid')){
			$session_cart = session('cart');
			foreach ($session_cart as $k => $v) {
				if($v['gid'] == $gid){
					unset($session_cart[$k]);
					session('cart',$session_cart);
					echo 1;
					die();
				}
			}
		}else{
			$this->model->del_data(session('uid'),$gid);
			echo 1;
			die();
		}
	}
}


