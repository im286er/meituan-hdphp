<?php 
// 当访问的控制器不存在时,自动访问该控制器
class EmptyControl extends Control{
	public function index(){
		$this->error('您访问的页面不存在',__ROOT__);
	}
}