<?php 
class MemberControl extends CommonControl{

      public function __init(){
            if(!session('uid')) go(U('Login/index'));
            // 执行公共控制器 CommonControl 类里的公共方法，获得所有分类数据
            $this->all_category();
            // 执行执行公共控制器 CommonControl 类里的公共方法，获得用户的购物车商品
            $this->cart();
            // 获得用户信息
            $this->get_info();
      }

      public function get_info(){
          $uid = session('uid');
          $userinfo = K('UserView')->get_one($uid);

          $this->assign('userinfo',$userinfo);
      }
/**
 * [order 订单列表]
 * @return [type] [description]
 */
	public function order(){
		
      	// 获取该用户的订单
      	$uid = session('uid');
      	$order = K('Order')->get_data(array('uid'=>$uid));
      	$orderInfo = array();
      	if(is_array($order)){
      		foreach ($order as $k => $v) {
      			$gid = $v['gid'];
      			$goodsInfo = K('GoodsView')->get_seller_info(array('gid'=>$gid));
      			// 取出第一张图片作为缩略图
      			$picpos = strpos($goodsInfo[0]['pic'], ';');
      			$pic = substr($goodsInfo[0]['pic'], 0,$picpos);
      			$orderInfo[] = array(
      				'gid'		=> $v['gid'],
      				'order_id' 	=> $v['order_id'],
      				'pic'		=> $pic,
      				'title'		=> $goodsInfo[0]['title'],
      				'goods_num'	=> $v['goods_num'],
      				'subtotal'	=> $v['subtotal'],
      				'close_date'=> $goodsInfo[0]['close_date'],
              'state'   => $v['state'],
              'is_comments'   => $v['is_comments'],
      				);
      		}
                  // 通过select 筛选不同状态的订单
                  $state = Q('get.state');
                  // 未付款的订单
                  if($state == 'unpaid'){
                        foreach ($orderInfo as $k => $v) {
                              if($v['state'] == 1){
                                    unset($orderInfo[$k]);
                              }
                        }
                  }
                  // 已付款的订单
                  if($state == 'paid'){
                        foreach ($orderInfo as $k => $v) {
                              if($v['state'] == 0){
                                    unset($orderInfo[$k]);
                              }
                        }
                  }
      		$this->assign('orderInfo',$orderInfo);
      	}
		$this->display();
	}
/**
 * [del_order 删除订单]
 * @return [type] [description]
 */
	public function del_order(){
		$order_id = Q('get.order_id','','intval');
		K('Order')->del_data(array('order_id'=>$order_id));
		$this->success('删除成功！');
	}
/**
 * [collect 收藏列表]
 * @return [type] [description]
 */
      public function collect(){
            $uid = session('uid');
            $collect = K('Collect')->get_data(array('user_uid'=>$uid));
            if(is_null($collect)){
                  $this->assign('collect',null);
            }else{
                  foreach ($collect as $k => $v) {
                       $goodsInfo = K('GoodsView')->get_seller_info(array('gid'=>$v['gid']));
                       $goodsInfo = $goodsInfo[0];
                       $collect[$k]['title'] = $goodsInfo['title'];
                       // 取第一章图片
                       $pic = $goodsInfo['pic'];
                       $picpos = strpos($pic, ';');
                       $pic = substr($pic, 0,$picpos);
                       $collect[$k]['pic'] = $pic;
                       // 团购价
                       $collect[$k]['group_price'] = $goodsInfo['group_price'];
                       if(date('Y-m-d') <= $goodsInfo['close_date']){
                            $collect[$k]['state'] = '进行中';
                       }else{
                            $collect[$k]['state'] = '已关闭';
                       }
                       
                  }

                 $this->assign('collect',$collect);
            }

            $this->display();
      }
/**
 * [userinfo 用户这里修改页]
 * @return [type] [description]
 */
    public function userinfo(){
      // 获取用户信息
      $uid = session('uid');
      $userinfo = K('Userinfo')->get_data(array('uid'=>$uid));
      $hobby = $userinfo['hobby'];
      $hobby = explode(',', $hobby);
      $userinfo['hobby'] = $hobby;
      
      $this->assign('userinfo',$userinfo);
      // 处理用户更改的信息
      if(IS_POST){

         $birthday = Q('post.year1') . '-' .Q('post.month1') . '-' .Q('post.day1');
         $_POST['birthday'] = $birthday;
         $hobby = '';
         foreach ($_POST['hobby'] as $v) {
           $hobby .= $v . ',';
         }
         $hobby = rtrim($hobby,',');
         $_POST['hobby'] = $hobby;
         $_POST['user_uid'] = session('uid');
         
        k('Userinfo')->edit(array('user_uid'=>$uid));
        $this->success('修改成功!');
      }
        
        $this->display();
    }
/**
 * [address 收获地址]
 * @return [type] [description]
 */
    public function address(){
        $uid = session('uid');
        $address = K('address')->get_data(array('user_uid'=>$uid));
        $this->assign('address',$address);
        // 判断是否是默认地址
        
        $this->display();
    }
/**
 * [add_address description]
 */
    public function add_address(){
      if(IS_POST){
            $uid = session('uid');
            $s_province = isset($_POST['s_province']) ? Q('s_province') : '';
            $s_city = isset($_POST['s_city']) ? Q('s_city') : '';
            $s_county = isset($_POST['s_county']) ? Q('s_county') : '';
            $address = $s_province .','. $s_city .','. $s_county;
            $_POST['address'] = $address;
            $_POST['user_uid'] = $uid;

            K('Address')->add_data(array('user_uid'=>$uid));
            $this->success('添加成功！',U('Member/address'));
      }
      
      $this->display();
    }
/**
 * [set_default 设置默认地址]
 */
    public function set_default(){
        $uid = session('uid');
        $address_id = Q('get.address_id');
        // 先让该用户的其他地址改为不是默认地址
        K('Address')->edit(array('user_uid'=>$uid),array('is_default'=>0));
        // 修改此地址为默认地址
        K('Address')->edit(array('address_id'=>$address_id),array('is_default'=>1));
        $this->success('修改成功！');
    }
/**
 * [del_address 删除地址]
 * @return [type] [description]
 */
    public function del_address(){
        $address_id = Q('get.address_id','','intval');

        K('Address')->del_address(array('address_id'=>$address_id));
        $this->success('删除成功！');
    }
/**
 * [edit 修改地址]
 * @return [type] [description]
 */
    public function edit(){
      if(IS_POST){

           $s_province = isset($_POST['s_province']) ? Q('s_province') : '';
           $s_city = isset($_POST['s_city']) ? Q('s_city') : '';
           $s_county = isset($_POST['s_county']) ? Q('s_county') : '';
           $address = $s_province .','. $s_city .','. $s_county;
           $_POST['address'] = $address;
           K('Address')->edit();
           $this->success('修改成功！',U('Member/address'));
      }
        $address_id = Q('get.address_id','','intval');
        $data = K('Address')->get_data(array('address_id'=>$address_id));
        $this->assign('data',$data[0]);

        $this->display();
    }
/**
 * [face 用户头像]
 * @return [type] [description]
 */
    public function face(){
        $obj = new Upload();
        $faceinfo = $obj->upload();
        $facepath = $faceinfo[0]['path'];
        $uid = session('uid');
        K('Userinfo')->edit(array('user_uid'=>$uid),array('face'=>$facepath));
        $this->success('修改成功');
    }
/**
 * [basicInfo 用户基本信息]
 * @return [type] [description]
 */
    public function basicInfo(){
        $uid = session('uid');
        $userinfo = K('User')->get_one(array('uid'=>$uid));
        $this->assign('userinfo',$userinfo);

        $this->display();
    }

    public function edit_info(){
        if(IS_POST){
           $password = Q('post.password');
           $password2 = Q('post.password2');
           $nick = Q('post.nick');
           // 判断两次密码是否一致
           if($password2 != $password){
              $this->error('两次密码不一样');
           }
           // 判断旧密码是否正确
           $data = K('User')->where(array('uid'=>session('uid'),'password'=>md5($password)))->get_one();
           if(is_null($data)){
               $this->error('对不起，你的旧密码输入不正确');
           }

           K('User')->edit(array('uid'=>session('uid')),array('nick'=>$nick,'password'=>md5($password)));
           $this->success('修改成功',U('Member/basicInfo'));
           $nick = Q('nick');
        }
        $uid = session('uid');
        $userinfo = K('User')->get_one(array('uid'=>$uid));
        $this->assign('userinfo',$userinfo);

        $this->display();
    }

}