<?php
//首页控制器类
class IndexControl extends CommonControl{

   public function index(){

        // 执行公共控制器 CommonControl 类里的公共方法，获得所有分类数据
      	$this->all_category();
        // 取得每个顶级分类下的商品(用在每楼层的商品数据)
        $this->get_floor_goods();
        // 获得全部区域
        $quyuArr = K('Attr')->get_value(1);
        $this->assign('quyuArr',$quyuArr);
        // 获得热门区域
        $tempArr = array(0,2,4,6,8,10,11,12,15);
        $hotqu = array();
        foreach ($tempArr as $v) {
            $hotqu[] = $quyuArr[$v];
        }
        $this->assign('hotqu',$hotqu);
        // 本周精选
        $this->week();
        // 执行执行公共控制器 CommonControl 类里的公共方法，获得用户的购物车商品
        $this->cart();

        // 载入首页模板
        $this->display();
    }

/**
 * [get_cate_goods 获得各分类下的商品 公共方法 传入 顶级分类 cid 即可获得数据]
 * @param  [type] $cid [顶级分类 cid ]
 * @return [type]      [description]
 */
    public function get_cate_goods($cid){
         //  获得顶级分类的子分类 cid
            $cidArr = K('Category')->get_fields(array('pid'=>$cid),'cid');
            if(!$cidArr) return $goodsArr = null;
                 // 2. 将 $cidArr 组合成方便操作的一维数据
                $tempArr = array();
                if(is_array($cidArr)){
                    foreach ($cidArr as $v) {
                        $tempArr[] = $v['cid'];
                    }
                }
                // 获得符合条件的商品
                $goodsArr = K('Goods')->get_data2($tempArr , '0,12');
              
                // 取出第一张图片路径，作为缩略图
               if(is_array($goodsArr)){
                    foreach ($goodsArr as $k => $v) {
                        $pos = strpos($v['pic'], ';');
                        $firstPicPath = substr($v['pic'], 0,$pos);
                        $goodsArr[$k]['pic'] = $firstPicPath;
                    }
               }
           
           return $goodsArr;
         
    }
/**
 * [get_floor_goods 获得首页每楼下是商品]
 * @return [type] [description]
 */
    public function get_floor_goods(){
         // 美食商品 顶级分类 cid = 1
           $meishi = $this->get_cate_goods(1);
        // 酒店商品 顶级分类 cid = 2
           $hotel = $this->get_cate_goods(2);
        // 电影商品 顶级分类 cid = 3
           $movie = $this->get_cate_goods(3);
        // 休闲娱乐商品 顶级分类 cid = 4
           $recreation = $this->get_cate_goods(4);
        // 旅游商品 顶级分类 cid = 5
           $travel = $this->get_cate_goods(5);
        // 生活服务商品 顶级分类 cid = 6
           $lifeServer = $this->get_cate_goods(6);
        // 购物商品 顶级分类 cid = 7
           $shopping = $this->get_cate_goods(7);
        // 丽人商品 顶级分类 cid = 8
           $beauty = $this->get_cate_goods(8);
           // 自定义分类名称数组
           $cateNameArr = array(
                'meishi'        => $meishi,
                'hotel'         => $hotel,
                'movie'         => $movie,
                'recreation'    => $recreation,
                'travel'        => $travel,
                'lifeServer'    => $lifeServer,
                'shopping'      => $shopping,
                'beauty'        => $beauty,
            );
           // 循环分配
           foreach ($cateNameArr as $k => $v) {
              $this->assign($k,$v);

           }
    }
/**
 * [area 点击首页全部区域的页面]
 * @return [type] [description]
 */
    public function area(){
       // 执行公共控制器 CommonControl 类里的公共方法，获得所有分类数据
        $this->all_category();
        // 取得每个顶级分类下的商品(用在每楼层的商品数据)
        $this->get_floor_goods();
        // 获得所有的顶级分类
        $topcate = K('Category')->get_all(array('pid'=>0));
        // 获得各分类下的商品数量
        foreach ($topcate as $k => $v) {
          // 获得分类的子分类 cid
            $son_cid = K('Category')->get_all(array('pid'=>$v['cid']));
            // 重组数组
            $tempArr = array();
            if($son_cid){
                foreach ($son_cid as $v) {
                    $tempArr[] = $v['cid'];
                }
            }
            // 获得该分类子分类下的所有的商品
            if($tempArr){
                $goodsArr = K('Goods')->get_data(array('cid'=>$tempArr));
            }else{
                $goodsArr = array();
            }
            $topcate[$k]['count'] = count($goodsArr);
        }
        //分配顶级分类
        $this->assign('topcate',$topcate);
        // 获得全部区域
        $quyuArr = K('Attr')->get_value(1);
       
        // 计算每个区域下有有多少商品
        foreach ($quyuArr as $k => $v) {
            $gidArr = K('GoodsAttr')->get_data_byatid($v['atid']);
            $quyuArr[$k]['count']=count($gidArr);
        }
        $this->assign('quyuArr',$quyuArr);
        // 获得区域 atid
        $atid = Q('get.atid','','intval');
        // 通过 atid 获得商品 gid
        $gidArr = K('GoodsAttr')->get_data_byatid($atid);
        // 重组数组
        $tempArr = array();
        if(is_array($gidArr)){
            foreach ($gidArr as $v) {
                 $tempArr[] = $v['goods_gid'];
            }
        }
        // 定义order
        $order = '';
        // 默认排序是 asc
        $pai = 'asc';
         // 获取排序条件
          
          $sort = isset($_GET['sort']) ? $_GET['sort'] : '';
          // 将sort 拆分 (例如：time-desc用‘-’拆分)
          
          if($sort){
              $pos = strpos($sort, '-');
              $pai = substr($sort, $pos+1);
              $sort = substr($sort, 0,$pos);
              $this->assign('sort',$sort);
              if(is_null($sort) || empty($sort) || $sort == 'default') $order = '';
              if($sort == 'sell') $order = "sell_num  $pai";
              if($sort == 'price')  $order = "group_price $pai";
              if($sort == 'time')  $order = "sendtime $pai";
              if($pai == 'asc'){
                 $pai = 'desc';
              }else{
                  $pai = 'asc';
              }
              
          }
         $this->assign('pai',$pai);
        // 获得商品信息
         if($tempArr){
            $goodsArr = K('Goods')->get_data(array('gid'=>$tempArr) ,'',$order);
            // 取出第一张图片路径，作为缩略图
           if(is_array($goodsArr)){
                foreach ($goodsArr as $k => $v) {
                    $pos = strpos($v['pic'], ';');
                    $firstPicPath = substr($v['pic'], 0,$pos);
                    $goodsArr[$k]['pic'] = $firstPicPath;
                }
           }

            $this->assign('goodsArr',$goodsArr);
       }else{
            $this->assign('goodsArr',array());
       }
       
        $this->display();
    }

    public function week(){
        $weekgoods = K('Goods')->get_data('','','sell_num desc',10);
        
         if(is_array($weekgoods)){
              foreach ($weekgoods as $k => $v) {
                // 取出第一张图片路径，作为缩略图
                  $pos = strpos($v['pic'], ';');
                  $firstPicPath = substr($v['pic'], 0,$pos);
                  $weekgoods[$k]['pic'] = $firstPicPath;
                  // 计算折扣
                  $zhe = round(($weekgoods[$k]['group_price'] / $weekgoods[$k]['price'])*10,1);
                  $weekgoods[$k]['zhe'] = $zhe;

              }
         }
         // 通过实体价和团购价 计算折扣
        
        $tempArr = array();
        foreach ($weekgoods as $k => $v) {
            if($k%2 == 0){
                $tempArr[$k] = array($v);
            }else{
                $tempArr[$k-1][] = $v;
            }
        }
        $this->assign('weekgoods',$tempArr);
    }
}
?>