<?php 
/**
 * 内容页控制器类
 */
class ArticleControl extends CommonControl{

	public function index(){
		// 执行公共控制器 CommonControl 类里的公共方法，获得所有分类数据
      	$this->all_category();
      	// 获得商品的详细信息，并加入浏览记录里
      	$this->get_goods();
      	// 获得收藏该商品的数量
      	$this->get_collect();
      	// 执行执行公共控制器 CommonControl 类里的公共方法，获得用户的购物车商品
      	$this->cart();
      	// 获得分店地址
      	$this->get_location();
      	// 获得商品评价
      	$this->get_comments();
      	// 获得当前位置
      	$this->location();

		$this->display();

	}
	
/**
 * [get_goods 获得商品详细信息]
 * @return [type] [description]
 */
	public function get_goods(){
		$gid = Q('get.gid','','intval');
		$data = K('GoodsView')->get_one($gid);
		// 通过实体价和团购价 计算折扣
		$zhe = round(($data['group_price'] / $data['price'])*10,1);
		$data['zhe'] = $zhe;
		// 获得图片，把字符串格式转为数组，方便调用
		$pic = $data['pic'];
		$picArr = explode(';', rtrim($pic,';'));
		$data['pic'] = $picArr;
		// 获得服务承诺,组合数组
		$promise = $data['promise'];
		$promiseArr = explode(',', $promise);
		$data['promise'] = $promiseArr;
		$this->assign('data',$data);

		/*将该被浏览的商品信息存入 session*/
		 // 组合要记录的商品信息
		$newViewed = array(
				'gid' 			=> $gid,
				'pic'			=> $data['pic'][0],
				'title'			=> $data['title'],
				'price'			=> $data['price'],
				'group_price'	=> $data['group_price']
			);
		if(is_null(session('viewed'))){
			$viewed = array();	
			$viewed[] = $newViewed;
		}else{
			$viewed = session('viewed');
			// 判断是否浏览商品是否已经记录，如果记录，就不重复记录，无操作
			$is_have = false; // 定义标识，用来判断，浏览的该商品是否已经记录，默认没记录
			foreach ($viewed as $v) {
				if($v['gid'] == $gid){
					$is_have = true; // 如果已经记录，就改变标识 为 true
				}
			}
			if(!$is_have){
				$viewed_num = count($viewed); //计算已经记录的商品个数
				if($viewed_num <5){
					array_unshift($viewed, $newViewed);
				}else{
					array_pop($viewed);
					array_unshift($viewed, $newViewed);
				}
			}
		}
		session('viewed',$viewed);
	}
/**
 * [get_viewed 获得浏览过的商品]
 * @return [type] [description]
 */
	public function ajax_unset_viewed(){
		session('viewed',null);
	}
/**
 * [get_collect 获得该商品的收藏数量]
 * @return [type] [description]
 */
	public function get_collect(){	
		$gid = Q('get.gid','','intval');
		$collect = K('Collect')->get_data(array('gid'=>$gid));
		if($collect){
			$this->assign('collect',$collect);
		}else{
			$this->assign('collect',array());
		}

		// 对数据循环，判断该用户是否评论了该商品
		if(session('uid')){
			if($collect){
				foreach ($collect as $v) {
					if($v['user_uid'] == session('uid')){
						$this->assign('collected',0); //已收藏
					}else{
						$this->assign('collected',1); //可以收藏
					}
				}
			}else{
				$this->assign('collected',2); // 为收藏
			}
		}
	}
/**
 * [get_location 获取商家分店地址]
 * @return [type] [description]
 */
	public function get_location(){
		$gid = Q('get.gid','','intval');
		$goods = K('Goods')->get_one(array('gid'=>$gid));
		// 商家 cid
		$sid = $goods['sid'];
		// 获取商家分店地址
		$location = K('SellerLocation')->get_seller_location($sid);
		$this->assign('location',$location);
	}
/**
 * [get_comments 获得商品评价]
 * @return [type] [description]
 */
	public function get_comments(){
		$gid = Q('get.gid','','intval');
		$Comments = K('Comments')->get_data(array('gid'=>$gid));
		// 获得评论的用户信息
		if(!is_null($Comments)){
			foreach ($Comments as $k => $v) {
				$uid = $v['uid'];
				$data = K('User')->get_one(array('uid'=>$uid));
				$Comments[$k]['username'] = $data['username'];
				$Comments[$k]['nick'] = $data['nick'];
			}
		}

		$this->assign('Comments',$Comments);

	}
/**
 * [location 获得当前位置]
 * @return [type] [description]
 */
	public function location(){
		$gid = Q('get.gid',0,'intval');
		$goods = K('Goods')->get_one(array('gid'=>$gid));
		$cid = $goods['cid'];

		$positoinArr = $this->get_cate($cid);
		$positoinArr = array_reverse($positoinArr,true);
		$this->assign('positoinArr',$positoinArr);
	}

/**
 * [is_parent 得到父级到子级的数组]
 * @param  [type]  $cid [description]
 * @return boolean      [description]
 */
    private function get_cate($cid){
        static $tempArr = array();

        $category = K('category')->get_one($cid);
        
        $tempArr[$category['cid']] = $category['cname'];
        if($category['pid'] != 0){
            $this->get_cate($category['pid']);
        }
        return $tempArr;
    }

}



