<?php 
class CommentsControl extends CommonControl{
	private $model;

	public function __init(){
		$this->model = K('Comments');
	}

/**
 * [add 添加评论]
 */
	public function add_data(){
		$_POST['sendtime'] = time();
		$_POST['uid'] = session('uid');
		// 向评论表添加数据
		$this->model->add_data();
		// 改变订单的是否已评价字段
		K('Order')->edit($_POST['order_id'],array('is_comments'=>1));
		// 向对应的商品表中增加评论数
		$gid = Q('post.gid');
		$goods = K('Goods')->get_data(array('gid'=>$gid),'comment_num');
		$comment_num = $goods[0]['comment_num']+1;
		K('Goods')->edit(array('gid'=> $gid),array('comment_num'=>$comment_num));
		$this->success('评价成功',U('Member/order'));
	}
}