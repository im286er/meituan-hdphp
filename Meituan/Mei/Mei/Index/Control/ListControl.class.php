<?php 
class ListControl extends CommonControl{
	public function index(){
		/* 执行公共控制器 CommonControl 类里的公共方法，获得所有分类数据*/
      	$this->all_category();

      	/*获得所有该顶级分类下的子分类*/
      	$this->get_sonCate();

      	/*获得该顶级分类下筛选条件*/
      	$this->get_attr();

        // 执行执行公共控制器 CommonControl 类里的公共方法，获得用户的购物车商品
        $this->cart();

        /**/
      	/*获得符合筛选条件的商品*/
      	$this->get_goods();


		$this->display();
	}

/**
 * [get_sonCate 获得子分类，通过分类筛选]
 * @return [type] [description]
 */
	public function get_sonCate(){
		// 获得所点击的分类的 cid
		$cid = Q('cid','','intval');
		// 获得该分类的信息
      	$data = K('Category')->get_one($cid);
      	// 通过 pid 来判断是否是顶级分类 ，如果 pid 不为零 ，则该 pid 就是顶级分类的 cid
      	if($data['pid']){
      		$cid = $data['pid']; //顶级分类 cid
      		$sonCate = K('Category')->get_all(array('pid'=>$data['pid']));
      	}else{ //如果是顶级分类
      		$sonCate = K('Category')->get_all(array('pid'=>$data['cid']));
      	}
        // 查询各分类下商品的数量
        foreach ($sonCate as $k => $v) {
            $goodsArr = K('Goods')->get_data(array('cid'=>$v['cid']));
            $sonCate[$k]['count'] = count($goodsArr);
        }

      	// 分配该分类的顶级分类
      	$this->assign('cid',$cid);
        // 获得顶级分类名称
        $Category = K('Category')->get_one($cid); 
        $this->assign('cname',$Category['cname']);
      	// 分配该顶级分类下的所有子分类
      	$this->assign('sonCate',$sonCate);
	}

	public function get_attr(){
		// 获得所点击的分类的 cid
		$cid = Q('cid','','intval');
		// 获得该分类的信息
      	$data = K('Category')->get_one($cid);

		$pid = $data['pid'];
		if($pid){
			$attr = K('GoodsAttrView')->get_type_attr($pid);
		}else{
			$attr = K('GoodsAttrView')->get_type_attr($cid);
		}
		
		// 组合数组
		$tempArr = array();
		if($attr){
			foreach ($attr as $v) {
				$tempArr[$v['kindid']]['attrname'] = $v['attrname'];
				$tempArr[$v['kindid']]['value'][$v['atid']] = $v['attrvalue'];	
			}
		}
		$tempArr2 = array();
    foreach ($tempArr as $v) {
      $tempArr2[] = $v;
    }
    // 计算每个属性下有有多少商品
    foreach ($tempArr2 as $k => $v) {
        foreach ($v['value'] as $key => $value) {
            $gidArr = K('GoodsAttr')->get_data_byatid($key);
            $tempArr2[$k]['value'][$key]=array('value'=>$value,'count'=>count($gidArr));
        }
    }

		$this->assign('attrArr',$tempArr2);

    // 通过填充数组的方法，使点击筛选条件时能让 url 自动组合参数
    $num = count($tempArr2); // 获取属性种类数
    $filter = isset($_GET['attr']) ? explode('_',$_GET['attr']) : array_fill(0, $num, 0);

    $this->assign('filter',$filter);

	}
	public function get_goods(){
     
		    $cid = isset($_GET['cid']) ? Q('get.cid','','intval') : 0;
        // 判断是否是顶级分类，(如果是顶级分类还得获取其子集分类来获取商品)
        $pidArr = K('Category') -> get_fields(array('cid'=>$cid),'pid'); //获取顶级pid
        $pidArr = current($pidArr); // 转为一维数组
        $pid = $pidArr['pid']; //获取pid 值
          // 判断 pid 是否等于 0 ，如果等于 0 ,开始查询子分类 cid 
		    if($pid == 0){
          //  获取所有子分类cid 数组
            $cidArr = K('Category')->get_fields(array('pid'=>$cid),'cid');
            // 如果没有子分类，商品为空
            if(!$cidArr) return  $goodsArr = null;
             // 2. 将 $cidArr 组合成方便操作的一维数据
            $tempArr = array();
            if(is_array($cidArr)){
                foreach ($cidArr as $v) {
                    $tempArr[] = $v['cid'];
                }
            }
            // 获得符合条件的商品
            $goodsArr = K('Goods')->get_data2($tempArr);

        // 如果是子分类，直接以此 cid 来获取该分类下的商品
        }else{
            $goodsArr = K('Goods')->get_data(array('cid'=>$cid));
        }
       // 取出商品的 gid
       $gidArr = array();
       if(is_array($goodsArr)){
            foreach ($goodsArr as $k => $v) {
               $gidArr[] = $v['gid'];
            }
       }


       /*以上是按分类筛选,以下按属性筛选出商品的 gid*/
       // 获取属性值 0_0_0
       $attr = isset($_GET['attr']) ? Q('get.attr') : 0;
       // 拆分成数组
       $attrArr = explode('_', $attr);
       // 获取具有这些参数的商品 gid
       // $gid_attr_Arr = array();
       foreach ($attrArr as $v) {
            if($v == 0) continue; // 如果属性值为0 ，代表全部不查询数据库
            $data = K('GoodsAttr')->get_data_byatid($v);
            
            // 如果查询出有数据，重组数组，变为一维数组，并与分类下的商品 gid 取交集
            if(is_array($data)){
                 $tempArr = array();
                 foreach ($data as $v) {
                      $tempArr[] = $v['goods_gid'];
                 }

                 $gidArr = array_intersect($gidArr, $tempArr);

            }else{ // 如果数据为空，则没哟符合条件的商品，将data 赋为空数组，取交集，为空
                $gidArr = array_intersect($gidArr, array());
            } 
       } 
         // 定义order
        $order = '';
        // 默认排序是 asc
        $pai = 'asc';
         // 获取排序条件
        $sort = isset($_GET['sort']) ? $_GET['sort'] : '';
          // 将sort 拆分 (例如：time-desc用‘-’拆分)
          
          if($sort){
              $pos = strpos($sort, '-');
              $pai = substr($sort, $pos+1);
              $sort = substr($sort, 0,$pos);
              $this->assign('sort',$sort);
              if(is_null($sort) || empty($sort) || $sort == 'default') $order = '';
              if($sort == 'sell') $order = "sell_num  $pai";
              if($sort == 'price')  $order = "group_price $pai";
              if($sort == 'time')  $order = "sendtime $pai";
              if($pai == 'asc'){
                 $pai = 'desc';
              }else{
                  $pai = 'asc';
              }
              
          }
         $this->assign('pai',$pai);

        if($gidArr){
            $goodsArr = K('Goods')->get_data(array('gid'=>$gidArr) ,'',$order);
            // 取出第一张图片路径，作为缩略图
           if(is_array($goodsArr)){
                foreach ($goodsArr as $k => $v) {
                    $pos = strpos($v['pic'], ';');
                    $firstPicPath = substr($v['pic'], 0,$pos);
                    $goodsArr[$k]['pic'] = $firstPicPath;
                }
           }

            $this->assign('goodsArr',$goodsArr);
       }else{
            $this->assign('goodsArr',array());
       }
        
       
      
	}
}


