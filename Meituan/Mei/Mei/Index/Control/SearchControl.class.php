<?php 
class SearchControl extends CommonControl{

    public function __init(){
        /* 执行公共控制器 CommonControl 类里的公共方法，获得所有分类数据*/
        $this->all_category();

        /*获得所有该顶级分类下的子分类*/
        $this->get_sonCate();
         // 执行执行公共控制器 CommonControl 类里的公共方法，获得用户的购物车商品
        $this->cart();
    }
	public function index(){
		  // 获得关键词
        $word = Q('post.search');
        $this->assign('word',$word);
        // 中文分词
        $word = String::splitWord($word);
        
        // 对分词后的数组进行循环查询数据库
        $i = 0;
        $gidArr = array();
        foreach ($word as $k => $v) {
            $data = K('Goods')->get_data_sch("title like '%$k%'");
            
            if(is_array($data)){
               
                if($i == 0){
                    // 定义空数组，用来记录商品 gid
                    
                     foreach ($data as $v) {
                       $gidArr[] = $v['gid'];
                    }
                }else{
                    $tempArr = array();
                    foreach ($data as $value) {
                        $tempArr[] = $value['gid'];
                    }
                    $gidArr = array_intersect($gidArr, $tempArr);
                }
                
            }
            $i++ ;
        }
        // 如果有商品 gid ,就获取商品信息
        if($gidArr){
             $goodsArr = K('Goods')->get_data_sch(array('gid'=>$gidArr));
             
            // 取出第一张图片路径，作为缩略图
           if(is_array($goodsArr)){
                foreach ($goodsArr as $k => $v) {
                    $pos = strpos($v['pic'], ';');
                    $firstPicPath = substr($v['pic'], 0,$pos);
                    $goodsArr[$k]['pic'] = $firstPicPath;
                }
           }
           $this->assign('goodsArr',$goodsArr);
        }
        $this->display();
	}

/**
 * [search 商品搜索]
 * @return [type] [description]
 */
    public function ajax_get_keywords(){
        // 获得关键词
        $word = Q('post.word');
        // 中文分词
        $word = String::splitWord($word);
       	if(empty($word)) die();
        
        // 对分词后的数组进行循环查询数据库
        $i = 0;
        $gidArr = array();
        foreach ($word as $k => $v) {
            $data = K('Goods')->get_data_sch("title like '%$k%'");
            
            if(is_array($data)){
               
                if($i == 0){
                    // 定义空数组，用来记录商品 gid
                    
                     foreach ($data as $v) {
                       $gidArr[] = $v['gid'];
                    }
                }else{
                    $tempArr = array();
                    foreach ($data as $value) {
                        $tempArr[] = $value['gid'];
                    }
                    $gidArr = array_intersect($gidArr, $tempArr);
                }
                
            }
            $i++ ;
        }
        // 如果有商品 gid ,就获取商品信息
        if($gidArr){
             $goodsInfo = K('Goods')->get_data_sch(array('gid'=>$gidArr));
             $goods_title = array();
             foreach ($goodsInfo as $v) {
                 $goods_title[] = $v['title'];
             }
             echo json_encode($goods_title);
             die();
        }
       
        echo 0;
        die();
    }
}

