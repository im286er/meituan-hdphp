<?php 
/**
 * 公共类
 */
class CommonControl extends Control{
/**
 * [__empty description]
 * @return [type] [description]
 */
     public function __empty(){
          $this->error('您访问的页面不存在',__ROOT__);
      }
/**
 * [all_category 所有左侧分类]
 * @return [type] [description]
 */
	function all_category(){
		// 从数据获得分类数据
		$allCate = K('Category')->get_all();
		// 组合数组、方便调数据
    	$tempArr = array();
    	if(is_array($allCate)){
    		foreach ($allCate as $v) {
    			if($v['pid'] == 0){
    				$tempArr[$v['cid']]['parent'] = $v['cname'];
    			}else{
    				$tempArr[$v['pid']]['son'][$v['cid']] = $v['cname'];
    			}

    		}
    	}
        // p($tempArr);
    	// 分配分类数据
    	$this->assign('allCate',$tempArr);
	}
/**
 * [cart 公共方法，获得用户的购物车里的商品信息]
 * @return [type] [description]
 */
    public function cart(){
        $uid = session('uid');
        if($uid){
             $cart = K('Cart')->get_use_cart($uid);

            // 循环购物车数据，重组数组
            if(is_array($cart)){
                foreach ($cart as $k => $v) {
                    $data = K('Goods')->get_one($v['gid']);
                    // 获得图片，把字符串格式转为数组，方便调用
                    $pic = $data['pic'];
                    $picArr = explode(';', rtrim($pic,';'));
                    $data['pic'] = $picArr;
                    $cart[$k] = array(
                        'gid'           => $v['gid'],
                        'pic'           => $data['pic'][0],
                        'title'         => $data['title'],
                        'group_price'   => $data['group_price'],
                        'num'           => $v['goods_num']
                        );

                }
            }
        }else{
            $cart = session('cart');
        }
       
        $this->assign('cart',$cart);
        return $cart;
    }
/**
 * [del_viewed 清除浏览记录]
 * @return [type] [description]
 */
    public function del_viewed(){
       session('viewed',null);
       echo 1;
       die();
    }



}

