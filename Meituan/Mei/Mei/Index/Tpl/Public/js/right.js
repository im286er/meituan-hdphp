$(function(){
	// 当窗口页面滚动时，让浏览记录块到达顶部时固定
	var recently_viewed = $('#view').offset().top;
	$(window).scroll(function(){
		if($(window).scrollTop() > recently_viewed){
			$('.recently-viewed').addClass('viewed-fixed');
		}else{
			$('.recently-viewed').removeClass('viewed-fixed');
		}
	})
})