$(function(){
	
	

/*首页本周特选特效 开始*/
	// 鼠标放到上面显示左右箭头
	$('.all-box').hover(function(){
		clearTimeout(auto_bian)
		$(this).find('.btn a').fadeTo('fast','0.6');
	},function(){
		auto_bian = setInterval(function(){
			i++
			if(i == 5) i=0
			jingxuan();
		},2000)
		$(this).find('.btn a').fadeTo('fast','0');
	})
	// 鼠标放到左右箭头上加深颜色
	$('.all-box .btn a').hover(function(){
		$(this).fadeTo('fast','1')
	},function(){
		$(this).fadeTo('fast','0.6')
	})
	// 点击右箭头变化
	var i=0;
	$('.all-box .r-btn').click(function(){
		i++
		if(i == 5) i=0
		jingxuan();
	})
	// 点击左箭头
	$('.all-box .l-btn').click(function(){
		i--
		if(i == -1) i=4
		jingxuan()
	})
	// 点击圆点
	$('.btn-list li').click(function(){
		clearTimeout(auto_bian)
		i = $(this).index();
		jingxuan();
	})
	var auto_bian = setInterval(function(){
		i++
		if(i == 5) i=0
		jingxuan();
	},2000)

	// 图片转换公共函数
	function jingxuan(){
		// 圆点按钮改变
		$('.btn-list li').eq(i).addClass('hover').siblings('li').removeClass('hover')
		// 图片转换
		var this_li = $('.all-box').find('.list li').eq(i);
		this_li.css('z-index',1).fadeTo('fast',1).siblings('li').css('z-index',0).fadeTo('fast',0);
	}
/*首页本周特选特效 结束*/

/*首页产品鼠标放移入效果开始*/
	$('.list-box').hover(function(){
		//下边框变颜色
		$(this).css('border-bottom-color','#2bb8aa') 
		$(this).find('.bot').css('border-bottom-color','#2bb8aa') 
		//商圈显示
		$(this).find('.sq').stop().fadeTo('','0.9') 
		// 字体颜色变化
		$(this).find('.des').css('color','#333')
	},function(){
		//下边框变颜色
		$(this).css('border-bottom-color','#ddd') 
		$(this).find('.bot').css('border-bottom-color','#ddd') 
		//商圈隐藏
		$(this).find('.sq').stop().fadeTo('','0') 
		// 字体颜色变化
		$(this).find('.des').css('color','#999')
	})

	/*左侧悬浮按钮*/
	// 获得各楼层距离页面顶部的高度
	var food = $('.food').offset().top;
	var yule = $('.yule').offset().top;
	var movie = $('.movie').offset().top;
	var hotel = $('.hotel').offset().top;
	var life = $('.life').offset().top;
	var shop = $('.shop').offset().top;
	var beauty = $('.beauty').offset().top;
	var travel = $('.travel').offset().top;
	var arr = new Array('food','yule','movie','hotel','life','shop','beauty','travel');

	// 设置左边悬浮div的位置
	$('.left-side').css('top',food+55);

	$(window).scroll(function(){
		var scrollTop = $(window).scrollTop();
		// 让左边div 固定
		if(scrollTop > food + 55){
			$('.left-side').css({'position':'fixed','top':0})
		}else{
			$('.left-side').css({'position':'position','top':food+55})
		}
		// 对 arr 数组循环，当滚动到该楼层，让对应的左侧按钮选中
		for (y in arr){
			if(scrollTop >= $('.'+arr[y]).offset().top){
				for(x = 0; x < $('.left-side li').length; x++){
					$('.left-side li').removeClass('current'+x)
				}
				$('li[mark='+arr[y]+']').addClass('current'+y);

			}
		}
		
		
	})
	// 点击到达相应楼层
		$('.left-side li').click(function(){
			// 改变背景色
			var index = $(this).index();

			$('.left-side li').each(function(i){
				$(this).removeClass('current'+i)
			})
			$(this).addClass('current'+index)
			// 到达相应楼层
			var mark = $(this).attr('mark');
			height = $('.'+mark).offset().top;
			$('body,html').animate({'scrollTop':height},500)
		})
// 返回头部
	$('.gotop').click(function(){
		$('body,html').animate({'scrollTop':0},500)
	})

/*首页产品鼠标放移入效果结束*/

})