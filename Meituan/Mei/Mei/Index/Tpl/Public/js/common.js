$(function(){
	/*头部异步删除购物车*/
	$('.top-right-li').on('click','.top_del_cart',function(){
		var _this = $(this);
		var gid = $(this).attr('gid'); 

		$.ajax({
			'url' : APP + '/Cart/ajax_top_del_cart',
			'type' : 'post',
			'data' : {'gid':gid},
			'success':function(data){
				if(data == 1){

					_this.parents('.top_cartlist').remove();
					var num = $('.gwche-num').html();
					num--
					$('.gwche-num').html(num)
					if(num == 0){
						$('#cart').html('<p class="wu">暂没有加入购物车的商品</p>')
					}
				}
			}
		})
	})

	/*清除浏览记录*/
	$('.del_viewed').click(function(){
		$.ajax({
			'url' : APP + "/Common/del_viewed",
			'type':'post',
			'success':function(data){
				if(data == 1){
					var p = '<p class="wu">暂无浏览记录</p>';
					$('.viewed-pos').html(p)
					
				}
			}
		})
	})

	/*当搜索商品的时候，异步获取相关商品*/
	$('.search').bind('keyup focus',function(){
		var word = $(this).val();
		$.ajax({
			'url':APP + '/Search/ajax_get_keywords',
			'type':'post',
			'dataType':'json',
			'data':{'word':word},
			'success': function(data){
				var str = '<div class="sch_result index_sch_result"><ul>';
		     	$.each(data,function(i,n){
		     		if(i>9) return false;
		     		str +=  '<li>' + n + '</li>';

		     	})
		     	str += '</ul></div>';
		     	$('.sch_result').remove();
		     	$('.search').after(str);

		   }
		   
		})
		if(word == '') $('.sch_result').remove();

	})
	// 鼠标划上匹配出来的下拉商品，背景色改变
  $('.sch-piece').on('mouseover','.sch_result li',function(){
  		$(this).siblings('li').css('background','#FFF');
  		$(this).css('background','#f0f0f0');
  })
  // 单点击每个商品名，自动输进搜索框
	$('.sch-piece').on('click','.sch_result li',function(){
		$('.search').val($(this).text());
		$('.sch_result').remove();
		$('form[name=sch_form]').submit();
		
  })
	$('body').click(function(){
		$('.sch_result').remove();
	})
	// 搜索提交验证，关键词不能为空
	$('form[name=sch_form]').submit(function(){
		if($('.search').val() == '') return false;
	})

	
	


	/*首页产品鼠标放移入效果开始*/
	$('.list-box').hover(function(){
		//下边框变颜色
		$(this).css('border-bottom-color','#2bb8aa') 
		$(this).find('.bot').css('border-bottom-color','#2bb8aa') 
		//商圈显示
		$(this).find('.sq').stop().fadeTo('','0.9') 
		// 字体颜色变化
		$(this).find('.des').css('color','#333')
	},function(){
		//下边框变颜色
		$(this).css('border-bottom-color','#ddd') 
		$(this).find('.bot').css('border-bottom-color','#ddd') 
		//商圈隐藏
		$(this).find('.sq').stop().fadeTo('','0') 
		// 字体颜色变化
		$(this).find('.des').css('color','#999')
	})
})