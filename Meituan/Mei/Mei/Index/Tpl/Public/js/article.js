$(function(){
	/*产品图片切换*/
	// 默认第一张图片显示
	$('.big-pic li:first').css('display','block');
	$('.sma-pic li:first').addClass('hover');
	$('.sma-pic li').mouseover(function(){
		$(this).addClass('hover').siblings('li').removeClass('hover');
		$('.big-pic li').eq($(this).index()).show().siblings('li').hide();
	})

	/*加减购买数量*/
	
	$('.subtract').click(function(){
		var num = $('.num-text').val();
		if(num == 1){
			$('.num-box .tishi').show();
			setTimeout(function(){$('.num-box .tishi').hide()},3000)
		}else{
			num--
			$('.num-text').val(num)
		}
	})

	$('.plus').click(function(){
		$('.num-box .tishi').hide();
		var num = $('.num-text').val();
		num++
		$('.num-text').val(num)
	})


	/*异步加入购物车*/
	$('.to-che').click(function(){
		// 获得商品 gid 和 要加入购物车的数量
		var gid = $('input[name=gid]').val();
		var num = $('input[name=num]').val();
		$.ajax({
			'url' 	: APP + '/Cart/go_cart',
			'dataType':'html',
			'type'	:'post',
			'data' 	: {'gid':gid,'num':num},
			'success':function(data){
				if(data){
					if(data == 1){
						alert('加入购物车成功！');
					}else{
						$('#cart').html(data);
						var count = $('.gwche-num').html(); //加入购物车的件数
						count++
						$('.gwche-num').html(count);
						alert('加入购物车成功！');
					}
					
				}else{
					alert('加入购物车失败！')
				}
			}
		})
	})

	// 收藏
	$('.sc').click(function(){
		var gid = $('input[name=gid]').val();

		$.ajax({
			'url': APP + '/Collect/collect',
			'type':'post',
			'data':{'gid':gid},
			'success':function(data){
				if(data == 0){
					alert('请先登陆');
					// 如果没登陆，跳转至登陆页面
					location.href=APP + '/Login/index';
				}
				if(data == 1){
					alert('收藏成功！');
					// 获得原来的收藏数量
					var num = $('.collect').find('strong').html();
					num++
					var str = '<i></i>收藏(<strong>'+num+'</strong>)';
					$('.collect').html(str);
				}
			}
		})
	})

	// 鼠标移入商家分店，显示隐藏内容
	$('.list').hover(function(){
		$('.list .info').stop().slideUp();
		$(this).find('.info').stop().slideDown();
	})
	// 当页面页面滚动时, 对象定在屏幕顶部
	// 获得对象距离页面顶部的高度
	var pro_menu = $('.pro-menu').offset().top;
	var xuzhi = $('#xuzhi').offset().top;
	var content = $('#content').offset().top;
	var sellerinfo = $('#sellerinfo').offset().top;
	var comments = $('#comments').offset().top;

	$(window).scroll(function(){

		if($(this).scrollTop() > pro_menu){
			$('.pro-menu').addClass('fixed');
			$('.tidai').show();
		}else{
			$('.pro-menu').removeClass('fixed');
			$('.tidai').hide();
		}

		if($(this).scrollTop() < (xuzhi)){
			$('.pro-menu').find('a').removeClass('current');
			$('.title_seller').addClass('current');
		}

		if($(this).scrollTop() > (xuzhi - 60)){
			$('.pro-menu').find('a').removeClass('current');
			$('.title_zuzhi').addClass('current');
		}

		if($(this).scrollTop() > (content - 60)){
			$('.pro-menu').find('a').removeClass('current');
			$('.title_content').addClass('current');
		}

		if($(this).scrollTop() > (sellerinfo - 60)){
			$('.pro-menu').find('a').removeClass('current');
			$('.title_sellerinfo').addClass('current');
		}

		if($(this).scrollTop() > (comments - 60)){
			$('.pro-menu').find('a').removeClass('current');
			$('.title_comments').addClass('current');
		}
	})

})