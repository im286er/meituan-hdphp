$(function(){

	// 验证用户注册表单项
	$('form[name=user_regist_form]').validate({
		'email' : {
			'rule':{
				'required':true,
				'email' :true,
				'ajax':{
					'url':APP + '/Regist/ajax_check_email',
					'field':['email']
				}
			},
			'error':{
				'required':'请填写邮箱',
				'email':'邮箱格式不正确',
				'ajax':'邮箱已存在'
			},
			'message':'推荐qq邮箱',
			'success':'邮箱可以注册'
		},
		'username':{
			'rule':{
				'required':true,
				'maxlen':16,
				'minlen':4,
				'ajax':{
					'url':APP + '/Regist/ajax_check_username',
					'field':['username'],
				},
				'user':true,

			},
			'error':{
				'required':'请填写用户名',
				'maxlen':'用户名不能超过16个字符',
				'minlen':'用户名不能低于4个字符',
				'ajax':'用户名已存在',
				'user':'用户名不正确',
			},
			'message':'4-16字符，不能以数字开头',
			'success':'用户名可以注册'
		},
		'password':{
			'rule':{
				'required':true,
				'maxlen':32,
				'minlen':6,
			},
			'error':{
				'required':'请填写密码',
				'maxlen':'密码长度不能超过32个字符',
				'minlen':'密码长度不能低于6个字符',
			},
			'message':'6-32字符，可使用字母、数字及符号的任意组合',
			'success':'填写正确'

		},
		'confirm_pwd':{
			'rule':{
				'confirm':'password'
			},
			'error':{
				'confirm':'密码不一致'
			},
			'success':'密码正确'

		},
		'code':{
			'rule':{
				'required':true,
				'ajax':{
					'url':APP + '/Regist/ajax_check_code',
					'field':['code']
				},
			},
			'error':{
				'required':'验证码不能为空',
				'ajax':'验证码不正确'
			},
			'success':'验证码正确'
		}


	});


})