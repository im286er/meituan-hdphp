
$(function(){

	// 初始计算每个项目的小计及总计(默认所有都是被选中状态)
	var count = 0;
	 $('.listtr').each(function(i){
		var xiaoji = ($(this).find('.group_price').html() * $(this).find('.num-text').val()).toFixed(2);
		$(this).find('.xiaoji span').html(xiaoji);
		count += parseInt(xiaoji);
	 });
	
	 $('.zong').html(count.toFixed(2));
	 // 初始化选种商品件数
	 jian();
// 当点击减号的时候
	$('.subtract').click(function(){
		var num = $(this).parent('.num').find('.num-text').val();
		if(num == 1){
			$('.zhishao').show();
			setTimeout(function(){$('.zhishao').hide()},3000)
		}else{
			num--
			$(this).parent('.num').find('.num-text').val(num);
			// 单项目小计
			total(num,$(this));
		}
	})
// 当点击加号的时候
	$('.plus').click(function(){
		$('.zhishao').hide();
		var num =  $(this).parent('.num').find('.num-text').val();
		num++
		 $(this).parent('.num').find('.num-text').val(num);
		 total(num,$(this));
	})

// 当checkbox改变选中状态时，相应的改变，总价
	$('input[name="gid[]"]').change(function(){

		// 获取总价
		var zong = parseInt($('.zong').html());
		// 获得当前商品的小计
		var xiaoji = parseInt($(this).parents('tr').find('.xiaoji span').html());
		// 判断该复选框点击后是否是取消选中还是，选中
		var checked = $(this).prop('checked');
		if(checked){
			// 如果选中了该商品，计算总价
			zong = zong + xiaoji;
			$('.zong').html(zong.toFixed(2))
			$('input[name="gid[]"]').each(function(i){
				var is_all = true;
				if($(this).prop('checked') == false){
					is_all = false;
				}
				if(is_all){
					$('input[name=all]').prop('checked','checked');
				}
			})
		}else{
			// 如果取消了该商品，计算总价
			zong = zong - xiaoji;
			$('.zong').html(zong.toFixed(2))
			$('input[name=all]').prop('checked','');
		}
		// 计算选中商品件数
		jian();
	})
// 当点击全选。或者取消全选时
	$('input[name=all]').change(function(){
		var checked = $(this).prop('checked');
		if(checked){
			// 如果全选
			$('input[name="gid[]"]').prop('checked','checked')
			// 计算总价格
			all_total();
			// 计算选中商品的数量（每件商品为一件，不记每件商品的数目）
			jian();
		}else{
			// 如果全不选
			$('input[name="gid[]"]').prop('checked','')
			$('.zong').html(0);
			$('.jian').html(0)
		}
	})
	// 当手动改变商品数量，改变相应的值

	$('.num-text').keyup(function(){
		if($(this).val() <= 0 || isNaN($(this).val())){
			$(this).val(1)
		}
		var obj = $(this);
		var num = obj.val();
		if(num != 0 && num != ''){
			total(num,obj);
		}else{
			$('.zhishao').show();
			setTimeout(function(){$('.zhishao').hide()},3000)
		}
		

	})
// 计算总价格，用在当该页面打开时初始化总价格，和全选时，计算总价格
	function all_total(){
		var count = 0;
		 $('.listtr').each(function(i){
			var xiaoji = ($(this).find('.group_price').html() * $(this).find('.num-text').val()).toFixed(2);
			$(this).find('.xiaoji span').html(xiaoji);
			count += parseInt(xiaoji);
		 });

		 $('.zong').html(count.toFixed(2));
	}

// 计算选中商品的数量（每件商品为一件，不记每件商品的数目）
	function jian(){
		var jian = 0;
		$('input[name="gid[]"]').each(function(i){
			if($(this).prop('checked') == true){
				jian ++
			}
		})

		$('.jian').html(jian)
	}

// 小计、总计
	function total(num,obj){
		 var group_price = obj.parents('tr').find('.group_price').html();
		 var xiaoji = (group_price * num).toFixed(2);
		 obj.parents('tr').find('.xiaoji span').html(xiaoji);
		 // 总计,在这里判断该商品是否被选中
		 var checked = obj.parents('tr').find('input[name="gid[]"]').prop('checked');
		 if(checked == true){
		 	 var total = 0;
			 $('.xiaoji span').each(function(i){
				total += parseInt($(this).html());
			 });
			$('.zong').html(total.toFixed(2));
		 }
		
	}	

})