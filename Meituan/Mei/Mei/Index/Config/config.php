<?php
if (!defined("HDPHP_PATH"))exit('No direct script access allowed');
//更多配置请查看hdphp/Config/config.php
$dbconfig = include COMMON_CONFIG_PATH . 'dbconfig.php';
$config = array(
	// 水印
	 'WATER_ON'                      => false,           //开关
	//url重写模式，开启后，通过apache 配置可以隐藏项目入口文件
	 'URL_REWRITE'=>true,
	 // 路由
	 'route'=>array(
	     '/^uid(\d+)$/'=>'Index/List/index/uid/#1'
  	  ),
	 'PATHINFO_HTML'                 => '.html',          //伪静态扩展名
);

return array_merge($dbconfig , $config);


?>