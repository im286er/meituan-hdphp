<?php 
class AdminModel extends Model{
	public $table = 'Admin'; //操作 admin 表
	public $validate = array(
		array('username','nonull','用户名不能为空',2,3),
		array('password','nonull','密码不能为空',2,3),
		);
	/**
	 * [adminLogin 后台登陆]
	 * @return [type] [description]
	 */
	public function adminLogin(){
		// 验证码是否为空
			$code = Q('post.code','','trim,strtolower');
			if(!$code){
				$this->error ='验证码不能为空';
				return false;
			}
			// 验证码是否正确
			if($code != strtolower(session('code'))){
				$this->error = '验证码不正确';
				return false;
			}
			// 自动验证
			if(!$this->create()){
				return false;
			}
			// 获得表单提交过来的用户名和密码
		$username = Q('username');
		$password = md5(Q('password'));
		// 查询数据库
		$result = $this->where(array('aname'=>$username,'password'=>$password))->find();

		if($result){
			session('aname',$username); // 将登陆用户名写入session
			// 更新登录时间
			$logintime = time();
			$this->where(array('aname'=>$username))->save(array('logintime'=>$logintime));
			return $result;
		}
		return false;
	}

}


 ?>