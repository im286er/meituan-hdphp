<?php 
class LoginControl extends Control{
	private $model;

	public function __init(){
		$this->model=K('Admin');
	}

	public function index(){
		if(session('aname')) go(U('Index/index'));
		$this->display();
	}

	/**
	 * [login 登陆后台]
	 * @return [type] [description]
	 */
	public function login(){
		if(IS_POST){
			$admininfo = $this->model->adminLogin();
			if($admininfo){
				$this->success('登陆成功！',U('Index/index'));
			}
			$this->error($this->model->error);
		}
	}
	/**
	 * [ajax_check_code 验证码是否正确]
	 * @return [type] [description]
	 */
	public function ajax_check_code(){
		// 接收ajax传输过来的code值
		$code = Q('post.code','','trim,strtolower');
		if($code == strtolower(session('code'))){
			echo 1;
			die();
		}else{
			echo 0;
			die();
		}
	}
	public function quit(){
		session('aname',null);
		$this->success('退出成功',U('Login/index'));
	}
	/**
	 * [code 登陆验证码]
	 * @return [type] [description]
	 */
	public function code(){
		$obj = new Code();
		$obj->show();
	}

}



 ?>