<?php 
class GoodsControl extends CommonControl{
	private  $model;

	public function __init(){
		$this->model = K('Goods');
	}
/**
 * [index 商家列表]
 * @return [type] [description]
 */
	public function index(){
		$allGoods = K('GoodsView')->get_field();
		$pagelist = $allGoods['pagelist'];
		array_pop($allGoods);
		$this->assign('pagelist',$pagelist);
		$this->assign('allGoods',$allGoods);
		$this->display();
	}	
/**
 * [add 添加商家第一步]
 */
	public function add_1(){
		// 获得所有的分类
		$allCate = K('Category')->get_all();
		$allCate = Data::tree($allCate,'cname');
		$this->assign('allCate',$allCate);
		// 获得商家
		$allSeller = K('Seller')->get_data();
		$this->assign('allSeller',$allSeller);

		$this->display();
	}
/**
 * [add_2 添加商品第二步【添加商品参数】]
 */
	public function add_2(){
		$cid = Q('post.cid','','intval');
		$sid = Q('post.sid','','intval');
		$gid = Q('post.gid','','intval');

		// 将第一步的值分配到模板中
		$this->assign('step1',array('cid'=>$cid,'sid'=>$sid,'gid'=>$gid));
		// 通过 pid 获得属性及属性值
		$cidData = K('Category')->get_one($cid);
		$pid = $cidData['pid'];
		if($pid){
			$attr = K('GoodsAttrView')->get_type_attr($pid);
		}else{
			$attr = K('GoodsAttrView')->get_type_attr($cid);
		}
		
		// 组合数组
		$tempArr = array();
		if($attr){
			foreach ($attr as $v) {
				$tempArr[$v['kindid']]['attrname'] = $v['attrname'];
				$tempArr[$v['kindid']]['value'][$v['atid']] = $v['attrvalue'];	
			}
		}
		$this->assign('attrArr',$tempArr);

		$this->display();
	}
/**
 * [add2_save 将商品数据写入数据库]
 * @return [type] [description]
 */
	public function add2_save(){
		$this->model->add_data();
		$this->success('添加成功！',U('index'));
	}
/**
 * [edit_1 修改商品分类及所属商家]
 * @return [type] [description]
 */
	public function edit_1(){
		// 获得修改商品 gid
		$gid = Q('gid','','intval');
		// 获得该商品信息
		$data = $this->model->get_one($gid);
		$this->assign('data',$data);

		// 获得所有的分类
		$allCate = K('Category')->get_all();
		$allCate = Data::tree($allCate,'cname');
		$this->assign('allCate',$allCate);

		// 获得商家
		$allSeller = K('Seller')->get_data();
		$this->assign('allSeller',$allSeller);

		$this->display();
	}
/**
 * [edit_2 修改商品信息]
 * @return [type] [description]
 */
	public function edit_2(){
		$cid = Q('post.cid','','intval');
		$sid = Q('post.sid','','intval');
		$gid = Q('post.gid','','intval');

		// 将第一步的值分配到模板中
		$this->assign('step1',array('cid'=>$cid,'sid'=>$sid));
		// 通过判断 pid 获得属性及属性值
		$cidData = K('Category')->get_one($cid);
		$pid = $cidData['pid'];
		if($pid){
			$attr = K('GoodsAttrView')->get_type_attr($pid);
		}else{
			$attr = K('GoodsAttrView')->get_type_attr($cid);
		}
		
		// 组合数组
		$tempArr = array();
		if($attr){
			foreach ($attr as $v) {
				$tempArr[$v['kindid']]['attrname'] = $v['attrname'];
				$tempArr[$v['kindid']]['value'][$v['atid']] = $v['attrvalue'];	
			}
		}
		$this->assign('attrArr',$tempArr);

		// 获得原数据
		$data = K('GoodsView')->get_one($gid);

		// 获得服务承诺
		$promise = $data['promise'];
		$promise = explode(',', $promise);
		$data['promise'] = $promise;
		
		// 获得商品图片
		$pic = $data['pic'];
		$picArr = explode(';', rtrim($pic,';'));
		$data['pic'] = $picArr;

		// 获得该商品的属性
		$goodsAttr = K('GoodsAttr')->get_data($gid);
		if(is_array($goodsAttr)){
			$tempArr2 = array();
			foreach ($goodsAttr as $v) {
			 	$tempArr2[] = $v['attr_atid'];
			 } 
			$data['goodsAttr'] = $tempArr2;
		}
		$this->assign('data',$data);
		$this->display();
	}
/**
 * [edit2_save 处理修改信息]
 * @return [type] [description]
 */
	public function edit2_save(){
		$this->model->edit_data();
		$this->success('修改成功！',U('index'));
	}

	public function del_data(){
		$gid = Q('get.gid','','intval');
		$this->model->del_data($gid);
		$this->success('删除成功');
	}
}

