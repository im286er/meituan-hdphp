<?php 
class TypeControl extends CommonControl{
	private $model;

	public function __init(){
		$this->model = K('Type');
	}
	/**
	 * [index 分类列表]
	 * @return [type] [description]
	 */
	public function index(){
		// 获得所有类型并分页样式
		$allType = $this->model->get_all_type(20);
		// 取出分页样式
		$pagelist = $allType['pagelist'];
		// 剔除分页
		array_pop($allType);
		// 分配
		$this->assign('allType',$allType);
		$this->assign('pagelist',$pagelist);

		$this->display();
	}
	/**
	 * [index 添加分类]
	 * @return [type] [description]
	 */
	public function add(){
		if(IS_POST){
			// 向类型表插入数据并获得该数据id
			$tid = $this->model->add_type();
			// 实例化类型、属性中间表模型
			$m = K('TypeAttr');
			// 循环插入中间表数据
			if($_POST['attr']){
				foreach ($_POST['attr'] as $v) {
					$m->add_data(array('type_tid'=>$tid,'attr_kindid'=>$v));
				}
			}
			
			$this->success('添加成功！',U('index'));
		}
		// 获得所有的属性
		$allAttr = K('Attr')->get_all_attr('kindid');
		array_pop($allAttr);
		$this->assign('allAttr',$allAttr);

		$this->display();
	}
	/**
	 * [edit 修改类型]
	 * @return [type] [description]
	 */
	public function edit(){
		
		if(IS_POST){
			$tid = Q('post.tid','','intval');
			// 修改类型表
			$this->model->edit();
			// 修改类型属性中间表
			  // 1.删除该类型的所有属性
			  K('TypeAttr')->dele($tid);
			  // 2. 重新添加修改后的数据
			  foreach ($_POST['attr'] as $v) {
			  	 k('TypeAttr')->add_data(array('type_tid'=>$tid,'attr_kindid'=>$v));
			  }
			 
			$this->success('修改成功！',U('Type/index'));
		}

		$tid = Q('get.tid','','intval');
		// 获得原来数据
		$typeinfo = $this->model->get_one($tid);
		$this->assign('tname',$typeinfo['tname']); //分配类型名称
		// 获得所有的属性
		$allAttr = K('Attr')->get_all_attr('kindid');
		array_pop($allAttr);
		$this->assign('allAttr',$allAttr); //分配所有属性
		// 从类型属性中间表获得该类型下的属性
		$TypeAttr = K('TypeAttr')->get_onetype_attr($tid);
		//  重组数组
		$tempArr = array();
		if($TypeAttr){
			foreach ($TypeAttr as $v) {
				$tempArr[] = $v['attr_kindid'];
			}
		}
		// 分配该类型下的属性
		$this->assign('tempArr',$tempArr);
		
		$this->display();
	}
	/**
	 * [dele 删除数据]
	 * @return [type] [description]
	 */
	public function dele(){
		$tid = Q('get.tid','','intval');
		// 删除 type 表中数据
		$this->model->dele($tid);
		// 删除 type_attr 表中数据
		K('TypeAttr')->dele($tid);
		$this->success('删除成功！');
	}
}


 ?>