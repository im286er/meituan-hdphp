<?php 
class AttrControl extends CommonControl{

	private $model; //定义变量，保存实例化模型
	/**
	 * [__init 自动执行方法]
	 * @return [type] [description]
	 */
	public function __init(){
		$this->model = K('Attr'); //实例化Attr模型
	}

	/**
	 * [list 属性列表]
	 * @return [type] [description]
	 */
	public function index(){
		// 获得所有的属性
		$allAttr = $this->model->get_all_attr('attrname','10');
		// 取出分页
		$pagelist = $allAttr['pagelist'];
		// 剔除分页
		array_pop($allAttr);
		// 分配
		$this->assign('allAttr',$allAttr);
		$this->assign('pagelist',$pagelist);

		$this->display();
	}
	/**
	 * [add 添加属性]
	 */
	public function add(){
		
		if(IS_POST){
			$this->model->add_attr();
			$this->success('添加成功',U('Attr/index'));
		}
		// 获得所有的属性
		$allAttr = $this->model->get_all_attr();
		array_pop($allAttr);
		$this->assign('allAttr',$allAttr);
		// 获得最大的种类id
		$max_kindid= $this->model->get_max_kindid();
		// 让所添加的属性的kindid加 1
		$kindid = $max_kindid + 1;
		$this->assign('kindid',$kindid);
		$this->display();

	}
	/**
	 * [edit 修改属性]
	 * @return [type] [description]
	 */
	public function attr_edit(){
		if(IS_POST){
			$kindid = Q('post.kindid','','intval');
			$attrname = Q('post.attrname');
			$this->model->edit(array('kindid'=>$kindid),array('attrname'=>$attrname));
			$this->success('修改成功！');
		}

		// 载入模板
		$this->display();
	}
	/**
	 * [edit 修改属性值]
	 * @return [type] [description]
	 */
	public function attr_value_edit(){
		if(IS_POST){
			$atid = Q('post.atid','','intval');
			$attrvalue = Q('post.attrvalue');
			$this->model->edit(array('atid'=>$atid),array('attrvalue'=>$attrvalue));
			$this->success('修改成功！');
		}
	}

	/**
	 * [edit 删除属性]
	 * @return [type] [description]
	 */
	public function attr_del(){
		$kindid = Q('get.kindid','','intval');
		$this->model->attr_del(array('kindid'=>$kindid));
		$this->success('删除成功！');
		// 载入模板
	}

	/**
	 * [edit 删除属性值]
	 * @return [type] [description]
	 */
	public function attr_value_del(){
		$atid = Q('get.atid','','intval');
		$this->model->attr_del(array('atid'=>$atid));
		$this->success('删除成功！');

	}
	/**
	 * [attr_value_list 属性值列表]
	 * @return [type] [description]
	 */
	public function attr_value_list(){
		$attrvalue = $this->model->get_attr_value();
		$this->assign('attrvalue',$attrvalue);
		$this->display();
	}
/**
 * [add_value_son 给属性值添加子集，主要用在区域值下面]
 */
	public function add_value_son(){
		if(IS_POST){
			$this->model->add_attr();
			
		}
		$data = $this->model->get_one();
		$this->assign('data',$data);

		$this->display();
	}
/**
 * [value_son_list 查看商圈]
 * @return [type] [description]
 */
	public function value_son_list(){
		$atid = Q('atid','','intval');
		$data = $this->model->get_value_son($atid);
		$this->assign('attrvalue',$data);

		$this->display();
	}

}


 ?>