<?php 
/*分类管理类*/
class CategoryControl extends CommonControl{
	private $model;
	public function __init(){
		$this->model = K('Category');
	}
	/**
	 * [index 分类列表]
	 * @return [type] [description]
	 */
    function index(){
    	// 从关联模型中获得数据
    	$allCate = K('CategoryTypeView')->get_all_data();
    	$allCate= Data::tree($allCate,'cname','cid');
    	$this->assign('allCate',$allCate);
       	$this->display();
    }
	/**
	 * [add 添加分类]
	 */
   function add(){
   		if(IS_POST){
   			$this->model->add();
   			$this->success('添加成功！',U('Category/index'));
   		}
   	   // 获得所有类型
   	   $allType = K('Type')->get_all_type();
   	   array_pop($allType);
   	   $this->assign('allType',$allType);

       $this->display();
    }

    /**
	 * [add_sonCate 添加子分类]
	 */
   function add_sonCate(){
   		if(IS_POST){
   			$this->model->add();
   			$this->success('添加成功！',U('Category/index'));
   		}
   		$cid = Q('get.cid','','intval');
   	   // 获得该父级的数据
   	   $data = K('categoryTypeView')->get_one_data($cid);
   	   $this->assign('data',$data);

       $this->display();
    }

    /**
	 * [edit 修改分类]
	 */
   function edit(){
	   	if(IS_POST){
	   		$this->model->edit();
	   		$this->success('修改成功！',U('Category/index'));
	   	}
   	 // 获得所有类型
   	   $allType = K('Type')->get_all_type();
   	   array_pop($allType);
   	   $this->assign('allType',$allType);

   	   $cid = Q('get.cid','','intval');
   		// 获得原数据
   	   $data = K('categoryTypeView')->get_one_data($cid);
 
   	   $this->assign('data',$data);

       $this->display();
    }

      /**
	 * [dele 删除分类]
	 */
   function dele(){
   	   $cid = Q('get.cid','','intval');
   	   // 判断是否有子分类
   	   $data = $this->model->get_one($cid);
   	   // 有子分类，一块删除子分类
   	   if($data){
   	   	 	$this->model->dele(array('pid'=>$cid));
   	   }
	   $this->model->dele(array('cid'=>$cid));

	   $this->success('删除成功！');
    }

}


 ?>