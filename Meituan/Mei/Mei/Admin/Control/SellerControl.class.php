<?php 
class SellerControl extends CommonControl{
	private $model;

	public function __init(){
		$this->model = K('Seller');
	}
/**
 * [index 商家列表]
 * @return [type] [description]
 */
	public function index(){
		// 获得所有信息
		$allSeller = $this->model->get_all_data();
		// 取出分页
		$pagelist = $allSeller['pagelist'];
		// 剔除分页
		array_pop($allSeller);
		$this->assign('pagelist',$pagelist);
		$this->assign('allSeller',$allSeller);

		$this->display();
	}

/**
 * [add 添加商家]
 * @return [type] [description]
 */
	public function add(){
		if(IS_POST){
			$this->model->add_data();
			$this->success('添加成功！',U('Seller/index'));
		}
		$this->display();
	}
/**
 * [eidt 修改商家]
 * @return [type] [description]
 */
	public function edit(){
		if(IS_POST){
			$this->model->save();
			$this->success('修改成功！',U('Seller/index'));
		}
		$sid = Q('get.sid','','intval');
		// 获得元数据
		$data = $this->model->get_one_data($sid);
		$this->assign('data',$data);

		$this->display();
	}
/**
 * [edit_location 修改商家位置]
 * @return [type] [description]
 */
	public function edit_location(){
		if(IS_POST){
			$sid = Q('post.sid','','intval');
			K('SellerLocation')->save();
			$this->success('修改成功！',U('Seller/location',array('sid'=>$sid)));
		}
		$id = Q('get.id','','intval');
		// 获得元数据
		$data = K('SellerLocation')->get_one_location($id);
		$this->assign('data',$data);
		$this->display();
	}
/**
 * [location 显示商家列表]
 * @return [type] [description]
 */
	public function location(){
		$sid = Q('get.sid','','intval');
		// 获得该商家的所有位置
		$data = K('SellerLocation')->get_seller_location($sid);
		$this->assign('data',$data);

		$this->display();
	}
/**
 * [add_location 添加商家位置]
 */
	public function add_location(){
		if(IS_POST){
			$sid = Q('post.sid','','intval');
			K('SellerLocation')->add_data();
			$this->success('添加成功！',U('Seller/location',array('sid'=>$sid)));
		}
		
		$this->display();
	}
/**
 * [del_seller 删除商家及其位置]
 * @return [type] [description]
 */
	public function del_seller(){
		$sid = Q('get.sid','','intval');
		$this->model->del_data($sid);
		$this->success('删除成功！',U('Seller/index'));
	}
/**
 * [del_location 删除商家位置]
 * @return [type] [description]
 */
	public function del_location(){
		$id = Q('get.id','','intval');
		K('SellerLocation')->del_one_location($id);
		$this->success('删除成功！');
	}

}	


 ?>