<?php 
class UserControl extends CommonControl{
	private $model;

	public function __init(){
		$this->model = K('User');
	}
/**
 * [index 用户列表]
 * @return [type] [description]
 */
	public function index(){
		$data = $this->model->get_all();
		$this->assign('data',$data);
		$this->display();
	}
/**
 * [lock 锁定/解除锁定用户]
 * @return [type] [description]
 */
	public function lock(){
		$uid = Q('get.uid',0,'intval');
		$data = $this->model->get_one(array('uid'=>$uid));
		$is_lock = $data['is_lock'];
		if($is_lock == 0){
			$this->model->edit(array('uid'=>$uid),array('is_lock'=>1));
			$this->success('用户被锁定');
		}else{
			$this->model->edit(array('uid'=>$uid),array('is_lock'=>0));
			$this->success('用户已解除锁定');
		}
	}

	public function del_user(){
		$uid = Q('get.uid',0,'intval');
		$this->model->del_user(array('uid'=>$uid));
		$this->success('删除成功');
	}
}



 ?>
