<?php 
class OrderControl extends CommonControl{
	public $model; //实例化模型变量
	// 自动执行
	public function __init(){
		$this->model = K('Order');
	}

	public function index(){
		$order = K('Order')->get_data();
		if($order){
			foreach ($order as $k => $v) {
				$goods = K('Goods')->get_one(array('gid'=>$v['gid']));
				$order[$k]['title'] = $goods['title'];
			}
		}
		$this->assign('order',$order);
		$this->display();
	}
}
