<?php 
class CartModel extends Model{
	public $table = 'cart'; //该模型所操作的表
/**
 * [get_use_cart 获取用户的购物车商品]
 * @param  [type] $uid [description]
 * @return [type]      [description]
 */
	public function get_use_cart($uid = 0){
		return $this->where(array('uid'=>$uid))->order('sendtime desc')->all();
	}
/**
 * [add_data 向购物车表中添加数据]
 * @param [type] $data [description]
 */
	public function add_data($data = null){
		return $this->add($data);
	}
/**
 * [edit 修改数据]
 * @param  [type] $where [description]
 * @param  [type] $data  [description]
 * @return [type]        [description]
 */
	public function edit($where,$data = null){
		$this->where($where)->save($data);
	}
/**
 * [del_data 删除购物车商品]
 * @param  integer $uid [description]
 * @param  [type]  $gid [description]
 * @return [type]       [description]
 */
	public function del_data($uid = 0, $gid = null){
		$this->where(array('uid'=>$uid,'gid'=>$gid))->delete();
	}

	public function del_data_byuid($uid = 0){
		$this->where(array('uid',$uid))->delete();
	}
}



