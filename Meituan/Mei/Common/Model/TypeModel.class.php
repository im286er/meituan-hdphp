<?php 
/**
 * 类型模型
 */
class TypeModel extends Model{
	public $table = 'Type';
/**
 * [add_type 添加数据]
 */
	public function add_type(){
		return $this->add();
	}
/**
 * [get_all_type 获得所有的类型]
 * @param  [type] $row [每页显示条数]
 * @return [type]      [description]
 */
	public function get_all_type($row = null){
		// 获得所有类型总数
		$total = $this->count();
		// 实例化分页类
		$page = new Page($total,$row,'5');
		// 获得分页
		$pagelist = $page->show();
		$page->show(3);
		// 获得所有类型
		$data = $this->all($page->limit());
		// 将分页样式压入数组一并返回
		$data['pagelist'] = $pagelist;
		return $data;
	}
	/**
	 * [get_one 获得一条数据]
	 * @param  [type] $tid [description]
	 * @return [type]      [description]
	 */
	public function get_one($tid){
		return $this->where(array('tid'=>$tid))->find();
	}

	public function edit(){
		return $this->save();
	}

	/**
	 * [del 删除数据]
	 * @param  [type] $tid [description]
	 * @return [type]      [description]
	 */
	public function dele($tid){
		return $this->where(array('tid'=>$tid))->delete();
	}
}

 ?>