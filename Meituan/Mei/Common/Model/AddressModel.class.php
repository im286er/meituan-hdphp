<?php 
class AddressModel extends Model{
	public $table = 'useraddress';
/**
 * [get_data 获得用户地址]
 * @param  [type] $where [description]
 * @return [type]        [description]
 */
	public function get_data($where = null){
		return $this->where($where)->all();
	}
/**
 * [add_data 增加地址]
 * @param [type] $where [description]
 */
	public function add_data($where = null, $data = null){
		$this->where($where)->add($data);
	}
/**
 * [edit 修改地址]
 * @param  [type] $where [description]
 * @param  [type] $data  [description]
 * @return [type]        [description]
 */
	public function edit($where = null, $data = null){
		$this->where($where)->save($data);
	}
/**
 * [del_address 删除地址]
 * @return [type] [description]
 */
	public function del_address($where = null){
		$this->where($where)->delete();
	}
}