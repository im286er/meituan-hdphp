<?php 
class CollectModel extends Model{
	public $table = 'usercollect';
/**
 * [add_data 添加收藏]
 * @param [type] $where [description]
 * @param [type] $data  [description]
 */
	public function add_data($data = null){
		$this->add($data);
	}
/**
 * [get_data 获得收藏数据]
 * @param  [type] $where [description]
 * @return [type]        [description]
 */
	public function get_data($where = null){
		return $this->where($where)->all();
	}

	public function del_data($where){
		$this->where($where)->delete();
	}


}


