<?php 
class SellerModel extends Model{
	public $table = 'seller';

/**
 * [add 添加数据]
 */
	public function add_data(){
		return $this->add();
	}
/**
 * [get_all_data 获得所有数据]
 * @return [type] [description]
 */
	public function get_all_data($row = 10){
		// 获得商家总数
		$total = $this->count();
		// 实例化分页类
		$page = new Page($total,$row,10);
		// 获得分页
		$pagelist = $page ->show();

		$data = $this->all($page->limit());
		$data['pagelist'] = $pagelist;
		return $data;
	}

	public function get_data(){
		return $this->all();
	}
/**
 * [get_one_data 获得一条数据]
 * @param  [type] $sid [description]
 * @return [type]      [description]
 */
	public function get_one_data($sid){
		return $this->where(array('sid'=>$sid))->find();
	}
/**
 * [get_one_data 修改数据]
 * @param  [type] $sid [description]
 * @return [type]      [description]
 */
	public function edit_data(){
		return $this->save();
	}

/**
 * [del_data 删除商家]
 * @param  [type] $sid [description]
 * @return [type]      [description]
 */
	public function del_data($sid){
		$this->where(array('sid'=>$sid))->del();
		// 删除商家位置
		K('SellerLocation')->del_data($sid);
		// 删除商家发布的产品
		    // 1.获得商家发布的商品 gid 
		    $gidArr = K('Goods')->get_data(array('sid'=>$sid),'gid');
		    if(!empty($gidArr)){
		    	foreach ($gidArr as $v) {
		    		K('Goods')->del_data($v['gid']);
		    	}
		    }
	}
}


 ?>