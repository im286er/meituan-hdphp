<?php 
class GoodsAttrViewModel extends ViewModel{
	public $table = 'category';

/**
 * [$veiw 关联表]
 * @var array
 */
	public $view = array(
		'type' => array(
			'type' => INNER_JOIN, // 指定连接方式
			'on'   => 'category.tid = type.tid'
			),
		'type_attr' => array(
			'type' => INNER_JOIN, // 指定连接方式
			'on'   => 'type.tid = type_attr.type_tid'
			),
		'attr' => array(
			'type' => INNER_JOIN, // 指定连接方式
			'on'   => 'type_attr.attr_kindid = attr.kindid'
			)
		);
/**
 * [get_type_attr 取出一个类型的属性]
 * @param  [type] $tid [description]
 * @return [type]      [description]
 */
	public function get_type_attr($cid = 0){
		return $this->where(array('cid'=>$cid))->order('atid asc')->all();
	}
}

 ?>