<?php 
class GoodsViewModel extends ViewModel{
	public $table = 'goods';

	public $view = array(
		'category' => array(
			'type' => INNER_JOIN,
			'on'   => 'goods.cid = category.cid'
			),
		'seller'   => array(
			'type' => INNER_JOIN,
			'on'   => 'goods.sid = seller.sid'
			),
		'goods_info'   => array(
			'type' => INNER_JOIN,
			'on'   => 'goods.gid = goods_info.goods_gid'
			),
		);
/**
 * [get_field 获得所有商品中的某些字段数据]
 * @param  [type] $field [description]
 * @return [type]        [description]
 */
	public function get_field($field = null,$row = null){
		$total = $this->count();
		$page = new Page($total , $row , 5 ,3);
		$pagelist = $page->show();
		
		$data = $this->field($field)->all($page->limit());
		$data['pagelist'] = $pagelist;
		return $data;
	}

/**
 * [get_one 获得一条数据]
 * @param  integer $gid [description]
 * @return [type]       [description]
 */
	public function get_one($gid = 0){
		return $this->where(array('gid'=>$gid))->find();
	}
/**
 * [get_seller_info 获得商家的一条基本信息]
 * @return [type] [description]
 */
	public function get_seller_info($where = null){
		return $this->where($where)->join('goods_info')->all();
	}

	
}



