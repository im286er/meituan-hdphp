<?php 
class GoodsInfoModel extends Model{
	public $table = 'goods_info';

/**
 * [add_data 添加数据]
 */
	public function add_data(){
		$this->add();
	}

/**
 * [edit_data 修改数据]
 * @param  integer $gid [description]
 * @return [type]       [description]
 */
	public function edit_data($gid = 0){
		$this->where(array('goods_gid'=>$gid))->save();
	}
/**
 * [del_data 删除数据]
 * @param  integer $gid [description]
 * @return [type]       [description]
 */
	public function del_data($gid = 0){
		$this->where(array('goods_gid'=>$gid))->del();
	}
}


