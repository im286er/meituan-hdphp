<?php 
class CategoryTypeViewModel extends ViewModel{
	public $table = 'category';

	public $view = array(
		'type'=>array( // 定义 type_attr 表规则
		 		'type' => INNER_JOIN, // 指定链接方式
		 		'on'   => 'type.tid = category.tid' //关联条件
		 		),
		);

/**
 * [get_all_data 获得所有数据]
 * @return [type] [description]
 */
	public function get_all_data(){
		return $this->order('cid asc')->all();
	}


/**
 * [get_all_data 获得所有数据]
 * @return [type] [description]
 */
	public function get_one_data($cid){
		return $this->where(array('cid'=>$cid))->find();
	}
}


 ?>