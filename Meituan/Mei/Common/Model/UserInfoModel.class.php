<?php 
class UserInfoModel extends Model{
	public $table = 'userinfo';
/**
 * [get_data 获得用户详情]
 * @param  integer $uid [description]
 * @return [type]       [description]
 */
	public function get_data($uid = 0){
		return $this->where(array('user_uid'=>$uid))->find();
	}
/**
 * [edit 修改用户某些信息]
 * @return [type] [description]
 */
	public function edit($uid = 0, $data = null){
		$this->where(array('user_uid'=>$uid))->save($data);
	}
/**
 * [add_data 添加数据]
 */
	public function add_data($data = null){
		$this->add($data);
	}
}


