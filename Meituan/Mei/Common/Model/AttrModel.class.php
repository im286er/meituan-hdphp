<?php 
class AttrModel extends Model{
	public $table = 'attr'; //定义模型表
/**
 * [add_attr 添加属性]
 */
	public function add_attr(){
		// 获得属性值
		$value = Q('post.attrvalue');
		// 将字符串拆分成数组
		$valueArr = explode('|', $value);
		// 循环插入数据库
		foreach ($valueArr as $v) {
			$_POST['attrvalue'] = $v;
			$this->add();
		}
	}

	public function add_value_son(){
		$this->add();
	}
/**
 * [get_all_attr 获得所有的属性]
 * @param  [type] $group [description]
 * @param  [type] $row   [每页显示条数]
 * @return [type]        [description]
 */
	public function get_all_attr($group = null,$row = '10'){
		// 总数
		$total = $this->group($group)->count();
		// 实例化分页类
		$page = new Page($total,$row,5);
		// 获得分页样式
		$pagelist = $page->show();
		// 获得所有属性
		$data = $this->group($group)->order('kindid asc')->all($page->limit());
		// 将分页压入数组一并反出
		$data['pagelist'] = $pagelist;
		return $data;
	}
/**
 * [get_max_kindid 获得最大的种类id kindid]
 * @return [type] [description]
 */
	public function get_max_kindid(){
		$max_kindid = $this->max('kindid');
		return is_null($max_kindid) ? 0 : $max_kindid;
	}
	/**
	 * [get_one_attr 获得一条属性信息]
	 * @param  [type] $where [description]
	 * @return [type]        [description]
	 */
	public function get_one(){
		$atid = Q('get.atid','','intval');
		return $this->where(array('atid'=>$atid))->find();
	}
	/**
	 * [get_attr_value 获得属性的所有值]
	 * @return [type] [description]
	 */
	public function get_attr_value(){
		$kindid = Q('get.kindid','','intval');
		return $this->where(array('kindid'=>$kindid))->all();
	}
/**
 * [get_value 通过传递参数，获得属性值]
 * @param  [type] $kindid [description]
 * @return [type]         [description]
 */
	public function get_value($kindid = null){
		return $this->where(array('kindid'=>$kindid))->all();
	}
	/**
	 * [edit 修改数据]
	 * @return [type] [description]
	 */
	public function edit($where , $data){
		
		return $this->where($where)->save($data);
	}
	/**
	 * [del 删除数据]
	 * @param  [type] $where [description]
	 * @return [type]        [description]
	 */
	public function attr_del($where){
		return $this->where($where)->delete();
	}

	public function get_value_son($atid = 0){
		return $this->where(array('pid'=>$atid))->all();
	}
}


 ?>