<?php 
/**
 * 商品控制器类
 */
class GoodsModel extends Model{
	public $table = 'goods';

	public function add_data(){

		// 获得服务承诺数组
		$promise = Q('post.promise');
		// 将数组转为字符串
		if(!empty($promise)){
			$promise = implode(',', $promise);
		}

		$_POST['promise'] = $promise;
		// 获得图片数组
		$pic = Q('post.pic','','');
		// 将数组转成字符串
		$picStr = '';
		if(!empty($pic)){
			foreach ($pic as $v) {
				$picStr .= $v['path'] . ';';
			}
		}
		$_POST['pic'] = $picStr;
		// 向主表 goods 中添加数据 并得到插入 id
		$insert_id = $this->add();
		
		// 将该商品 id 压入 商品信息数组，为插入附表用
		$_POST['goods_gid'] = $insert_id;
		K('GoodsInfo')->add_data();
		// 向商品属性表 goods_attr 中添加数据
		$atid = isset($_POST['atid']) ? $_POST['atid'] : '';
		if(!empty($atid)){
			foreach ($atid as $k => $v) {
				K('GoodsAttr')->add_data(array('goods_gid'=>$insert_id,'attr_atid'=>$v));
			}
		}

		
	}


/**
 * [get_one 获取一条信息]
 * @param  [type] $gid [description]
 * @return [type]      [description]
 */
	public function get_one($where = null){
		return $this->where($where)->find();
	}
/**
 * [get_data 通过条件获得商品数据]
 * @return [type] [description]
 */
	public function get_data($where = null,$field = null,$order = null,$limit = null){
		return $this->where($where)->field($field)->order($order)->limit($limit)->all();
	}
/**
 * [get_data2 通过分类下的多个 cid 获得分类下的商品]
 * @return [type] [description]
 */
	public function get_data2($in = null , $limit = null){
		return $this->in(array('cid'=>$in))->limit($limit)->all();
	}

	public function get_data_sch($where = null){
		return $this->where($where)->all();
	}
/**
 * [get_from_attr 通过各种属性条件获得商品]
 * @return [type] [description]
 */
	public function get_from_attr($cid=null,$in=null){
		return $this->all();
	}
/**
 * [edit_data 修改数据]
 * @return [type] [description]
 */
	public function edit_data(){
		$gid = Q('post.gid','','intval');
		// 获得图片数组
		$pic = Q('post.pic','','');
		// 将数组转成字符串
		$picStr = '';
		if(!empty($pic)){
			foreach ($pic as $v) {
				$picStr .= $v['path'] . ';';
			}
		}
		$_POST['pic'] = $picStr;

		// 修改主表数据
		$this->save();

		// 修改附加表
		K('GoodsInfo')->edit_data($gid);

		// 修改商品属性表
		  // 1. 删除原来的属性
		   K('GoodsAttr')->del_attr($gid);
		  // 2.重新添加新数据
			$atid = isset($_POST['atid']) ? $_POST['atid'] : '';
			if(!empty($atid)){
				foreach ($atid as $k => $v) {
					K('GoodsAttr')->add_data(array('goods_gid'=>$gid,'attr_atid'=>$v));
				}
			}
	}

	public function edit($where,$data){
		$this->where($where)->save($data);
	}
/**
 * [del_data 删除数量]
 * @param  integer $gid [description]
 * @return [type]       [description]
 */
	public function del_data($gid = 0){
		
		// 删除主表
		$this->where(array('gid'=>$gid))->del();
		// 删除附表信息
		K('GoodsInfo')->del_data($gid);
		// 删除商品属性表中的数据
		 K('GoodsAttr')->del_attr($gid);
	}


}

 ?>