<?php 
// 商品属性表模型
class GoodsAttrModel extends Model{
	public $table = 'goods_attr';
/**
 * [add_data 插入数据]
 * @param [type] $data [description]
 */
	public function add_data($data){
		$this->add($data);
	}
/**
 * [get_data 获得数据]
 * @param  [type] $gid [description]
 * @return [type]      [description]
 */
	public function get_data($gid = 0){
		return $this->where(array('goods_gid'=>$gid))->field('attr_atid')->all();
	}
/**
 * [get_data_byatid 通过属性 atid 获得数据]
 * @return [type] [description]
 */
	public function get_data_byatid($atid){
		return $this->where(array('attr_atid'=>$atid))->all();
	}

/**
 * [del_attr 删除数据]
 * @param  integer $gid [description]
 * @return [type]       [description]
 */
	public function del_attr($gid = 0){
		$this->where(array('goods_gid'=>$gid))->del();
	}
}

