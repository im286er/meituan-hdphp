<?php 
class CategoryModel extends Model{
	public $table = 'category'; //定义模型表

/**
 * [get_one 获得一条数据]
 * @param  integer $cid [description]
 * @return [type]       [description]
 */
	public function get_one($cid = 0){
		return $this->where(array('cid'=>$cid))->find();
	}
/**
 * [get_all 获得所有的数据]
 * @return [type] [description]
 */
	public function get_all($where = null){
		return $this->where($where)->all();
	}

/**
 * [add_data 添加数据]
 */
	public function add_data(){
		return $this->add();
	}

/**
 * [edit 添加数据]
 */
	public function edit(){
		return $this->save();
	}


/**
 * [dete 添加数据]
 */
	public function dele($where){
		return $this->where($where)->delete();
	}
/**
 * [get_fields 获得指定条件查询出来的数据的某些字段值]
 * @param  [type] $field [description]
 * @return [type]        [description]
 */
	public function get_fields($where = null,$field = null){
		return $this->where($where)->field($field)->all();
	}	
}

 ?>