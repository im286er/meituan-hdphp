<?php 
class UserViewModel extends ViewModel{
	public $table = 'user';

	public $view = array(
		'userinfo'=>array(
			'type'	=> INNER_JOIN,
			'on' 	=> 'user.uid = userinfo.user_uid'
			),
		);

	public function get_one($where = null){
		return $this->where($where)->find();
	}
}


