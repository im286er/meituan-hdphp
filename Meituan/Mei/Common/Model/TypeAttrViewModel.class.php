<?php 
class TypeAttrViewModel extends ViewModel{
		//定义主表
		public $table = 'type';

		 public $view = array(
		 	'type_attr'=>array( // 定义 type_attr 表规则
		 		'type' => INNER_JOIN, // 指定链接方式
		 		'on'   => 'type.tid = type_attr.type_tid' //关联条件
		 		),
		 	'attr'=>array( // 定义 type_attr 表规则
		 		'type' => INNER_JOIN, // 指定链接方式
		 		'on'   => 'attr.kindid = type_attr.attr_kindid' //关联条件
		 		),
		 );


		 public function get_one($tid = 0){
		 	return $this->where(array('tid'=>$tid))->group('kindid')->all();
		 }
}


 ?>