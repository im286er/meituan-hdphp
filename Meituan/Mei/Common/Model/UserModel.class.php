<?php 
class UserModel extends Model{
	public $table = 'user';
	public $validate = array(
		array('email','nonull','邮箱不能为空',2,3),
		array('email','email','邮箱填写不正确',2,3),
		array('username','nonull','用户名不能为空',2,3),
		array('password','nonull','密码不能为空',2,3),
		);
	/**
	 * [is_have 检测数据库中是否已存在]
	 * @param  [type]  $field [description]
	 * @param  [type]  $value [description]
	 * @return boolean        [description]
	 */
	public function is_have($field = null , $value = null){
		return $this->where(array("$field"=>"$value"))->find();
	}

	/**
	 * [login 前台用户登陆]
	 * @return [type] [description]
	 */
	public function login(){
		// 接收登陆信息
		$username = Q('post.username');
		$password = Q('post.password','','md5');
		$result1 = $this->where(array('username'=>$username,'password'=>$password))->find();
		$result2 = $this->where(array('email'=>$username,'password'=>$password))->find();
		if($result1){
			return $result1;
		}
		if($result2){
			return $result2;
		}
		return false;
	}

	public function get_all(){
		return $this->all();
	}
/**
 * [get_one 获取用户信息]
 * @return [type] [description]
 */
	public function get_one($where = null){
		return $this->where($where)->find();
	}
/**
 * [edit 修改用户信息]
 * @param  [type] $where [description]
 * @param  [type] $data  [description]
 * @return [type]        [description]
 */
	public function edit($where = null ,$data = null){
		$this->where($where)->save($data);
	}
/**
 * [del_user 删除用户]
 * @param  [type] $where [description]
 * @return [type]        [description]
 */
	public function del_user($where = null){
		$this->where($where)->delete();
	}

}


 ?>