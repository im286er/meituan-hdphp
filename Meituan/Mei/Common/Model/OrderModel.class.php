<?php 
class OrderModel extends Model{
	public $table = 'order'; // 表名	
/**
 * [add_data 增加订单]
 * @param [type] $data [description]
 */
	public function add_data($data = null){
		return $this->add($data);
	}
/**
 * [edit 修改订单]
 * @param  integer $order_id [description]
 * @param  [type]  $data     [description]
 * @return [type]            [description]
 */
	public function edit($order_id = 0 , $data = null){
		$this->where(array('order_id'=>$order_id))->save($data);
	}
	public function get_one($where = null){
		return $this->where($where)->find();
	}
/**
 * [get_data 获得多个订单]
 * @param  [type] $where [description]
 * @return [type]        [description]
 */
	public function get_data($where = null){
		return $this->where($where)->all();
	}
/**
 * [del_data 删除订单]
 * @return [type] [description]
 */
	public function del_data($where = null){
		$this->where($where)->delete();
	}
}