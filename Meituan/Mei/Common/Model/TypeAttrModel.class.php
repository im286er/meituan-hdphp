<?php 
/*类型和属性中间表模型类*/
class TypeAttrModel extends Model{
	public $table = 'type_attr';
	/**
	 * [add_data 添加数据]
	 * @param [type] $data [description]
	 */
	public function add_data($data = null){
		return $this->add($data);
	}
	/**
	 * [get_one_type_attr 获得一个类型下的所有属性]
	 * @return [type] [description]
	 */
	public function get_onetype_attr($tid = 0){
		return $this->where(array('type_tid'=>$tid))->all();
	}

	/**
	 * [del 删除数据]
	 * @param  [type] $tid [description]
	 * @return [type]      [description]
	 */
	public function dele($tid){
		return $this->where(array('type_tid'=>$tid))->delete();
	}


}

 ?>