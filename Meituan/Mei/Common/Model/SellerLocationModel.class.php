<?php 
class SellerLocationModel extends Model{
	public $table = 'seller_location';
/**
 * [get_seller_location 获得某商家的所有位置]
 * @return [type] [description]
 */
	public function get_seller_location($sid){
		return $this->where(array('sid'=>$sid))->all();
	}
/**
 * [get_one_location 获得商家的一条位置信息]
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
	public function get_one_location($id){
		return $this->where(array('id'=>$id))->find();
	}

/**
 * [del_data 删除商家所有位置]
 * @param  [type] $sid [description]
 * @return [type]      [description]
 */
	public function del_data($sid){
		$this->where(array('sid'=>$sid))->del();
	}

	public function del_one_location($id){
		$this->where(array('id'=>$id))->del();
	}
/**
 * [add_data 添加数据]
 */
	public function add_data(){
		$this->add();
	}
}



 ?>