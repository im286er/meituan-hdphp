<?php
if (!defined("HDPHP_PATH"))exit('No direct script access allowed');
//更多配置请查看hdphp/Config/config.php
return array(
	    /********************************数据库********************************/
	    'DB_DRIVER'                     => 'mysqli',    //数据库驱动
	    'DB_CHARSET'                    => 'utf8',      //数据库字符集
	    'DB_HOST'                       => '127.0.0.1', //数据库连接主机  如127.0.0.1
	    'DB_PORT'                       => 3306,        //数据库连接端口
	    'DB_USER'                       => 'root',      //数据库用户名
	    'DB_PASSWORD'                   => '',          //数据库密码
	    'DB_DATABASE'                   => 'group',          //数据库名称
	    'DB_PREFIX'                     => 'hd_',          //表前缀
	    'DB_BACKUP'                     => 'backup/',   //数据库备份目录

		/********************************文件上传********************************/
	    'UPLOAD_THUMB_ON'               => FALSE,           //上传图片缩略图处理
	    'UPLOAD_EXT_SIZE'               => array('jpg' => 5000000, 'jpeg' => 5000000, 'gif' => 5000000,
	                                    'png' => 5000000, 'bmg' => 5000000, 'zip' => 5000000,
	                                    'txt' => 5000000, 'rar' => 5000000, 'doc' => 5000000), //上传类型与大小
	    'UPLOAD_PATH'                   => 'upload/' . date('ymd'),   //上传路径
	    'UPLOAD_IMG_DIR'                => '',          //图片上传目录名
	    'UPLOAD_IMG_RESIZE_ON'          => FALSE,       //上传图片缩放处理,超过以下值系统进行缩放
	    'UPLOAD_IMG_MAX_WIDTH'          => 1000,        //上传图片宽度超过此值，进行缩放
	    'UPLOAD_IMG_MAX_HEIGHT'         => 1000,        //上传图片高度超过此值，进行缩放
	    /********************************图片缩略图********************************/
	    'THUMB_PREFIX'                  => '',          //缩略图前缀
	    'THUMB_ENDFIX'                  => '_thumb',    //缩略图后缀
	    'THUMB_TYPE'                    => 1,  //生成方式,
	                                            //1:固定宽度,高度自增 2:固定高度,宽度自增 3:固定宽度,高度裁切
	                                            //4:固定高度,宽度裁切 5:缩放最大边       6:自动裁切图片
	    'THUMB_WIDTH'                   => 460,         //缩略图宽度
	    'THUMB_HEIGHT'                  => 279,         //缩略图高度
	    'THUMB_PATH'                    => '',          //缩略图路径
);
?>