/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : group

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2014-07-25 11:50:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `hd_category`
-- ----------------------------
DROP TABLE IF EXISTS `hd_category`;
CREATE TABLE `hd_category` (
  `cid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `cname` varchar(45) NOT NULL DEFAULT '' COMMENT '分类名称',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `tid` int(10) unsigned NOT NULL COMMENT '所属类型',
  `is_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `sort` tinyint(4) NOT NULL DEFAULT '50' COMMENT '排序',
  `keywords` varchar(60) DEFAULT NULL COMMENT '分类关键字',
  `des` varchar(60) DEFAULT NULL COMMENT '分类标题',
  PRIMARY KEY (`cid`),
  KEY `fk_hd_category_hd_type_idx` (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=utf8 COMMENT='栏目表';

-- ----------------------------
-- Records of hd_category
-- ----------------------------
INSERT INTO `hd_category` VALUES ('1', '美食', '0', '1', '1', '1', '', null);
INSERT INTO `hd_category` VALUES ('2', '酒店', '0', '2', '1', '2', '', null);
INSERT INTO `hd_category` VALUES ('3', '电影', '0', '3', '1', '3', '', null);
INSERT INTO `hd_category` VALUES ('4', '休闲娱乐', '0', '4', '1', '4', '', null);
INSERT INTO `hd_category` VALUES ('5', '旅游', '0', '5', '1', '5', '', null);
INSERT INTO `hd_category` VALUES ('6', '生活服务', '0', '6', '1', '6', '', null);
INSERT INTO `hd_category` VALUES ('7', '购物', '0', '7', '1', '7', '', null);
INSERT INTO `hd_category` VALUES ('8', '丽人', '0', '8', '1', '8', '', null);
INSERT INTO `hd_category` VALUES ('9', '自助餐', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('10', '火锅', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('11', '烤肉', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('12', '烧烤/烤串', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('13', '麻辣烫', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('14', '西餐', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('15', '日本料理', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('16', '蛋糕', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('17', '香锅烤鱼', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('18', '聚餐宴请', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('19', '川湘菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('20', '韩国料理', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('21', '鲁菜/北京菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('22', '小吃快餐', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('23', '甜点饮品', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('24', '海鲜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('25', '粤港菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('26', '咖啡酒吧茶馆', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('27', '东南亚菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('28', '江浙菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('29', '东北菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('30', '其他美食', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('31', '创意菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('32', '西北菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('33', '云贵菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('34', '台湾菜/客家菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('35', '素食', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('36', '新疆菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('37', '食品饮料', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('38', '汤/粥/炖菜', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('39', '蒙餐', '1', '1', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('40', '经济型酒店', '2', '2', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('41', '豪华酒店', '2', '2', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('42', '公寓式酒店', '2', '2', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('43', '主题酒店', '2', '2', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('44', '度假酒店', '2', '2', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('45', '温泉酒店', '2', '2', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('46', '客栈', '2', '2', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('47', '青年旅社', '2', '2', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('48', '钟点房', '2', '2', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('49', '美嘉欢乐影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('50', '华谊兄弟影院', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('51', '保利国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('52', 'CGV星星国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('53', 'CGV国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('54', '横店电影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('55', '17.5影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('56', '金逸影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('57', '鲁信影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('58', '星空影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('59', '大地数字影院', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('60', '耀莱成龙国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('61', '星美国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('62', '万达影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('63', '博纳国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('64', '东宫影剧院', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('65', '橙天嘉禾影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('66', '燕山影剧院', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('67', '中华影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('68', '幸福蓝海国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('69', '新华国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('70', '沃美影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('71', '阳光影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('72', '中影国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('73', '卢米埃影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('74', '嘉美国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('75', '百丽宫影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('76', '百老汇影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('77', '东都影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('78', 'UME国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('79', '首都电影院', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('80', '世茂国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('81', '华影国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('82', '嘉华国际影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('83', '红星太平洋电影城', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('84', '其它', '3', '3', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('85', 'KTV', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('86', '足疗按摩', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('87', '水上世界', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('88', '亲子游玩', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('89', '温泉', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('90', '洗浴/汗蒸', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('91', '滑雪', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('92', '游泳/水上乐园', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('93', '运动健身', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('94', '咖啡/酒吧', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('95', '桌游/电玩', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('96', '景点郊游', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('97', '主题公园/游乐园', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('98', '儿童乐园', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('99', '采摘/农家乐', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('100', '演出/赛事/展览', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('101', 'DIY手工', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('102', '真人CS', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('103', '私人影院', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('104', 'D/D电影', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('105', '密室逃脱', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('106', '其他娱乐', '4', '4', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('107', '本地/周边游', '5', '5', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('108', '景点门票', '5', '5', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('109', '国内游', '5', '5', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('110', '境外游', '5', '5', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('111', '婚纱摄影', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('112', '个性写真', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('113', '儿童摄影', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('114', '其他摄影', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('115', '照片冲印', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('116', '汽车服务', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('117', '配镜', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('118', '体检保健', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('119', '母婴亲子', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('120', '培训课程', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('121', '服装定制', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('122', '鲜花婚庆', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('123', '商场购物卡', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('124', '其他生活', '6', '6', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('125', '女装', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('126', '男装', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('127', '内衣', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('128', '鞋靴', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('129', '箱包/皮具', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('130', '家居日用', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('131', '家纺', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('132', '食品', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('133', '饰品/手表', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('134', '美妆/个护', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('135', '电器/数码', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('136', '母婴/玩具', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('137', '运动/户外', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('138', '本地购物', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('139', '其他', '7', '7', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('140', '美发', '8', '8', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('141', '美容美体', '8', '8', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('142', '美甲', '8', '8', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('143', '瑜伽/舞蹈', '8', '8', '1', '50', '', null);
INSERT INTO `hd_category` VALUES ('144', '个性写真', '8', '8', '1', '50', '', null);
