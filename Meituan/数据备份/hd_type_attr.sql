/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : group

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2014-07-25 11:51:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `hd_type_attr`
-- ----------------------------
DROP TABLE IF EXISTS `hd_type_attr`;
CREATE TABLE `hd_type_attr` (
  `type_tid` int(10) unsigned NOT NULL,
  `attr_kindid` int(10) unsigned NOT NULL,
  KEY `fk_hd_type_attr_hd_type1_idx` (`type_tid`),
  KEY `fk_hd_type_attr_hd_attr1_idx` (`attr_kindid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='类型属性中间表';

-- ----------------------------
-- Records of hd_type_attr
-- ----------------------------
INSERT INTO `hd_type_attr` VALUES ('1', '1');
INSERT INTO `hd_type_attr` VALUES ('1', '2');
INSERT INTO `hd_type_attr` VALUES ('1', '3');
INSERT INTO `hd_type_attr` VALUES ('2', '1');
INSERT INTO `hd_type_attr` VALUES ('2', '2');
INSERT INTO `hd_type_attr` VALUES ('2', '4');
INSERT INTO `hd_type_attr` VALUES ('3', '1');
INSERT INTO `hd_type_attr` VALUES ('4', '1');
INSERT INTO `hd_type_attr` VALUES ('5', '1');
INSERT INTO `hd_type_attr` VALUES ('6', '1');
INSERT INTO `hd_type_attr` VALUES ('7', '2');
INSERT INTO `hd_type_attr` VALUES ('8', '1');
