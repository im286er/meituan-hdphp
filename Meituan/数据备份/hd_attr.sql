/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : group

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2014-07-25 11:50:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `hd_attr`
-- ----------------------------
DROP TABLE IF EXISTS `hd_attr`;
CREATE TABLE `hd_attr` (
  `atid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attrname` varchar(45) NOT NULL DEFAULT '' COMMENT '属性名称',
  `attrvalue` varchar(45) NOT NULL DEFAULT '' COMMENT '属性名称',
  `kindid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '种类，比如地区算一个种类，价格算一个种类',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`atid`),
  KEY `kindid` (`kindid`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='属性表';

-- ----------------------------
-- Records of hd_attr
-- ----------------------------
INSERT INTO `hd_attr` VALUES ('1', '区域', '朝阳区', '1', '0');
INSERT INTO `hd_attr` VALUES ('2', '区域', '海淀区', '1', '0');
INSERT INTO `hd_attr` VALUES ('3', '区域', '丰台区', '1', '0');
INSERT INTO `hd_attr` VALUES ('4', '区域', '西城区', '1', '0');
INSERT INTO `hd_attr` VALUES ('5', '区域', '东城区', '1', '0');
INSERT INTO `hd_attr` VALUES ('6', '区域', '昌平区', '1', '0');
INSERT INTO `hd_attr` VALUES ('7', '区域', '石景山区', '1', '0');
INSERT INTO `hd_attr` VALUES ('8', '区域', '通州区', '1', '0');
INSERT INTO `hd_attr` VALUES ('9', '区域', '大兴区', '1', '0');
INSERT INTO `hd_attr` VALUES ('10', '区域', '顺义区', '1', '0');
INSERT INTO `hd_attr` VALUES ('11', '区域', '房山区', '1', '0');
INSERT INTO `hd_attr` VALUES ('12', '区域', '密云县', '1', '0');
INSERT INTO `hd_attr` VALUES ('13', '区域', '怀柔区', '1', '0');
INSERT INTO `hd_attr` VALUES ('14', '区域', '延庆县', '1', '0');
INSERT INTO `hd_attr` VALUES ('15', '区域', '平谷区', '1', '0');
INSERT INTO `hd_attr` VALUES ('16', '区域', '门头沟', '1', '0');
INSERT INTO `hd_attr` VALUES ('17', '价格', '20元以下', '2', '0');
INSERT INTO `hd_attr` VALUES ('18', '价格', '20-50元', '2', '0');
INSERT INTO `hd_attr` VALUES ('19', '价格', '50-80元', '2', '0');
INSERT INTO `hd_attr` VALUES ('20', '价格', '80-120元', '2', '0');
INSERT INTO `hd_attr` VALUES ('21', '价格', '120-200元', '2', '0');
INSERT INTO `hd_attr` VALUES ('22', '价格', '200-500元', '2', '0');
INSERT INTO `hd_attr` VALUES ('23', '价格', '500-800元', '2', '0');
INSERT INTO `hd_attr` VALUES ('24', '价格', '800元以上', '2', '0');
INSERT INTO `hd_attr` VALUES ('25', '人数', '单人餐', '3', '0');
INSERT INTO `hd_attr` VALUES ('26', '人数', '双人餐', '3', '0');
INSERT INTO `hd_attr` VALUES ('27', '人数', '3-4人', '3', '0');
INSERT INTO `hd_attr` VALUES ('28', '人数', '5-6人', '3', '0');
INSERT INTO `hd_attr` VALUES ('29', '人数', '7-8人', '3', '0');
INSERT INTO `hd_attr` VALUES ('30', '人数', '9-10人', '3', '0');
INSERT INTO `hd_attr` VALUES ('31', '人数', '10人以上', '3', '0');
INSERT INTO `hd_attr` VALUES ('32', '人数', '其他', '3', '0');
INSERT INTO `hd_attr` VALUES ('33', '房型', '大床房', '4', '0');
INSERT INTO `hd_attr` VALUES ('34', '房型', '双床房', '4', '0');
INSERT INTO `hd_attr` VALUES ('35', '房型', '单人房', '4', '0');
INSERT INTO `hd_attr` VALUES ('36', '房型', '三人间', '4', '0');
INSERT INTO `hd_attr` VALUES ('37', '房型', '其他', '4', '0');
