/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : group

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2014-07-25 11:50:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `hd_type`
-- ----------------------------
DROP TABLE IF EXISTS `hd_type`;
CREATE TABLE `hd_type` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '类型id',
  `tname` varchar(45) NOT NULL DEFAULT '' COMMENT '类型名称',
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hd_type
-- ----------------------------
INSERT INTO `hd_type` VALUES ('1', '美食类');
INSERT INTO `hd_type` VALUES ('2', '酒店类');
INSERT INTO `hd_type` VALUES ('3', '电影类');
INSERT INTO `hd_type` VALUES ('4', '休闲娱乐类');
INSERT INTO `hd_type` VALUES ('5', '旅游类');
INSERT INTO `hd_type` VALUES ('6', '生活服务类');
INSERT INTO `hd_type` VALUES ('7', '购物类');
INSERT INTO `hd_type` VALUES ('8', '丽人类');
