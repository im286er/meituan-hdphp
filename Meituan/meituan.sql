SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `group` ;
CREATE SCHEMA IF NOT EXISTS `group` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `group` ;

-- -----------------------------------------------------
-- Table `group`.`hd_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_type` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_type` (
  `tid` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '类型id',
  `tname` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '类型名称',
  PRIMARY KEY (`tid`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `group`.`hd_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_category` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_category` (
  `cid` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `cname` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '分类名称',
  `pid` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级id',
  `tid` INT UNSIGNED NOT NULL COMMENT '所属类型',
  `is_show` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '是否显示',
  `sort` TINYINT NOT NULL DEFAULT 50 COMMENT '排序',
  `keywords` VARCHAR(60) NULL COMMENT '分类关键字',
  `des` VARCHAR(60) NULL COMMENT '分类描述',
  PRIMARY KEY (`cid`),
  INDEX `fk_hd_category_hd_type_idx` (`tid` ASC))
ENGINE = MyISAM
COMMENT = '栏目表';


-- -----------------------------------------------------
-- Table `group`.`hd_seller`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_seller` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_seller` (
  `sid` INT NOT NULL AUTO_INCREMENT COMMENT '商家id',
  `sname` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '商家名称',
  `introduction` TEXT NULL COMMENT '商家介绍',
  PRIMARY KEY (`sid`))
ENGINE = MyISAM
COMMENT = '商家';


-- -----------------------------------------------------
-- Table `group`.`hd_goods`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_goods` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_goods` (
  `gid` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `title` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '主标题',
  `subtitle` VARCHAR(100) NULL COMMENT '副标题',
  `price` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT 0 COMMENT '原价',
  `group_price` DECIMAL(10,2) NULL COMMENT '团购价',
  `pic` VARCHAR(300) NULL COMMENT '商品图',
  `cid` INT UNSIGNED NOT NULL,
  `sid` INT NOT NULL COMMENT '所属商家id',
  PRIMARY KEY (`gid`),
  INDEX `fk_hd_goods_hd_category1_idx` (`cid` ASC),
  INDEX `fk_hd_goods_hd_seller1_idx` (`sid` ASC))
ENGINE = MyISAM
COMMENT = '商品表';


-- -----------------------------------------------------
-- Table `group`.`hd_attr`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_attr` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_attr` (
  `atid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `attrname` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '属性名称',
  `attrvalue` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '属性名称',
  `kindid` SMALLINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '种类，比如地区算一个种类，价格算一个种类',
  `pid` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`atid`),
  INDEX `kindid` (`kindid` ASC))
ENGINE = MyISAM
COMMENT = '属性表';


-- -----------------------------------------------------
-- Table `group`.`hd_type_attr`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_type_attr` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_type_attr` (
  `type_tid` INT UNSIGNED NOT NULL,
  `attr_kindid` INT UNSIGNED NOT NULL,
  INDEX `fk_hd_type_attr_hd_type1_idx` (`type_tid` ASC),
  INDEX `fk_hd_type_attr_hd_attr1_idx` (`attr_kindid` ASC))
ENGINE = MyISAM
COMMENT = '类型属性中间表';


-- -----------------------------------------------------
-- Table `group`.`hd_goods_attr`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_goods_attr` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_goods_attr` (
  `goods_gid` INT UNSIGNED NOT NULL COMMENT '商品id',
  `attr_atid` INT UNSIGNED NOT NULL COMMENT '属性atid',
  INDEX `fk_hd_goods_attr_hd_goods1_idx` (`goods_gid` ASC),
  INDEX `fk_hd_goods_attr_hd_attr1_idx` (`attr_atid` ASC))
ENGINE = MyISAM
COMMENT = '商品属性表';


-- -----------------------------------------------------
-- Table `group`.`hd_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_user` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_user` (
  `uid` INT NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `email` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '邮箱',
  `username` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '用户名',
  `nick` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` CHAR(32) NOT NULL DEFAULT '' COMMENT '密码',
  `registtime` INT(10) NOT NULL DEFAULT 0 COMMENT '注册时间',
  `logintime` INT(10) NOT NULL DEFAULT 0 COMMENT '登陆时间',
  PRIMARY KEY (`uid`))
ENGINE = MyISAM
COMMENT = '普通用户表';


-- -----------------------------------------------------
-- Table `group`.`hd_userinfo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_userinfo` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_userinfo` (
  `user_uid` INT NOT NULL,
  `sex` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '性别',
  `birthday` DATE NULL COMMENT '生日',
  `tel` CHAR(11) NOT NULL DEFAULT '' COMMENT '电话',
  `points` INT(10) NULL COMMENT '积分',
  `moeny` DECIMAL(10,2) NULL COMMENT '余额',
  `face` VARCHAR(100) NULL COMMENT '用户头像',
  INDEX `fk_userinfo_user1_idx` (`user_uid` ASC))
ENGINE = MyISAM
COMMENT = '用户信息表';


-- -----------------------------------------------------
-- Table `group`.`hd_useraddress`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_useraddress` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_useraddress` (
  `name` CHAR(10) NOT NULL DEFAULT '' COMMENT '姓名',
  `address` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '收获地址',
  `mobilephone` CHAR(11) NOT NULL DEFAULT '' COMMENT '收货人电话',
  `postcode` CHAR(6) NOT NULL DEFAULT '' COMMENT '邮编',
  `is_default` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否为默认地址',
  `user_uid` INT NOT NULL,
  INDEX `fk_useraddress_user1_idx` (`user_uid` ASC))
ENGINE = MyISAM
COMMENT = '收货地址';


-- -----------------------------------------------------
-- Table `group`.`hd_usercollect`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_usercollect` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_usercollect` (
  `gid` INT NOT NULL COMMENT '商品gid',
  `user_uid` INT NOT NULL,
  INDEX `fk_collect_user1_idx` (`user_uid` ASC))
ENGINE = MyISAM
COMMENT = '用户收藏表';


-- -----------------------------------------------------
-- Table `group`.`hd_admin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_admin` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_admin` (
  `aid` TINYINT NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `aname` CHAR(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` CHAR(32) NOT NULL DEFAULT '' COMMENT '密码',
  `logintime` INT(10) NOT NULL DEFAULT 0 COMMENT '登录时间',
  `issuper` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否超级管理员',
  PRIMARY KEY (`aid`))
ENGINE = MyISAM
COMMENT = '管理员表';


-- -----------------------------------------------------
-- Table `group`.`hd_cart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_cart` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_cart` (
  `goods_num` INT NOT NULL COMMENT '加入购物车的商品数量',
  `uid` INT NOT NULL COMMENT '用户id',
  `gid` INT UNSIGNED NOT NULL COMMENT '商品id',
  PRIMARY KEY (`goods_num`),
  INDEX `fk_cart_hd_user1_idx` (`uid` ASC),
  INDEX `fk_hd_cart_hd_goods1_idx` (`gid` ASC))
ENGINE = MyISAM
COMMENT = '购物车';


-- -----------------------------------------------------
-- Table `group`.`hd_comments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_comments` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_comments` (
  `sendtime` INT(10) NULL COMMENT '评论时间',
  `content` VARCHAR(100) NULL COMMENT '评论内容',
  `uid` INT NOT NULL COMMENT '用户id',
  `gid` INT UNSIGNED NOT NULL COMMENT '商品id',
  INDEX `fk_comments_hd_user1_idx` (`uid` ASC),
  INDEX `fk_hd_comments_hd_goods1_idx` (`gid` ASC))
ENGINE = MyISAM
COMMENT = '评论表';


-- -----------------------------------------------------
-- Table `group`.`hd_order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_order` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_order` (
  `order_id` INT(10) ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `sendtime` INT(10) NULL COMMENT '下单时间',
  `goods_num` TINYINT NULL COMMENT '商品数目',
  `subtotal` DECIMAL(10,2) NULL COMMENT '小计',
  `uid` INT NOT NULL COMMENT '用户id',
  `gid` INT UNSIGNED NOT NULL COMMENT '商品id',
  PRIMARY KEY (`order_id`),
  INDEX `fk_order_hd_user1_idx` (`uid` ASC),
  INDEX `fk_hd_order_hd_goods1_idx` (`gid` ASC))
ENGINE = MyISAM
COMMENT = '用户订单表';


-- -----------------------------------------------------
-- Table `group`.`hd_goods_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_goods_info` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_goods_info` (
  `goods_gid` INT UNSIGNED NOT NULL,
  `close_date` DATE NULL COMMENT '截止日期',
  `not_use_date` VARCHAR(30) NULL COMMENT '不可用日期',
  `use_time` VARCHAR(45) NULL COMMENT '使用时间',
  `promise` SET('1','2','3','4') NULL COMMENT '服务承诺（1，随时退 2过期退 3急速退 4真实评价）',
  `appointment` VARCHAR(45) NULL COMMENT '预约提醒',
  `reminder` TEXT(1000) NULL COMMENT '温馨提示',
  `service` VARCHAR(45) NULL COMMENT '服务',
  `details` TEXT NULL COMMENT '本单详情',
  INDEX `fk_hd_googs_info_hd_goods1_idx` (`goods_gid` ASC))
ENGINE = MyISAM
COMMENT = '商品附加表';


-- -----------------------------------------------------
-- Table `group`.`hd_seller_location`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group`.`hd_seller_location` ;

CREATE TABLE IF NOT EXISTS `group`.`hd_seller_location` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '商家位置id',
  `sid` INT NOT NULL COMMENT '商家di',
  `address` VARCHAR(60) NULL,
  `tel` VARCHAR(30) NULL COMMENT '商家电话',
  `subway` VARCHAR(45) NULL COMMENT '地铁',
  INDEX `fk_hd_seller_location_hd_seller1_idx` (`sid` ASC),
  PRIMARY KEY (`id`))
ENGINE = MyISAM
COMMENT = '商家位置（主店、分店位置）';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
